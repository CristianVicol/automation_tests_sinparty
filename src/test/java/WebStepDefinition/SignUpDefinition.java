package WebStepDefinition;

import enums.Context;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;
import pageObjects.webPages.CookiesPopUp;
import pageObjects.webPages.MainWeb;
import pageObjects.webPages.SignUpForm;
import utilities.TestContext;
import utilities.Wait;

import java.util.List;

import static enums.Context.USERNAME;

public class SignUpDefinition {
    TestContext testContext;
    SignUpForm signUpForm;
    MainWeb webMain;
    CookiesPopUp cookiesPopUp;

    public SignUpDefinition(TestContext testContext) {
        this.testContext = testContext;
        webMain = testContext.getPageObjectManager().getMainWeb();
        signUpForm = testContext.getPageObjectManager().getSignUpForm();
        cookiesPopUp = testContext.getPageObjectManager().getCookiesPopUp();
    }

    @Given("User is on sign up form")
    public void user_is_on_sign_up_form() {
        cookiesPopUp.clickAcceptCookiesBtn();
        signUpForm.navigateToSignUpForm();
    }

    @When("complete the form with valid email, user and password")
    public void complete_the_form_with_valid_email_user_and_password(DataTable dataTable) {
        int randomNumber = signUpForm.generateRandomNumber();
        List<String> dataRow = dataTable.row(1);
        String username = randomNumber + dataRow.get(0);
        testContext.getScenarioContext().setContext(Context.USERNAME, username);
        String email = randomNumber + dataRow.get(1);
        String password = dataRow.get(2);
        signUpForm.completeSigUpForm(username, email, password);
    }

    @When("click on Sign Up button")
    public void click_on_sign_up_button() {
        signUpForm.clickSignUpBtn();
    }

    @Then("Thank you pop up is displayed")
    public void thank_you_pop_up_is_displayed() {
        Assert.assertTrue(signUpForm.thankYouPopUpIsDisplayed());
    }

    @When("user click on proceed button")
    public void user_click_on_proceed_button() {
        signUpForm.clickProceedBtn();
    }

    @Then("user is redirected to main page")
    public void user_is_redirected_to_main_page() {
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                webMain.getUserNameTitle(), 10L);
        String userName = webMain.getUserNameTitle().getText();
        Assert.assertEquals(userName, testContext.getScenarioContext().getContext(USERNAME));
    }
}
package WebStepDefinition;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.testng.Assert;
import pageObjects.webPages.*;
import utilities.TestContext;
import utilities.Utilities;
import utilities.Wait;

import java.util.concurrent.TimeUnit;

public class AcquiringPremiumContent {
    TestContext testContext;
    CookiesPopUp cookiesPopUp;
    CreatorPage creatorPage;
    MainWeb mainWeb;
    LogInForm logInForm;
    UserAccount userAccount;
    CreatorsListing creatorsListing;
    SearchPage searchPage;

    public AcquiringPremiumContent(TestContext testContext){
        this.testContext = testContext;
        cookiesPopUp = testContext.getPageObjectManager().getCookiesPopUp();
        creatorPage = testContext.getPageObjectManager().getCreatorPage();
        mainWeb = testContext.getPageObjectManager().getMainWeb();
        logInForm = testContext.getPageObjectManager().getLogInForm();
        userAccount = testContext.getPageObjectManager().getUserAccount();
        creatorsListing = testContext.getPageObjectManager().getCreatorsListing();
        searchPage = testContext.getPageObjectManager().getSearchPage();
    }

    @Given("user is on the creator page to which  is not subscribed")
    public void user_is_on_the_creator_page_to_which_is_not_subscribed(DataTable dataTable) {
        String username = dataTable.cell(1, 0);
        String password = dataTable.cell(1, 1);
        String creatorName = dataTable.cell(1, 2);
        signIn(username, password);
        try {
            Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                    mainWeb.getUserMenu(), 30L);
            mainWeb.clickSearchBtn();
            Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                    searchPage.getSearchInput(), 30L);
        }catch (StaleElementReferenceException e){
            Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                    mainWeb.getUserMenu(), 30L);
            mainWeb.clickSearchBtn();
            Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                    searchPage.getSearchInput(), 30L);
        }
        try {
            searchPage.getSearchInput().sendKeys(creatorName);
        }catch (StaleElementReferenceException e){
            searchPage.getSearchInput().sendKeys(creatorName);
        }
        try {
            searchPage.getSearchBtn().click();
        }catch (StaleElementReferenceException e){
            searchPage.getSearchBtn().click();
        }
        try {
            Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                    searchPage.getCreatorName(), 40L);
            searchPage.getCreatorName().click();
        }catch (StaleElementReferenceException e){
            Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                    searchPage.getCreatorName(), 40L);
            searchPage.getCreatorName().click();
        }
    }
    @When("user click on premium posts section")
    public void user_click_on_premium_posts_section() {
        try {
            Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                    creatorPage.getPremiumPostsButton(), 30L);
            creatorPage.getPremiumPostsButton().click();
        }catch (StaleElementReferenceException e){
            Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                    creatorPage.getPremiumPostsButton(), 30L);
            creatorPage.getPremiumPostsButton().click();
        }
    }
    @When("click on subscribe now button")
    public void click_on_subscribe_now_button() {
        Utilities.scrollWindow(testContext.getDriverManager().getWebDriver(),
                0, 500);
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                creatorPage.getSubscribeNowBtn(), 30L);
        try {
            creatorPage.getSubscribeNowBtn().click();
        }catch (StaleElementReferenceException e){
            creatorPage.getSubscribeNowBtn().click();
        }
    }
    @Then("pop-up is displayed with subscribe options")
    public void pop_up_is_displayed_with_subscribe_options() {
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                creatorPage.getSubscribeToCreatorButtonFromPopUp(), 30L);
        Assert.assertTrue(creatorPage.getSubscribeToCreatorButtonFromPopUp().isDisplayed());
    }
    @When("user click on subscribe button from a chosen option")
    public void user_click_on_subscribe_button_from_a_chosen_option() {
        creatorPage.getSubscribeToCreatorButtonFromPopUp().click();
    }
    @Then("purchase successful pop-up is displayed")
    public void purchase_successful_pop_up_is_displayed() {
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                creatorPage.getPurchaseSuccessfulModal(), 30L);
        Assert.assertTrue(creatorPage.getPurchaseSuccessfulModal().isDisplayed());
    }
    @When("user click on continue button from the pop-up")
    public void user_click_on_continue_button_from_the_pop_up() {
            creatorPage.getContinueButtonFromModal().click();
    }
    @Then("congratulations pop-up is displayed with send message to creator option on it")
    public void congratulations_pop_up_is_displayed_with_send_message_to_creator_option_on_it(DataTable dataTable) {
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                creatorPage.getCongratulationsPopUp(), 30L);
        Assert.assertTrue(creatorPage.getCongratulationsPopUp().isDisplayed());
        String message = dataTable.cell(1, 0);
        creatorPage.getCongratulationsPopUpTextArea().sendKeys(message);
        Wait.untilElementIsClickable(testContext.getDriverManager().getWebDriver(),
                creatorPage.getSendMessageButton(), 20L);
        creatorPage.getSendMessageButton().click();
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                creatorPage.getMessageSentSuccessfulModal(), 30L);
        Assert.assertTrue(creatorPage.getMessageSentSuccessfulModal().isDisplayed());
        creatorPage.getCloseMessageSentSuccessfulModal().click();
    }

    @And("user is subscribed to the creator")
    public void userIsSubscribedToTheCreator() {
        Utilities.moveToWebElement(testContext.getDriverManager().getWebDriver(), mainWeb.getUserMenu());
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                mainWeb.getMyAccountBtn(), 30L);
        mainWeb.getMyAccountBtn().click();
        testContext.getDriverManager().getWebDriver().navigate().refresh();
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                userAccount.getActiveSubscriptionsBtn(), 30L);
        userAccount.getActiveSubscriptionsBtn().click();
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                userAccount.getCreatorActiveSubscriptionValue(), 40L);
        Assert.assertTrue(userAccount.getCreatorActiveSubscriptionValue().isDisplayed());
    }

    @Given("user is logged in")
    public void userIsLoggedIn(DataTable dataTable) {
        String username = dataTable.cell(1, 0);
        String password = dataTable.cell(1, 1);
        signIn(username, password);
    }

    @When("user navigate to Active subscriptions section")
    public void userNavigateToActiveSubscriptionsSection() {
        try {
            Utilities.moveToElement(testContext.getDriverManager().getWebDriver(), mainWeb.getUserMenu());
        }catch (StaleElementReferenceException e){
            Utilities.moveToElement(testContext.getDriverManager().getWebDriver(), mainWeb.getUserMenu());
        }
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                mainWeb.getMyAccountBtn(), 20L);
        mainWeb.getMyAccountBtn().click();
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                userAccount.getActiveSubscriptionsBtn(), 20L);
        userAccount.getActiveSubscriptionsBtn().click();
    }

    @And("click on cancel subscription option")
    public void clickOnCancelSubscriptionOption() {
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                userAccount.getCellMenuFromCreatorSubscriptions(), 30L);
        userAccount.clickCancelSubscriptionOption();
    }

    @Then("proceed to cancel pop-up is displayed")
    public void proceedToCancelPopUpIsDisplayed() {
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                userAccount.getProceedToCancelSubscriptionBtn(), 30L);
        Assert.assertTrue(userAccount.getProceedToCancelSubscriptionBtn().isDisplayed());
    }

    @When("user click on proceed to cancel button")
    public void userClickOnProceedToCancelButton() {
        userAccount.getProceedToCancelSubscriptionBtn().click();
    }

    @Then("provide reason pop-up is displayed")
    public void provideReasonPopUpIsDisplayed() {
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                userAccount.getSelectReasonDropDown(), 30L);
        Assert.assertTrue(userAccount.getSelectReasonDropDown().isDisplayed());
    }

    @When("user provide reason")
    public void userProvideReason() {
        userAccount.selectReason();
    }

    @And("select a rate for the creator")
    public void selectARateForTheCreator() {
        userAccount.getRatingStar().click();
    }

    @And("click on cancel subscription button from the provide reason pop-up")
    public void clickOnCancelSubscriptionButtonFromTheProvideReasonPopUp() {
        userAccount.getCancelSubscriptionBtn().click();
    }

    @Then("subscription canceled pop-up is displayed")
    public void subscriptionCanceledPopUpIsDisplayed() {
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                userAccount.getSubscriptionCanceledSuccessfulModal(), 20L);
        Assert.assertTrue(userAccount.getSubscriptionCanceledSuccessfulModal().isDisplayed());
    }

    @And("subscription is not present in the active section")
    public void subscriptionIsNotPresentInTheActiveSection() {
        userAccount.getCloseSubscriptionCanceledSuccessfulModalBtn().click();
        testContext.getDriverManager().getWebDriver().navigate().refresh();
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                userAccount.getActiveSubscriptionsBtn(), 20L);
        userAccount.getActiveSubscriptionsBtn().click();
        try{
            testContext.getDriverManager().getWebDriver().manage().timeouts().implicitlyWait(2L, TimeUnit.SECONDS);
            userAccount.getCreatorActiveSubscriptionValue().isDisplayed();
            Assert.fail();
        }catch (NoSuchElementException e){
            Assert.assertTrue(true);
        }
    }
    private void signIn(String username, String password){
        try {
            cookiesPopUp.clickRejectAllCookiesBtn();
            mainWeb.clickSignInBtn();
            Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                    logInForm.getUsernameField(), 30L);
            logInForm.getUsernameField().sendKeys(username);
            logInForm.getPasswordField().sendKeys(password);
            logInForm.clickLogInBtn();
        }catch (StaleElementReferenceException e){
            mainWeb.clickSignInBtn();
            Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                    logInForm.getUsernameField(), 30L);
            logInForm.getUsernameField().sendKeys(username);
            logInForm.getPasswordField().sendKeys(password);
            logInForm.clickLogInBtn();
        }
    }
}
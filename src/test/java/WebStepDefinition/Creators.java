package WebStepDefinition;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;
import pageObjects.webPages.CookiesPopUp;
import pageObjects.webPages.CreatorPage;
import pageObjects.webPages.CreatorsListing;
import pageObjects.webPages.MainWeb;
import utilities.TestContext;
import utilities.Utilities;
import utilities.Wait;

import java.util.Random;

import static enums.Context.*;

public class Creators {
    private TestContext testContext;
    private MainWeb mainWeb;
    private CreatorsListing creatorsListing;
    private CookiesPopUp cookiesPopUp;
    private CreatorPage creatorPage;

    public Creators(TestContext testContext) {
        this.testContext = testContext;
        mainWeb = testContext.getPageObjectManager().getMainWeb();
        creatorsListing = testContext.getPageObjectManager().getCreatorsListing();
        cookiesPopUp = testContext.getPageObjectManager().getCookiesPopUp();
        creatorPage = testContext.getPageObjectManager().getCreatorPage();
    }

    @Given("user is on the creators listing page - gay")
    public void userIsOnTheCreatorsListingPageGay() {
        cookiesPopUp.clickRejectAllCookiesBtn();
        mainWeb.moveToChooseGender();
        mainWeb.clickGayOrientationBtn();
        mainWeb.clickCreatorsButton();
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                creatorsListing.getUpdateButton(), 30L);
        Assert.assertTrue(creatorsListing.getUpdateButton().isDisplayed());
    }

    @Given("user is on the creators listing page - trans")
    public void userIsOnTheCreatorsListingPageTrans() {
        cookiesPopUp.clickRejectAllCookiesBtn();
        mainWeb.moveToChooseGender();
        mainWeb.clickOnTransOrientationBtn();
        mainWeb.clickCreatorsButton();
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                creatorsListing.getUpdateButton(), 30L);
        Assert.assertTrue(creatorsListing.getUpdateButton().isDisplayed());
    }

    @Given("user is on the creators listing page")
    public void user_is_on_the_creators_listing_page() {
        cookiesPopUp.clickRejectAllCookiesBtn();
        mainWeb.clickCreatorsButton();
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                creatorsListing.getUpdateButton(), 30L);
        Assert.assertTrue(creatorsListing.getUpdateButton().isDisplayed());
    }

    @When("user click on the update button")
    public void user_click_on_the_update_button() {
        creatorsListing.clickUpdateBtn();
    }

    @Then("page is refreshed")
    public void page_is_refreshed(DataTable dataTable) {
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                creatorsListing.getCreatorValue(), 30L);
        Assert.assertTrue(creatorsListing.getCreatorValue().isDisplayed());
        String url = dataTable.cell(1, 0);
        String currentUrl = testContext.getDriverManager().getWebDriver().getCurrentUrl();
        Assert.assertEquals(url, currentUrl);
    }

    @When("user goes to next page by clicking on next button")
    public void user_goes_to_next_page_by_clicking_on_next_button() {
        creatorsListing.clickNextPageBtn();
    }

    @Then("pages are changed")
    public void pages_are_changed(DataTable dataTable) {
        String numberOfPage = dataTable.cell(0, 0);
        String currentUrl = testContext.getDriverManager().getWebDriver().getCurrentUrl();
        System.out.println("numberOfPage ===> " + numberOfPage);
        System.out.println("currentUrl ===> " + currentUrl);
        Assert.assertTrue(currentUrl.contains(numberOfPage));
    }

    @When("user opens randomly a model page")
    public void user_opens_randomly_a_model_page() {
        int index = generateIndex();
        creatorsListing.clickOnCreatorElement(index);
    }

    @Then("model page is displayed")
    public void model_page_is_displayed() {
        Assert.assertTrue(creatorPage.getModelPoster().isDisplayed());
    }

    @When("user goes back to creators page")
    public void user_goes_back_to_creators_page() {
        testContext.getDriverManager().getWebDriver().navigate().back();
        Utilities.clickRejectCookiesIfDisplayed(testContext, cookiesPopUp);
    }

    @When("user click on Live cams button from creator listing page")
    public void user_click_on_live_cams_button_from_creator_listing_page() {
        creatorsListing.clickLiveCamsBtn();
    }

    @Then("is redirected to live cams page")
    public void is_redirected_to_live_cams_page(DataTable dataTable) {
        String url = dataTable.cell(1, 0);
        String currentUrl = testContext.getDriverManager().getWebDriver().getCurrentUrl();
        Assert.assertTrue(currentUrl.contains(url));
    }

    @When("user click on top rankings button")
    public void user_click_on_top_rankings_button() {
        creatorsListing.clickTopRankingsBtn();
    }

    @Then("top rankings section is displayed")
    public void top_rankings_section_is_displayed(DataTable dataTable) {
        String text = dataTable.cell(1, 0);
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                creatorsListing.getTopRankingsBtn(), 30L);
        Assert.assertTrue(creatorsListing.getListingHeader().getText().contains(text));
    }

    @When("user click on the sinparty logo")
    public void user_click_on_the_sinparty_logo() {
        mainWeb.clickSinPartyLogo();
    }

    @Then("user is redirected to the main page")
    public void user_is_redirected_to_the_main_page() {
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                mainWeb.getFeaturedCreatorLink(), 30L);
        Assert.assertTrue(mainWeb.getFeaturedCreatorLink().isDisplayed());
    }

    @When("user navigates back to the Top Rankings page")
    public void user_navigates_back_to_the_top_rankings_page() {
        mainWeb.clickCreatorsButton();
        creatorsListing.clickTopRankingsBtn();
    }

    @When("user click on {string} button")
    public void user_click_on_type_button(String type) {
        testContext.getScenarioContext().setContext(TYPE, type);
        switch (type) {
            case "Trending":
                creatorsListing.clickTrendingBtn();
                break;
            case "Most Viewed":
                creatorsListing.clickMostViewedBtn();
                break;
            case "Newest":
                creatorsListing.clickNewestBtn();
                break;
            case "Alphabetic":
                creatorsListing.clickAlphabeticalBtn();
                break;
        }
    }

    @Then("page is displayed")
    public void page_is_displayed() {
        String type = testContext.getScenarioContext().getContext(TYPE).toString();
        Assert.assertTrue(creatorsListing.getListingHeader().getText().contains(type));
    }

    @When("user click on random button")
    public void userClickOnRandomButton() {
        creatorsListing.clickRandomBtn();
    }

    @Then("user is redirected to a random model page")
    public void userIsRedirectedToARandomModelPage() {
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                creatorPage.getModelPoster(), 30L);
        Assert.assertTrue(creatorPage.getModelPoster().isDisplayed());
    }

    @When("user click on featured {string}")
    public void userClickOnFeaturedButton(String button) {
        switch (button){
            case "Featured":
                creatorsListing.clickFeaturedCreatorsBtn();
                break;
            case "Premium":
                creatorsListing.clickPremiumCreatorsBtn();
                break;
        }
    }
    @Then("user is redirected to featured section {string}")
    public void userIsRedirectedToFeaturedSection(String testData) {
        String currentUrl = testContext.getDriverManager().getWebDriver().getCurrentUrl();
        Assert.assertTrue(currentUrl.contains(testData));
    }

    @When("user click on gender {string}")
    public void userClickOnGenderButton(String button) {
        switch (button){
            case "Male":
                creatorsListing.clickMaleGenderBtn();
                break;
            case "Female":
                creatorsListing.clickFemaleGenderBtn();
                break;
        }
    }

    @Then("user is redirected to selected gender section {string}")
    public void userIsRedirectedToSelectedGenderSectionTestData(String testData) {
        String currentUrl = testContext.getDriverManager().getWebDriver().getCurrentUrl();
        Assert.assertTrue(currentUrl.contains(testData));
    }

    @When("creators hover on creator live now container")
    public void creatorsHoverOnCreatorLiveNowContainer() {
        Utilities.scrollWindow(testContext.getDriverManager().getWebDriver(),
                0, 4300);
        Utilities.moveToWebElement(testContext.getDriverManager().getWebDriver(),
                creatorsListing.getScrollLiveCreatorsContainer());
    }

    @Then("scroll button is displayed")
    public void scrollButtonIsDisplayed() {
        Utilities.clickRejectCookiesIfDisplayed(testContext, cookiesPopUp);
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                creatorsListing.getScrollNextLiveCreatorBtn(), 20L);
        Assert.assertTrue(creatorsListing.getScrollNextLiveCreatorBtn().isDisplayed());
    }

    @When("user click on see all creator live now")
    public void userClickOnSeeAllCreatorLiveNow() {
        creatorsListing.clickSeeAllCreatorsOnlineNow();
    }

    @Then("user is redirected to live creators page")
    public void userIsRedirectedToLiveCreatorsPage(DataTable dataTable) {
        String url = dataTable.cell(0,0);
        String currentUrl = testContext.getDriverManager().getWebDriver().getCurrentUrl();
        Assert.assertTrue(currentUrl.contains(url));
    }
    private int generateIndex() {
        Random random = new Random();
        return random.nextInt(10);
    }
}
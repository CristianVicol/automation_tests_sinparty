package WebStepDefinition;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;
import pageObjects.webPages.CookiesPopUp;
import pageObjects.webPages.PrivacyPolicy;
import pageObjects.webPages.MainWeb;
import utilities.TestContext;

import java.util.List;

public class PopUpCookiesDefinition {
    TestContext testContext;
    MainWeb webMain;
    CookiesPopUp cookiesPopUp;
    PrivacyPolicy privacyPolicy;

    public PopUpCookiesDefinition(TestContext testContext){
        this.testContext = testContext;
        webMain = testContext.getPageObjectManager().getMainWeb();
        cookiesPopUp = testContext.getPageObjectManager().getCookiesPopUp();
        privacyPolicy = testContext.getPageObjectManager().getPrivacyPolicy();
    }
    @Given("user access the website")
    public void user_access_the_website() {
        Assert.assertTrue(webMain.isDisplayed());
    }
    @Then("pop-up cookies is displayed with all elements on it")
    public void popUpCookiesIsDisplayedWithAllElementsOnIt() {
        Assert.assertTrue(cookiesPopUp.pupUpCookiesAreDisplayed());
    }
    @When("user click on learn more link")
    public void user_click_on_learn_more_link() {
        cookiesPopUp.clickLearnMoreLink();
    }
    @Then("a new window open with privacy policy page at cookies section")
    public void a_new_window_open_with_privacy_policy_page(DataTable dataTable) {
        List<String> daraRow = dataTable.row(1);
        String nameOfCookiesSection = daraRow.get(0);
        String linkPrivacyPolicy = daraRow.get(1);
        String currentUrl = testContext.getDriverManager().getWebDriver().getCurrentUrl();
        Assert.assertEquals(privacyPolicy.getTitleCookiesSectionFromPrivacyPolicyPage().getText(),
                nameOfCookiesSection);
        Assert.assertTrue(currentUrl.contains(linkPrivacyPolicy));
    }

    @When("user click on cookie settings button")
    public void userClickOnCookieSettingsButton() {
        cookiesPopUp.clickCookiesSettingsBtn();
    }

    @Then("a new pop-up opens with cookie settings")
    public void aNewPopUpOpensWithCookieSettings() {
        Assert.assertTrue(cookiesPopUp.getCancelFromCookiesSettingsBtn().isDisplayed());
        Assert.assertTrue(cookiesPopUp.getSaveFromCookiesSettingsBtn().isDisplayed());
    }

    @When("user click on cancel button")
    public void userClickOnCancelButton() {
        cookiesPopUp.clickCancelFromCookiesSettingsBtn();
    }
    @When("click on save button")
    public void clickOnSaveButton() {
        cookiesPopUp.clickSaveFromCookiesSettingsBtn();
    }

    @Then("main website is displayed")
    public void mainWebsiteIsDisplayed() {
        Assert.assertTrue(webMain.isDisplayed());
    }

    @When("user click on reject all button")
    public void userClickOnRejectAllButton() {
        cookiesPopUp.clickRejectAllCookiesBtn();
    }
}

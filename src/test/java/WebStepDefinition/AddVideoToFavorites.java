package WebStepDefinition;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pageObjects.webPages.CookiesPopUp;
import pageObjects.webPages.LogInForm;
import pageObjects.webPages.MainWeb;
import pageObjects.webPages.MyVideos;
import utilities.TestContext;
import utilities.Utilities;
import utilities.Wait;

import static enums.Context.TEXT;

public class AddVideoToFavorites {
    TestContext testContext;
    CookiesPopUp cookiesPopUp;
    MainWeb mainWeb;
    LogInForm logInForm;
    MyVideos myVideos;

    public AddVideoToFavorites(TestContext testContext) {
        this.testContext = testContext;
        cookiesPopUp = testContext.getPageObjectManager().getCookiesPopUp();
        mainWeb = testContext.getPageObjectManager().getMainWeb();
        logInForm = testContext.getPageObjectManager().getLogInForm();
        myVideos = testContext.getPageObjectManager().getMyVideos();
    }

    @Given("user is logged in on the main page")
    public void user_is_logged_in_on_the_main_page(DataTable dataTable) {
        String username = dataTable.cell(1, 0);
        String password = dataTable.cell(1, 1);
        cookiesPopUp.clickRejectAllCookiesBtn();
        mainWeb.clickSignInBtn();
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                logInForm.getUsernameField(), 30L);
        logInForm.getUsernameField().sendKeys(username);
        logInForm.getPasswordField().sendKeys(password);
        logInForm.clickLogInBtn();
        Utilities.clickRejectCookiesIfDisplayed(testContext, cookiesPopUp);
    }

    @When("user click on add to favorites icon from a video")
    public void user_click_on_add_to_favorites_icon_from_a_video() {
        WebElement videoTitle = mainWeb.getVideoValueTitle().get(1);
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                mainWeb.getUserMenu(), 30L);
        try {
            Utilities.scrollWindow(testContext.getDriverManager().getWebDriver(),
                    0, 1000);
            testContext.getScenarioContext().setContext(TEXT, videoTitle.getText());
            mainWeb.clickAddToFavoritesIcon();
        } catch (StaleElementReferenceException e) {
            testContext.getScenarioContext().setContext(TEXT, videoTitle.getText());
            mainWeb.clickAddToFavoritesIcon();
        }
    }

    @Then("add to pop-up is displayed")
    public void add_to_pop_up_is_displayed() {
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                mainWeb.getAddToFavoritesPopUpBtn(), 20L);
        Assert.assertTrue(mainWeb.getAddToFavoritesPopUpBtn().isDisplayed());
    }

    @When("user click on the playlist")
    public void user_click_on_the_playlist() {
        mainWeb.getPlaylist().click();
    }

    @When("click on add button")
    public void click_on_add_button() {
        mainWeb.getAddToFavoritesPopUpBtn().click();
    }

    @Then("video added to playlist notification is displayed")
    public void video_added_to_playlist_notification_is_displayed() {
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                mainWeb.getVideoAddedToFavoritesNotification(), 20L);
        Assert.assertTrue(mainWeb.getVideoAddedToFavoritesNotification().isDisplayed());

    }

    @Then("the video is present in my favorites videos section")
    public void the_video_is_present_in_my_favorites_videos_section() {
        mainWeb.clickMyVideoBtn();
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                myVideos.getFavoriteVideoValue(), 50L);
        Assert.assertEquals(myVideos.getVideoTitle().getText(),
                testContext.getScenarioContext().getContext(TEXT).toString());
    }

    @When("user in on the home page")
    public void userInOnTheHomePage() {
        cookiesPopUp.clickRejectAllCookiesBtn();
        Assert.assertTrue(mainWeb.getSignInBtn().isDisplayed());
    }

    @Then("pop-up is displayed where log in is advised")
    public void popUpIsDisplayedWhereLogInIsAdvised() {
            Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                    mainWeb.getAccountRequiredModal(), 3L);
            Assert.assertTrue(mainWeb.getAccountRequiredModal().isDisplayed());
    }

    @And("not authorized user click on add to favorites icon from a video")
    public void notAuthorizedUserClickOnAddToFavoritesIconFromAVideo() {
        Utilities.scrollWindow(testContext.getDriverManager().getWebDriver(),
                0, 1000);
        mainWeb.clickAddToFavoritesIcon();
    }
}
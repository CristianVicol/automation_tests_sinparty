package WebStepDefinition.brokenLinksChecks;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pageObjects.webPages.CookiesPopUp;
import pageObjects.webPages.LogInForm;
import pageObjects.webPages.MainWeb;
import utilities.TestContext;
import utilities.Wait;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

public class HomePage {
    TestContext testContext;
    MainWeb mainWeb;
    CookiesPopUp cookiesPopUp;
    LogInForm logInForm;
    public HomePage(TestContext testContext){
        this.testContext = testContext;
        mainWeb = testContext.getPageObjectManager().getMainWeb();
        cookiesPopUp = testContext.getPageObjectManager().getCookiesPopUp();
        logInForm = testContext.getPageObjectManager().getLogInForm();
    }
    @Given("not logged in user is on homepage")
    public void user_is_on_homepage() {
        cookiesPopUp.clickRejectAllCookiesBtn();
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                mainWeb.getSignInBtn(), 30L);
        Assert.assertTrue(mainWeb.getSignInBtn().isDisplayed());
    }
    @Then("links displayed on the page are not broken")
    public void links_displayed_on_the_page_are_not_broken() {
        List<WebElement> listOfLinks = mainWeb.getListOfLinks();
        try {
            checkStatusCodeOfLinks(listOfLinks);
        }catch (StaleElementReferenceException e){
            checkStatusCodeOfLinks(listOfLinks);
        }
    }

    @Given("user is on homepage")
    public void userIsOnHomepage(DataTable dataTable) {
        cookiesPopUp.clickAcceptCookiesBtn();
        List<String> dataRow = dataTable.row(1);
        String username = dataRow.get(0);
        String password = dataRow.get(1);
        mainWeb.clickSignInBtn();
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                logInForm.getUsernameField(), 30L);
        logInForm.getUsernameField().sendKeys(username);
        logInForm.getPasswordField().sendKeys(password);
        logInForm.clickLogInBtn();
    }
    private void checkStatusCodeOfLinks(List<WebElement> listOfLinks){
        listOfLinks.forEach(link -> {
            String url = link.getAttribute("href");
            try {
                HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();
                conn.setRequestMethod("HEAD");
                conn.connect();
                int responseCode = conn.getResponseCode();
                System.out.println(url + " ===> " + responseCode);
                if(responseCode >= 400){
                    System.out.println("The link ===> " + url + " is broken!");
                    Assert.fail("Broken link was found!!!");
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
    }
}
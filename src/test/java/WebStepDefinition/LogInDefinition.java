package WebStepDefinition;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.StaleElementReferenceException;
import org.testng.Assert;
import pageObjects.authenticationForms.WebGoogleAuthentication;
import pageObjects.webPages.*;
import utilities.TestContext;
import utilities.Utilities;
import utilities.Wait;

import java.util.List;

import static enums.Context.USERNAME;

public class LogInDefinition {
    TestContext testContext;
    MainWeb mainWeb;
    CookiesPopUp cookiesPopUp;
    LogInForm logInForm;
    WebGoogleAuthentication webGoogleAuthentication;

    public LogInDefinition(TestContext testContext) {
        this.testContext = testContext;
        webGoogleAuthentication = testContext.getPageObjectManager().getGoogleAuthentication();
        mainWeb = testContext.getPageObjectManager().getMainWeb();
        cookiesPopUp = testContext.getPageObjectManager().getCookiesPopUp();
        logInForm = testContext.getPageObjectManager().getLogInForm();
    }
    @Given("Website is displayed")
    public void websiteIsDisplayed() {
        Assert.assertTrue(mainWeb.isDisplayed());
    }

    @When("user click on sign in button")
    public void userClickOnSignInButton() {
        cookiesPopUp.clickAcceptCookiesBtn();
        mainWeb.clickSignInBtn();
    }

    @When("input credentials into username and password field")
    public void inputCredentialsIntoUsernameAndPasswordField(DataTable dataTable) {
        List<String> dataRow = dataTable.row(1);
        String username = dataRow.get(0);
        String password = dataRow.get(1);
        testContext.getScenarioContext().setContext(USERNAME, username);
        logInForm.getUsernameField().sendKeys(username);
        logInForm.getPasswordField().sendKeys(password);
    }
    @And("click on Login button")
    public void clickOnLoginButton() {
        logInForm.clickLogInBtn();
    }

    @Then("user is Logged in")
    public void userIsLoggedIn() {
        Utilities.sleep(3000);
        String expectedUserName = testContext.getScenarioContext().getContext(USERNAME).toString();
        String actualUserName = mainWeb.getUserNameTitle().getText();
        Assert.assertEquals(actualUserName, expectedUserName);
//        try {
//        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
//                mainWeb.getUserNameTitle(), 30L);
//        String actualUserName = mainWeb.getUserNameTitle().getText();
//            Assert.assertEquals(actualUserName, expectedUserName);
//        }catch (StaleElementReferenceException e){
//            String actualUserName = mainWeb.getUserNameTitle().getText();
//            Assert.assertEquals(actualUserName, expectedUserName);
//        }
    }

    @Then("user not found pop-up is displayed")
    public void userNotFoundPopUpIsDisplayed() {
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                logInForm.getUsernameNotFoundPopUp(), 10L);
        Assert.assertTrue(logInForm.getUsernameNotFoundPopUp().isDisplayed());
    }

    @And("click on on Login with Google button")
    public void clickOnOnLoginWithGoogleButton() {
        logInForm.clickLogInWithGoogleBtn();
    }

    @Then("google authentication page is displayed")
    public void webGoogleAuthenticationPageIsDisplayed() {
        Assert.assertTrue(webGoogleAuthentication.googleAuthenticationPageIsDisplayed());
    }

    @When("user complete authentication google page")
    public void userCompleteAuthenticationGooglePage(DataTable dataTable) {
        List<String> dataRow = dataTable.row(1);
        String email = dataRow.get(0);
        String password = dataRow.get(1);
        String username = dataRow.get(2);
        testContext.getScenarioContext().setContext(USERNAME, username);
        webGoogleAuthentication.getEmailField().sendKeys(email);
        webGoogleAuthentication.getNextBtn().click();
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                webGoogleAuthentication.getPasswordField(), 15L);
        webGoogleAuthentication.getPasswordField().sendKeys(password);
        webGoogleAuthentication.getNextBtn().click();
    }
}
package WebStepDefinition;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.StaleElementReferenceException;
import org.testng.Assert;
import pageObjects.webPages.CookiesPopUp;
import pageObjects.webPages.LogInForm;
import pageObjects.webPages.MainWeb;
import pageObjects.webPages.UserAccount;
import utilities.TestContext;
import utilities.Utilities;
import utilities.Wait;

public class ChangeLanguageDef {
    TestContext testContext;
    MainWeb mainWeb;
    CookiesPopUp cookiesPopUp;
    LogInForm logInForm;
    UserAccount userAccount;
    public ChangeLanguageDef(TestContext testContext){
        this.testContext = testContext;
        mainWeb = testContext.getPageObjectManager().getMainWeb();
        cookiesPopUp = testContext.getPageObjectManager().getCookiesPopUp();
        logInForm = testContext.getPageObjectManager().getLogInForm();
        userAccount = testContext.getPageObjectManager().getUserAccount();
    }
    @Given("user is on the website main page - english language")
    public void user_is_on_the_website_main_page_english_language(DataTable dataTable) {
        String username = dataTable.cell(1, 0);
        String password = dataTable.cell(1, 1);
        try{
            logIn(username, password);
        }catch (StaleElementReferenceException e){
            logIn(username, password);
        }
    }
    @When("user click on one of the  {string} icon from the bottom of the page")
    public void user_click_on_one_of_the_icon_from_the_bottom_of_the_page(String language) {
        try{
            clickLanguage(language);
        }catch (StaleElementReferenceException e){
            clickLanguage(language);
        }
    }
    @Then("language of the website is changed to the selected one {string}")
    public void language_of_the_website_is_changed_to_the_selected_one(String url) {
        Assert.assertEquals(url, testContext.getDriverManager().getWebDriver().getCurrentUrl());
    }
    @When("user navigates to my account page")
    public void user_navigates_to_my_account_page() {
        Utilities.moveToElement(testContext.getDriverManager().getWebDriver(),
                mainWeb.getUserMenu());
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                mainWeb.getMyVideosBtn(), 30L);
        mainWeb.getMyAccountBtn().click();
    }
    @Then("the language remains unchanged {string}")
    public void the_language_remains_unchanged(String url) {
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                userAccount.getActiveSubscriptionsBtn(), 30L);
        Assert.assertEquals(testContext.getDriverManager().getWebDriver().getCurrentUrl(), url);
    }
    private void clickLanguage(String language) throws StaleElementReferenceException {
        switch (language){
            case "Deutsch":
                Utilities.moveToWebElement(testContext.getDriverManager().getWebDriver(),
                        mainWeb.getDeutschLanguage());
                mainWeb.getDeutschLanguage().click();
                break;
            case "Francais":
                Utilities.moveToWebElement(testContext.getDriverManager().getWebDriver(),
                        mainWeb.getFrenchLanguage());
                mainWeb.getFrenchLanguage().click();
                break;
            case "Spanish":
                Utilities.moveToWebElement(testContext.getDriverManager().getWebDriver(),
                        mainWeb.getSpanishLanguage());
                mainWeb.getSpanishLanguage().click();
                break;
        }
    }
    private void logIn(String username, String password) throws StaleElementReferenceException{
        cookiesPopUp.clickRejectAllCookiesBtn();
        mainWeb.clickSignInBtn();
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                logInForm.getUsernameField(), 30L);
        logInForm.getUsernameField().sendKeys(username);
        logInForm.getPasswordField().sendKeys(password);
        logInForm.clickLogInBtn();
        Utilities.clickRejectCookiesIfDisplayed(testContext, cookiesPopUp);
    }
}
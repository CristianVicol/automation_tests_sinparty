package WebStepDefinition;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.testng.Assert;
import pageObjects.webPages.CookiesPopUp;
import pageObjects.webPages.LogInForm;
import pageObjects.webPages.MainWeb;
import utilities.GmailReader;
import utilities.TestContext;
import utilities.Wait;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.concurrent.TimeUnit;

import static enums.Context.*;

public class ForgotPasswordWeb {
    TestContext testContext;
    MainWeb mainWeb;
    LogInForm logInForm;
    CookiesPopUp cookiesPopUp;

    public ForgotPasswordWeb(TestContext testContext) {
        this.testContext = testContext;
        mainWeb = testContext.getPageObjectManager().getMainWeb();
        logInForm = testContext.getPageObjectManager().getLogInForm();
        cookiesPopUp = testContext.getPageObjectManager().getCookiesPopUp();
    }

    @Given("user is on log form")
    public void user_is_on_log_form() {
        cookiesPopUp.clickAcceptCookiesBtn();
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                mainWeb.getSignInBtn(), 15L);
        mainWeb.clickSignInBtn();
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                logInForm.getUsernameField(), 15L);
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                logInForm.getPasswordField(), 15L);
        Assert.assertTrue(logInForm.getUsernameField().isDisplayed());
        Assert.assertTrue(logInForm.getPasswordField().isDisplayed());
    }

    @When("user click on forgot password link")
    public void user_click_on_forgot_password_link() {
        Wait.untilElementIsClickable(testContext.getDriverManager().getWebDriver(),
                logInForm.getForgotPasswordLink(), 25L);
        logInForm.getForgotPasswordLink().click();
    }

    @Then("forgot password pop-up is displayed for providing email")
    public void forgot_password_pop_up_is_displayed_for_providing_email() {
        try {
            Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                    logInForm.getForgotPasswordEmailField(), 25L);
            Assert.assertTrue(logInForm.getForgotPasswordEmailField().isDisplayed());
            Assert.assertTrue(logInForm.getResetPasswordBtn().isDisplayed());
        } catch (StaleElementReferenceException e) {
            System.out.println("CATCH BLOCK FROM DEFINITION");
            Assert.assertTrue(logInForm.getForgotPasswordEmailField().isDisplayed());
            Assert.assertTrue(logInForm.getResetPasswordBtn().isDisplayed());
        }
    }

    @When("user tap the email {string} into the field")
    public void user_tap_the_email_into_the_field(String email) {
        logInForm.getForgotPasswordEmailField().sendKeys(email);
        testContext.getScenarioContext().setContext(EMAIL, email);
    }

    @When("clicks on Reset Password button")
    public void clicks_on_reset_password_button() {
        Wait.untilElementIsClickable(testContext.getDriverManager().getWebDriver(),
                logInForm.getResetPasswordBtn(), 20L);
        logInForm.getResetPasswordBtn().click();
    }

    @Then("reset password link is sent to the email address provided from {string}")
    public void reset_password_link_is_sent_to_the_email_address_provided_from(String senderEmail) {
        try {
            GmailReader gmailReader = new GmailReader();
            String resetPasswordLink = gmailReader.getLinkFromInbox(senderEmail);
            System.out.println("RESET LINK ===>" + resetPasswordLink);
            testContext.getScenarioContext().setContext(LINK, resetPasswordLink);
        } catch (GeneralSecurityException | IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    @When("user access the link sent to email address")
    public void user_access_the_link_sent_to_email_address() {
        String resetPasswordLink = testContext.getScenarioContext().getContext(LINK).toString();
        testContext.getDriverManager().getWebDriver().get(resetPasswordLink);
    }

    @Then("user is redirected to the reset password section")
    public void user_is_redirected_to_the_reset_password_section() {
        try {
            GmailReader gmailReader = new GmailReader();
            gmailReader.deleteEmails();
        } catch (GeneralSecurityException | IOException e) {
            e.printStackTrace();
        }
        Assert.assertTrue(logInForm.getNewPasswordField().isDisplayed());
        Assert.assertTrue(logInForm.getConfirmPasswordField().isDisplayed());
    }

    @When("user insert new password into the fields")
    public void user_insert_new_password_into_the_fields() {
        String newPassword = logInForm.generatePassword();
        System.out.println("NEW PASSWORD ===> " + newPassword);
        testContext.getScenarioContext().setContext(NEW_PASSWORD, newPassword);
        logInForm.getNewPasswordField().sendKeys(newPassword);
        logInForm.getConfirmPasswordField().sendKeys(newPassword);
        logInForm.getResetPasswordBtn().click();
    }

    @Then("password was successfully reset")
    public void passwordWasSuccessfullyReset() {
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                logInForm.getPasswordSuccessfullyResetModal(), 20L);
        Assert.assertTrue(logInForm.getLoginButtonAfterResetPasswordIsDone().isDisplayed());
        logInForm.getLoginButtonAfterResetPasswordIsDone().click();
    }

    @Then("user is redirected to log in form")
    public void user_is_redirected_to_log_in_form() {
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                logInForm.getUsernameField(), 15L);
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                logInForm.getPasswordField(), 15L);
        Assert.assertTrue(logInForm.getUsernameField().isDisplayed());
        Assert.assertTrue(logInForm.getPasswordField().isDisplayed());
    }

    @Then("the user can log in with new password")
    public void the_user_can_log_in_with_new_password(DataTable dataTable) {
        logInForm.getUsernameField().sendKeys(testContext.getScenarioContext().getContext(EMAIL).toString());
        logInForm.getPasswordField().sendKeys(testContext.getScenarioContext().getContext(NEW_PASSWORD).toString());
        logInForm.clickLogInBtn();
        String username = dataTable.cell(1, 0);
        try {
            Assert.assertEquals(mainWeb.getUserNameTitle().getText(), username);
        } catch (StaleElementReferenceException e) {
            Assert.assertEquals(mainWeb.getUserNameTitle().getText(), username);
        }
    }

    @And("main page is displayed")
    public void mainPageIsDisplayed() {
        try {
            testContext.getDriverManager().getWebDriver().manage().timeouts().implicitlyWait(1L, TimeUnit.SECONDS);
            if (logInForm.getNewPasswordField().isDisplayed()) {
                Assert.fail("Reset password modal was displayed after reseting password");
            }
        } catch (NoSuchElementException e) {
            Assert.assertTrue(true);
        }
    }
}
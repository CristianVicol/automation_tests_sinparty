package WebStepDefinition;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pageObjects.webPages.CookiesPopUp;
import pageObjects.webPages.LiveCreatorsListing;
import pageObjects.webPages.MainWeb;
import utilities.TestContext;
import utilities.Utilities;
import utilities.Wait;

import java.util.List;
import java.util.Random;

import static enums.Context.*;

public class LiveCreators {
    private TestContext testContext;
    private MainWeb mainWeb;
    private LiveCreatorsListing liveCreatorsListing;
    private CookiesPopUp cookiesPopUp;

    public LiveCreators(TestContext testContext) {
        this.testContext = testContext;
        mainWeb = testContext.getPageObjectManager().getMainWeb();
        cookiesPopUp = testContext.getPageObjectManager().getCookiesPopUp();
        liveCreatorsListing = testContext.getPageObjectManager().getLiveCreatorsListing();
    }

    @Given("user is on live creators page")
    public void user_is_on_live_creators_page() {
        cookiesPopUp.clickRejectAllCookiesBtn();
        mainWeb.clickSinPartyLiveButton();
        Utilities.closePopUpIfDisplayed(testContext, liveCreatorsListing.getClosePopUpBtn());
    }

    @When("user click on Guys button")
    public void user_click_on_guys_button() {
        liveCreatorsListing.clickGuysBtn();
    }

    @Then("gay live porn page is displayed")
    public void gay_live_porn_page_is_displayed(DataTable dataTable) {
        String url = dataTable.cell(0, 0);
        String currentUrl = testContext.getDriverManager().getWebDriver().getCurrentUrl();
        Assert.assertTrue(currentUrl.contains(url));
    }

    @When("user click on girls button")
    public void user_click_on_girls_button() {
        liveCreatorsListing.clickGirlsBtn();
    }

    @Then("straight live porn page is displayed")
    public void straight_live_porn_page_is_displayed(DataTable dataTable) {
        String url = dataTable.cell(0, 0);
        String currentUrl = testContext.getDriverManager().getWebDriver().getCurrentUrl();
        Assert.assertTrue(currentUrl.contains(url));
    }

    @When("user click on new models button")
    public void user_click_on_new_models_button() {
        liveCreatorsListing.clickNewModelsBtn();
    }

    @Then("new models section is displayed")
    public void new_models_section_is_displayed(DataTable dataTable) {
        String urlPart = dataTable.cell(0, 0);
        String currentUrl = testContext.getDriverManager().getWebDriver().getCurrentUrl();
        Assert.assertTrue(currentUrl.contains(urlPart));
    }

    @When("user click on a live model")
    public void user_click_on_a_live_model() {
        int index = generateRandomNumber();
        testContext.getScenarioContext().setContext(CREATOR_NAME, liveCreatorsListing.getListOfLiveModels().get(index).getText());
        liveCreatorsListing.clickListOfLiveModelsElement(index);
    }

    @Then("live model page is displayed")
    public void live_model_page_is_displayed() {
        String textFromElement = testContext.getScenarioContext().getContext(CREATOR_NAME).toString().trim();
        String[] urlSplited = testContext.getDriverManager().getWebDriver().getCurrentUrl().split("/");
        Assert.assertTrue(textFromElement.toLowerCase().contains(urlSplited[urlSplited.length - 1]));
    }

    @When("user goes back to live models page")
    public void user_goes_back_to_live_models_page() {
        testContext.getDriverManager().getWebDriver().navigate().back();
    }

    @When("click on next button")
    public void click_on_next_button() {
        Utilities.scrollWindow(testContext.getDriverManager().getWebDriver(), 0, 3500);
        liveCreatorsListing.clickNextPageBtn();
    }

    @Then("next page with live models is displayed")
    public void next_page_with_live_models_is_displayed(DataTable dataTable) {
        String testData = dataTable.cell(0, 0);
        String url = testContext.getDriverManager().getWebDriver().getCurrentUrl();
        Assert.assertTrue(url.contains(testData));
    }

    @When("user click on gold show button")
    public void userClickOnGoldShowButton() {
        liveCreatorsListing.clickGoldShowBtn();
    }

    @When("user click on a random tag")
    public void userClickOnARandomTag() {
        int index = generateRandomNumber();
        String href = liveCreatorsListing.getListOfTags().get(index).getAttribute("href");
        testContext.getScenarioContext().setContext(HREF, href);
        liveCreatorsListing.clickListOfTagsElement(index);
    }

    @Then("user is redirected to the relevant page")
    public void userIsRedirectedToTheRelevantPage() {
        String href = testContext.getScenarioContext().getContext(HREF).toString();
        String url = testContext.getDriverManager().getWebDriver().getCurrentUrl();
        Assert.assertTrue(url.contains(href));
    }

    @When("user click on show less button")
    public void userClickOnShowLessButton() {
        Utilities.closePopUpIfDisplayed(testContext, liveCreatorsListing.getClosePopUpBtn());
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                liveCreatorsListing.getShowMoreOrLessTagsBtn(), 30L);
        liveCreatorsListing.clickShowMoreOrLessTagsBtn();
    }

    @Then("less tags are displayed")
    public void lessTagsAreDisplayed(DataTable dataTable) {
        String testData = dataTable.cell(0, 0);
        Assert.assertTrue(liveCreatorsListing.getShowMoreOrLessTagsBtn().getText().contains(testData));
    }

    @And("click on show more tags button")
    public void clickOnShowMoreTagsButton() {
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                liveCreatorsListing.getShowMoreOrLessTagsBtn(), 30L);
        liveCreatorsListing.clickShowMoreOrLessTagsBtn();
    }

    @Then("more tags are displayed")
    public void moreTagsAreDisplayed(DataTable dataTable) {
        String testData = dataTable.cell(0, 0);
        Assert.assertTrue(liveCreatorsListing.getShowMoreOrLessTagsBtn().getText().contains(testData));
    }

    @When("user click on Category filter button")
    public void userClickOnCategoryFilterButton() {
        liveCreatorsListing.clickCategoryFilterBtn();
    }

    @And("click on a random category")
    public void clickOnARandomCategory() {
        int index = generateRandomNumber();
        Utilities.closePopUpIfDisplayed(testContext, liveCreatorsListing.getClosePopUpBtn());
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                liveCreatorsListing.getListOfCategories().get(index), 20L);
        String textFromCategoryFilter = liveCreatorsListing.getListOfCategories().get(index).getText().toLowerCase()
                .replaceAll("[^a-zA-Z]", "");
        testContext.getScenarioContext().setContext(CATEGORY, textFromCategoryFilter);
        liveCreatorsListing.clickCheckBoxListOfCategoriesElement(index);
    }

    @Then("page is loaded with selected category")
    public void pageIsLoadedWithSelectedCategory() {
        Utilities.closePopUpIfDisplayed(testContext, liveCreatorsListing.getClosePopUpBtn());
        String textFromCategoryFilter = testContext.getScenarioContext().getContext(CATEGORY).toString();
        String currentUrl = testContext.getDriverManager().getWebDriver().getCurrentUrl();
        Assert.assertTrue(currentUrl.contains(textFromCategoryFilter));
    }

    @When("user click on show more categories button")
    public void userClickOnShowMoreCategoriesButton() {
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                liveCreatorsListing.getShowMoreOrShowLessCategoriesBtn(), 20L);
        liveCreatorsListing.clickShowMoreOrLessCategoriesBtn();
    }

    @Then("more categories are displayed")
    public void moreCategoriesAreDisplayed(DataTable dataTable) {
        // String testData = dataTable.cell(0,0);
        //  Assert.assertTrue(liveCreatorsListing.getShowMoreOrShowLessCategoriesBtn().getText().contains(testData));
    }

    @When("user click on show less categories button")
    public void userClickOnShowLessCategoriesButton() {
        liveCreatorsListing.clickShowMoreOrLessCategoriesBtn();
    }

    @Then("less categories are displayed")
    public void lessCategoriesAreDisplayed(DataTable dataTable) {
        // String testData = dataTable.cell(0, 0);
        // Assert.assertTrue(liveCreatorsListing.getShowMoreOrShowLessCategoriesBtn().getText().contains(testData));
    }

    @When("user click on region filter button")
    public void userClickOnRegionFilterButton() {
        Utilities.scrollWindow(testContext.getDriverManager().getWebDriver(), 0, 500);
        liveCreatorsListing.clickRegionFilterBtn();
    }

    @And("select a region")
    public void selectARegion() {
        int indexOfRegionElement = new Random().nextInt(5);
        testContext.getScenarioContext().setContext(INDEX, indexOfRegionElement);
        liveCreatorsListing.clickListOfRegionsElement(indexOfRegionElement);
    }

    @Then("live creator of selected region are displayed")
    public void liveCreatorOfSelectedRegionAreDisplayed() {
        int index = Integer.parseInt(testContext.getScenarioContext().getContext(INDEX).toString());
        String numberOfRegionThanWasSelected = Integer.toString(index + 1);
        String expressionToAssert = "D=" + numberOfRegionThanWasSelected;
        String currentUrl = testContext.getDriverManager().getWebDriver().getCurrentUrl();
        Assert.assertTrue(currentUrl.contains(expressionToAssert));
    }

    @When("user click on age filter button")
    public void userClickOnAgeFilterButton() {
        liveCreatorsListing.clickAgeFilterBtn();
    }

    @Then("live creatos of select age range are displayed")
    public void liveOfSelectAgeRangeAreDisplayed(DataTable dataTable) {
        List<String> dataRow = dataTable.row(0);
        int size = liveCreatorsListing.getListOfAges().size();
        int indexOfElement = new Random().nextInt(size - 1);
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                liveCreatorsListing.getListOfAges().get(indexOfElement), 20L);
        liveCreatorsListing.clickListOfAgesElement(indexOfElement);
        String currentUrl = testContext.getDriverManager().getWebDriver().getCurrentUrl();
        Assert.assertTrue(currentUrl.contains(dataRow.get(indexOfElement)));
    }

    @When("user click on language")
    public void userClickOnLanguage() {
        liveCreatorsListing.clickLanguageFilterBtn();
    }

    @Then("live creator of selected language is displayed")
    public void liveCreatorOfSelectedLanguageIsDisplayed(DataTable dataTable) {
        List<String> dataRow = dataTable.row(0);
        int size = liveCreatorsListing.getListOfLanguageElements().size();
        int indexOfElement = new Random().nextInt(size - 1);
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                liveCreatorsListing.getListOfLanguageElements().get(indexOfElement), 20L);
        liveCreatorsListing.clickListOfLanguageElement(indexOfElement);
        String currentUrl = testContext.getDriverManager().getWebDriver().getCurrentUrl();
        Assert.assertTrue(currentUrl.contains(dataRow.get(indexOfElement)));

    }

    @When("user click on show only new")
    public void userClickOnShowOnlyNew() {
        liveCreatorsListing.clickNewModelsFilterBtn();
        WebElement webElement = liveCreatorsListing.getListOfNewModelsFilterOptions().get(0);
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                webElement, 20L);
        webElement.click();
    }

    @Then("new models are displayed")
    public void newModelsAreDisplayed(DataTable dataTable) {
        String testData = dataTable.cell(0, 0);
        String currentUrl = testContext.getDriverManager().getWebDriver().getCurrentUrl();
        System.out.println("testData   " + testData);
        System.out.println("currentUrl   " + currentUrl);
        Assert.assertTrue(currentUrl.contains(testData));
    }

    @When("user click on show all")
    public void userClickOnShowAll() {
        WebElement webElement = liveCreatorsListing.getListOfNewModelsFilterOptions().get(1);
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(), webElement, 20L);
        webElement.click();
    }

    @Then("all models are displayed")
    public void allModelsAreDisplayed(DataTable dataTable) {
        String testData = dataTable.cell(0, 0);
        String currentUrl = testContext.getDriverManager().getWebDriver().getCurrentUrl();
        System.out.println("testData   " + testData);
        System.out.println("currentUrl   " + currentUrl);
        Assert.assertTrue(currentUrl.contains(testData));
    }

    @When("user choose a gender")
    public void userChooseAGender() {
        Utilities.scrollWindow(testContext.getDriverManager().getWebDriver(), 0, 800);
        liveCreatorsListing.clickGenderFilterBtn();
        int randomIndex = new Random().nextInt(1, 7);
        testContext.getScenarioContext().setContext(INDEX, randomIndex);
        liveCreatorsListing.clickListOfGenderFilterOptions(randomIndex);
    }

    @Then("creators of selected gender are displayed")
    public void creatorsOfSelectedGenderAreDisplayed(DataTable dataTable) {
        List<String> dataRow = dataTable.row(0);
        int randomIndex = (int) testContext.getScenarioContext().getContext(INDEX);
        String currentUrl = testContext.getDriverManager().getWebDriver().getCurrentUrl();
        Assert.assertTrue(currentUrl.contains(dataRow.get(randomIndex - 1)));
    }

    private int generateRandomNumber() {
        Random random = new Random();
        return random.nextInt(10);
    }
}
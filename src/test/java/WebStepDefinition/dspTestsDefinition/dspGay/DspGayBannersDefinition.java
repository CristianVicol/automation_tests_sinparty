package WebStepDefinition.dspTestsDefinition.dspGay;

import io.cucumber.java.en.When;
import pageObjects.webPages.*;
import pageObjects.webPages.dsp.DspElements;
import utilities.TestContext;
import utilities.Utilities;
public class DspGayBannersDefinition {
    TestContext testContext;
    DspElements dspElements;
    CookiesPopUp cookiesPopUp;
    MainWeb mainWeb;
    CreatorsListing creatorsListing;
    SearchPage searchPage;
    ChannelPage channelPage;
    LiveCreatorsListing liveCreatorsListing;
    CreatorPage creatorPage;

    public DspGayBannersDefinition(TestContext testContext){
        this.testContext = testContext;
        dspElements = testContext.getPageObjectManager().getDspElements();
        cookiesPopUp = testContext.getPageObjectManager().getCookiesPopUp();
        mainWeb = testContext.getPageObjectManager().getMainWeb();
        creatorsListing = testContext.getPageObjectManager().getCreatorsListing();
        searchPage = testContext.getPageObjectManager().getSearchPage();
        channelPage = testContext.getPageObjectManager().getChannelPage();
        liveCreatorsListing = testContext.getPageObjectManager().getLiveCreatorsListing();
        creatorPage = testContext.getPageObjectManager().getCreatorPage();
    }
    @When("user navigates on the gay home page")
    public void clickOnGayOrientationButton() {
        mainWeb.moveToChooseGender();
        mainWeb.clickGayOrientationBtn();
        cookiesPopUp.clickRejectAllCookiesBtn();
    }

    @When("user navigates on the gay creator video page")
    public void user_navigates_on_the_creator_video_page() {
        Utilities.preparePageForDspTests(testContext, dspElements, cookiesPopUp);
        mainWeb.moveToChooseGender();
        mainWeb.clickGayOrientationBtn();
        mainWeb.clickCreatorsButton();
        Utilities.clickRejectCookiesIfDisplayed(testContext, cookiesPopUp);
        creatorsListing.clickCreatorValue();
        Utilities.scrollWindow(testContext.getDriverManager().getWebDriver(),
                0, 300);
        creatorPage.clickCreatorClipValue();
    }

    @When("user navigates on the gay affiliate video page")
    public void userNavigatesOnTheGayAffiliateVideoPage() {
        mainWeb.moveToChooseGender();
        mainWeb.clickGayOrientationBtn();
        cookiesPopUp.clickRejectAllCookiesBtn();
        mainWeb.clickSearchBtn();
        searchPage.getChannelsBtn().click();
        searchPage.clickChannelVideoValue();
        Utilities.scrollWindow(testContext.getDriverManager().getWebDriver(), 0, 500);
        channelPage.clickVideoValue();
    }

    @When("user navigates to the gay creators listing page")
    public void userNavigatesToTheGayCreatorsListingPage() {
        mainWeb.moveToChooseGender();
        mainWeb.clickGayOrientationBtn();
        cookiesPopUp.clickRejectAllCookiesBtn();
        mainWeb.clickCreatorsButton();
    }

    @When("user navigates to the gay videos listing page")
    public void userNavigatesToTheGayVideosListingPage() {
        mainWeb.moveToChooseGender();
        mainWeb.clickGayOrientationBtn();
        cookiesPopUp.clickRejectAllCookiesBtn();
        mainWeb.clickVideosButton();
    }

    @When("user navigates to the gay categories listing page")
    public void userNavigatesToTheGayCategoriesListingPage() {
        mainWeb.moveToChooseGender();
        mainWeb.clickGayOrientationBtn();
        cookiesPopUp.clickRejectAllCookiesBtn();
        mainWeb.clickCategoriesButton();
    }

    @When("user navigates to the gay live creators listing page")
    public void userNavigatesToTheGayLiveCreatorsListingPage() {
        mainWeb.moveToChooseGender();
        mainWeb.clickGayOrientationBtn();
        cookiesPopUp.clickRejectAllCookiesBtn();
        mainWeb.clickSinPartyLiveButton();
        Utilities.clickRejectCookiesIfDisplayed(testContext, cookiesPopUp);
    }

    @When("user navigates to the gay live creator page")
    public void userNavigatesToTheGayLiveCreatorPage() {
        mainWeb.moveToChooseGender();
        mainWeb.clickGayOrientationBtn();
        cookiesPopUp.clickRejectAllCookiesBtn();
        mainWeb.clickSinPartyLiveButton();
        Utilities.clickRejectCookiesIfDisplayed(testContext, cookiesPopUp);
        liveCreatorsListing.clickLiveCreator();
    }
}
package WebStepDefinition.dspTestsDefinition;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import pageObjects.webPages.CookiesPopUp;
import pageObjects.webPages.MainWeb;
import pageObjects.webPages.dsp.HomePageDspElements;
import utilities.TestContext;
import utilities.Utilities;
import utilities.Wait;

public class DspHomepage {
    TestContext testContext;
    MainWeb mainWeb;
    CookiesPopUp cookiesPopUp;
    HomePageDspElements homePageDspElements;
    public DspHomepage(TestContext testContext){
        this.testContext = testContext;
        mainWeb = testContext.getPageObjectManager().getMainWeb();
        cookiesPopUp = testContext.getPageObjectManager().getCookiesPopUp();
        homePageDspElements = testContext.getPageObjectManager().getHomePageDspElements();
    }

    @When("user navigates to the homepage")
    public void user_navigates_to_the_homepage() {
        mainWeb.clickSinPartyLogo();
        cookiesPopUp.clickRejectAllCookiesBtn();
    }

    @Then("banner under creators spotlight is displayed")
    public void bannerUnderCreatorsSpotlightIsDisplayed() {
        Utilities.scrollWindow(testContext.getDriverManager().getWebDriver(), 0, 600);
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                homePageDspElements.getBannerUnderCreatorsSpotlight(), 20L);
        Assert.assertTrue(homePageDspElements.getBannerUnderCreatorsSpotlight().isDisplayed());
    }

    @Then("banner under popular on sinparty is displayed")
    public void bannerUnderPopularOnSinpartyIsDisplayed() {
        Utilities.scrollWindow(testContext.getDriverManager().getWebDriver(), 600, 1000);
        Assert.assertTrue(homePageDspElements.getBannerUnderPopularOnSinPartySection().isDisplayed());
    }

    @Then("video adv is displayed under trending now section")
    public void videoAdvIsDisplayedUnderTrendingNowSection() {
        new Actions(testContext.getDriverManager().getWebDriver())
                .moveToElement(homePageDspElements.getVideoAdvUnderTrendingNowSection()).perform();
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                homePageDspElements.getVideoAdvUnderTrendingNowSection(), 40L);
        Assert.assertTrue(homePageDspElements.getVideoAdvUnderTrendingNowSection().isDisplayed());
    }

    @Then("adv banners are displayed under trending now section")
    public void advBannersAreDisplayedUnderTrendingNowSection() {
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                homePageDspElements.getBannerUnderTrendingNowSection(), 10L);
        Assert.assertTrue(homePageDspElements.getBannerUnderTrendingNowSection().isDisplayed());
    }

    @Then("banner under paid videos section is displayed")
    public void bannerUnderPaidVideosSectionIsDisplayed() {
        Utilities.scrollWindow(testContext.getDriverManager().getWebDriver(), 1200, 1400);
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                homePageDspElements.getBannerUnderPaidVideosSection(), 10L);
        Assert.assertTrue(homePageDspElements.getVideoAdvUnderTrendingNowSection().isDisplayed());
    }

    @Then("banner under welcome to the party section is displayed")
    public void bannerUnderWelcomeToThePartySectionIsDisplayed() {
        Assert.assertTrue(homePageDspElements.getBannerUnderWelcomeToThePartySection().isDisplayed());
    }

    @Then("banner under most recent section is displayed")
    public void bannerUnderMostRecentSectionIsDisplayed() {
        Assert.assertTrue(homePageDspElements.getBannerUnderMostRecentSection().isDisplayed());
    }

    @Then("banner under creator categories is displayed")
    public void bannerUnderCreatorCategoriesIsDisplayed() {
        Assert.assertTrue(homePageDspElements.getBannerUnderCreatorsCategoriesSection().isDisplayed());
    }

    @Then("banner under the hottest section is displayed")
    public void bannerUnderTheHottestSectionIsDisplayed() {
        Assert.assertTrue(homePageDspElements.getBannerUnderTheHottestCreatorsSection().isDisplayed());
    }

    @And("click on trans orientation button")
    public void clickOnTransOrientationButton() {
        mainWeb.moveToChooseGender();
        mainWeb.clickOnTransOrientationBtn();
    }
}
package WebStepDefinition.dspTestsDefinition.dstTrans;

import io.cucumber.java.en.When;
import pageObjects.webPages.CookiesPopUp;
import pageObjects.webPages.MainWeb;
import utilities.TestContext;

public class DspTransBannersDefinition {
    TestContext testContext;
    MainWeb mainWeb;
    CookiesPopUp cookiesPopUp;

    public DspTransBannersDefinition(TestContext testContext){
        this.testContext = testContext;
        mainWeb = testContext.getPageObjectManager().getMainWeb();
        cookiesPopUp = testContext.getPageObjectManager().getCookiesPopUp();
    }
    @When("user navigates on the trans home page")
    public void user_navigates_on_the_trans_home_page() {
        mainWeb.moveToChooseGender();
        mainWeb.clickOnTransOrientationBtn();
        cookiesPopUp.clickRejectAllCookiesBtn();
    }
}
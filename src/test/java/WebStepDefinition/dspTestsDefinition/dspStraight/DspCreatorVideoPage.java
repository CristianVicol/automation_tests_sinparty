package WebStepDefinition.dspTestsDefinition.dspStraight;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;
import pageObjects.webPages.CookiesPopUp;
import pageObjects.webPages.CreatorPage;
import pageObjects.webPages.CreatorsListing;
import pageObjects.webPages.MainWeb;
import pageObjects.webPages.dsp.DspElements;
import utilities.TestContext;
import utilities.Utilities;
import utilities.Wait;

public class DspCreatorVideoPage {
    TestContext testContext;
    DspElements dspElements;
    CookiesPopUp cookiesPopUp;
    MainWeb mainWeb;
    CreatorsListing creatorsListing;
    CreatorPage creatorPage;

    public DspCreatorVideoPage(TestContext testContext){
        this.testContext = testContext;
        dspElements = testContext.getPageObjectManager().getDspElements();
        cookiesPopUp = testContext.getPageObjectManager().getCookiesPopUp();
        mainWeb = testContext.getPageObjectManager().getMainWeb();
        creatorsListing = testContext.getPageObjectManager().getCreatorsListing();
        creatorPage = testContext.getPageObjectManager().getCreatorPage();
    }
    @When("user navigates on the creator video page")
    public void user_navigates_on_the_creator_video_page() {
        Utilities.preparePageForDspTests(testContext, dspElements, cookiesPopUp);
        mainWeb.clickCreatorsButton();
        Utilities.clickRejectCookiesIfDisplayed(testContext, cookiesPopUp);
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                creatorsListing.getCreatorValue(), 30L);
        creatorsListing.clickCreatorValue();
        creatorPage.clickFreeVideoBtn();
        creatorPage.clickCreatorClipValue();
        dspElements.clickCloseVideoSliderBtn();
    }
    @Then("advertising banner is displayed on the right side of premium content section")
    public void advertisingBannerIsDisplayedOnTheRightSideOfPremiumContentSection() {
        Assert.assertTrue(dspElements.getAdvBannerOnSideOfFeed().isDisplayed());
    }

    @Then("advertising banner is displayed on the right side of the creators video")
    public void advertisingBannerIsDisplayedOnTheRightSideOfTheCreatorsVideo() {
        Assert.assertTrue(dspElements.getAdBannerOnTheRightSideOfTheVideo1().isDisplayed());
        Assert.assertTrue(dspElements.getAdBannerOnTheRightSideOfTheVideo2().isDisplayed());
    }
}
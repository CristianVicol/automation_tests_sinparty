package WebStepDefinition.dspTestsDefinition.dspStraight;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.NoSuchElementException;
import org.testng.Assert;
import pageObjects.webPages.CookiesPopUp;
import pageObjects.webPages.MainWeb;
import pageObjects.webPages.dsp.DspElements;
import pageObjects.webPages.dsp.DspElementsOnListingPages;
import utilities.TestContext;
import utilities.Utilities;
import utilities.Wait;

import java.util.concurrent.TimeUnit;

public class DspListingPages {
    TestContext testContext;
    DspElements dspElements;
    CookiesPopUp cookiesPopUp;
    MainWeb mainWeb;
    DspElementsOnListingPages dspElementsOnListingPages;

    public DspListingPages(TestContext testContext){
        this.testContext = testContext;
        dspElements = testContext.getPageObjectManager().getDspElements();
        cookiesPopUp = testContext.getPageObjectManager().getCookiesPopUp();
        mainWeb = testContext.getPageObjectManager().getMainWeb();
        dspElementsOnListingPages = testContext.getPageObjectManager().getDspElementsOnListingPages();
    }
    @When("user navigates to the creators listing page")
    public void user_navigates_to_the_creators_listing_page() {
        Utilities.preparePageForDspTests(testContext, dspElements, cookiesPopUp);
        mainWeb.clickCreatorsButton();
        Utilities.clickRejectCookiesIfDisplayed(testContext, cookiesPopUp);
    }

    @Then("first ad banner is displayed")
    public void firstAdBannerIsDisplayed() {
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                dspElementsOnListingPages.getAdBannerOne(), 20L);
        Utilities.scrollWindow(testContext.getDriverManager().getWebDriver(), 0, 500);
        Assert.assertTrue(dspElementsOnListingPages.getAdBannerOne().isDisplayed());
    }

    @Then("second ad banner is displayed")
    public void secondAdBannerIsDisplayed() {
        Assert.assertTrue(dspElementsOnListingPages.getAdBannerTwo().isDisplayed());
    }

    @Then("third ad banner is displayed")
    public void thirdAdBannerIsDisplayed() {
        Assert.assertTrue(dspElementsOnListingPages.getAdBannerThree().isDisplayed());
    }

    @Then("forth ad banner is displayed")
    public void forthAdBannerIsDisplayed() {
        Utilities.scrollWindow(testContext.getDriverManager().getWebDriver(), 500, 800);
        Assert.assertTrue(dspElementsOnListingPages.getAdBannerFour().isDisplayed());
    }

    @Then("banner on the left side of the page is displayed")
    public void bannerOnTheLeftSideOfThePageIsDisplayed() {
        Assert.assertTrue(dspElementsOnListingPages.getAdBannerOnTheLeftSide().isDisplayed());
    }

    @Then("video ad is displayed")
    public void videoAdIsDisplayed() {
        Utilities.moveToElement(testContext.getDriverManager().getWebDriver(),
                dspElementsOnListingPages.getVideoAd());
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                dspElementsOnListingPages.getVideoAd(), 20L);
        Assert.assertTrue(dspElementsOnListingPages.getVideoAd().isDisplayed());
    }

    @Then("ad banner on the end of the page is displayed")
    public void adBannerOnTheEndOfThePageIsDisplayed() {
        Assert.assertTrue(dspElementsOnListingPages.getAdBannerOnTheEndOfThePage().isDisplayed());
    }

    @When("user navigates to the videos listing page")
    public void userNavigatesToTheVideosListingPage() {
        Utilities.preparePageForDspTests(testContext, dspElements, cookiesPopUp);
        mainWeb.clickVideosButton();
        Utilities.clickRejectCookiesIfDisplayed(testContext, cookiesPopUp);
    }

    @When("user navigates to the categories listing page")
    public void userNavigatesToTheCategoriesListingPage() {
        Utilities.preparePageForDspTests(testContext, dspElements, cookiesPopUp);
        mainWeb.clickCategoriesButton();
        Utilities.clickRejectCookiesIfDisplayed(testContext, cookiesPopUp);
    }

    @When("user navigates to the live creators listing page")
    public void userNavigatesToTheLiveCreatorsListingPage() {
        Utilities.preparePageForDspTests(testContext, dspElements, cookiesPopUp);
        mainWeb.clickSinPartyLiveButton();
        Utilities.clickRejectCookiesIfDisplayed(testContext, cookiesPopUp);
    }
    @Then("fifth banner on the end of the page is displayed")
    public void fifthBannerOnTheEndOfThePageIsDisplayed() {
        Assert.assertTrue(dspElementsOnListingPages.getAdBannerFive().isDisplayed());
    }
}
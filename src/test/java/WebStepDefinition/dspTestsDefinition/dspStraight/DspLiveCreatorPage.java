package WebStepDefinition.dspTestsDefinition.dspStraight;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;
import pageObjects.webPages.CookiesPopUp;
import pageObjects.webPages.LiveCreatorsListing;
import pageObjects.webPages.MainWeb;
import pageObjects.webPages.dsp.DspElements;
import pageObjects.webPages.dsp.DspElementsLiveCreators;
import utilities.TestContext;
import utilities.Utilities;

public class DspLiveCreatorPage {
    TestContext testContext;
    DspElementsLiveCreators dspLiveCreatorPage;
    DspElements dspElements;
    CookiesPopUp cookiesPopUp;
    MainWeb mainWeb;
    LiveCreatorsListing liveCreatorsListing;
    public DspLiveCreatorPage(TestContext testContext){
        this.testContext = testContext;
        dspLiveCreatorPage = testContext.getPageObjectManager().getDspElementsLiveCreators();
        dspElements = testContext.getPageObjectManager().getDspElements();
        cookiesPopUp = testContext.getPageObjectManager().getCookiesPopUp();
        mainWeb = testContext.getPageObjectManager().getMainWeb();
        liveCreatorsListing = testContext.getPageObjectManager().getLiveCreatorsListing();
    }

    @When("user navigates to the live creator page")
    public void user_navigates_to_the_live_creator_page() {
        Utilities.preparePageForDspTests(testContext, dspElements, cookiesPopUp);
        mainWeb.clickSinPartyLiveButton();
        Utilities.clickRejectCookiesIfDisplayed(testContext, cookiesPopUp);
        liveCreatorsListing.clickLiveCreator();
    }
    @Then("ad banner under live creator video is displayed")
    public void ad_banner_under_live_creator_video_is_displayed() {
      Utilities.scrollWindow(testContext.getDriverManager().getWebDriver(), 0, 500);
      Assert.assertTrue(dspLiveCreatorPage.getAdBannerUnderLiveCreator().isDisplayed());
    }
    @Then("ad banner is displayed in the center of the page")
    public void ad_banner_is_displayed_in_the_center_of_the_page() {
        Assert.assertTrue(dspLiveCreatorPage.getAdBannerInTheCenterOfThePage().isDisplayed());
    }
    @Then("ad banner is displayed on the left side of the page")
    public void ad_banner_is_displayed_on_the_left_side_of_the_page() {
        Assert.assertTrue(dspLiveCreatorPage.getAdBannerOnTheLeftSide().isDisplayed());
    }
    @Then("ad banner is displayed on the right side of the page")
    public void ad_banner_is_displayed_on_the_right_side_of_the_page() {
        Assert.assertTrue(dspLiveCreatorPage.getAdBannerOnTheRightSide().isDisplayed());
    }
    @Then("video ad is displayed on the bottom of the page")
    public void video_ad_is_displayed_on_the_bottom_of_the_page() {
        Utilities.moveToElement(testContext.getDriverManager().getWebDriver(),
                dspLiveCreatorPage.getVideoOnTheBottom());
        Assert.assertTrue(dspLiveCreatorPage.getVideoOnTheBottom().isDisplayed());
    }
    @Then("ad banner is displayed on the bottom if the page")
    public void ad_banner_is_displayed_on_the_bottom_if_the_page() {
        Assert.assertTrue(dspLiveCreatorPage.getAdBannerOnTheBottom().isDisplayed());
    }
}
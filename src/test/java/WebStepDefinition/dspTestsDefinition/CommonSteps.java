package WebStepDefinition.dspTestsDefinition;

import hooks.Hooks;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.openqa.selenium.NoSuchElementException;
import org.testng.Assert;
import pageObjects.webPages.CookiesPopUp;
import pageObjects.webPages.dsp.DspElements;
import pageObjects.paidChannels.Fuq;
import utilities.Utilities;
import utilities.TestContext;
import utilities.Wait;


import java.util.concurrent.TimeUnit;

public class CommonSteps {
    TestContext testContext;
    Fuq fuq;
    CookiesPopUp cookiesPopUp;
    DspElements dspElements;
    public CommonSteps(TestContext testContext){
        this.testContext = testContext;
        fuq = testContext.getPageObjectManager().getPaidChannel();
        cookiesPopUp = testContext.getPageObjectManager().getCookiesPopUp();
        dspElements = testContext.getPageObjectManager().getDspElements();
    }

    @Given("user access the site from a paid channel {string}")
    public void user_access_the_site_from_a_paid_channel(String url) {
        if(Hooks.shouldSkipBefore()) {
            testContext.getDriverManager().getWebDriver().get(url);
            fuq.getConfirmAgeBtn().click();
            fuq.searchForSinParty();
            String originalWindow = testContext.getDriverManager().getWebDriver().getWindowHandle();
           // testContext.getScenarioContext().setContext(WINDOW, originalWindow);
            fuq.clickVideo();
            Utilities.switchWindow(testContext.getDriverManager().getWebDriver(), originalWindow);
        }
    }
    @Then("user close the ad video slider immediately by clicking on cross button")
    public void user_close_the_ad_video_slider_immediately_by_clicking_on_cross_button() {
        Utilities.preparePageForDspTests(testContext, dspElements, cookiesPopUp);
        try{
            testContext.getDriverManager().getWebDriver().manage().timeouts().implicitlyWait(1L, TimeUnit.SECONDS);
            Assert.assertFalse(dspElements.getCloseVideoSliderBtn().isDisplayed());
        }catch (NoSuchElementException e){
            Assert.assertTrue(true);
        }
    }
    @Then("advertising banner is displayed on the right side of the video")
    public void advertisingBannerIsDisplayedOnTheRightSideOfTheVideo() {
        Utilities.preparePageForDspTests(testContext, dspElements, cookiesPopUp);
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                dspElements.getAdBannerOnTheRightSideOfTheVideo1(), 20L);
        Assert.assertTrue(dspElements.getAdBannerOnTheRightSideOfTheVideo1().isDisplayed());
        Assert.assertTrue(dspElements.getAdBannerOnTheRightSideOfTheVideo2().isDisplayed());
    }
    @Then("first advertising banner is displayed")
    public void firstAdvertisingBannerIsDisplayed() {
        Utilities.scrollWindow(testContext.getDriverManager().getWebDriver(),0, 500);
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                dspElements.getAdvBannerUnderVideo(), 20L);
        Assert.assertTrue(dspElements.getAdvBannerUnderVideo().isDisplayed());
    }

    @Then("second advertising banner is displayed")
    public void secondAdvertisingBannerIsDisplayed() {
        Assert.assertTrue(dspElements.getAdvBannerUnderVideo2().isDisplayed());
    }

    @Then("third advertising banner is displayed")
    public void thirdAdvertisingBannerIsDisplayed() {
        Utilities.scrollWindow(testContext.getDriverManager().getWebDriver(),
                500, 600);
        Assert.assertTrue(dspElements.getAdvBannerUnderAboutSection().isDisplayed());
    }

    @Then("forth advertising banner is displayed")
    public void forthAdvertisingBannerIsDisplayed() {
        Utilities.scrollWindow(testContext.getDriverManager().getWebDriver(),
                600, 700);
        Assert.assertTrue(dspElements.getAdvBannerBottomOfPage().isDisplayed());
    }

    @Then("fifth advertising banner is displayed")
    public void fifthAdvertisingBannerIsDisplayed() {
        Utilities.scrollWindow(testContext.getDriverManager().getWebDriver(),
                700, 1200);
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                dspElements.getAdvBannerUnderPopularCreatorsSection(), 10L);
        Assert.assertTrue(dspElements.getAdvBannerUnderPopularCreatorsSection().isDisplayed());
    }

    @Then("sixth advertising banner is displayed")
    public void sixthAdvertisingBannerIsDisplayed() {
        Utilities.scrollWindow(testContext.getDriverManager().getWebDriver(),
                800, 900);
        Assert.assertTrue(dspElements.getAdvBannerUnderPopularVideosSection().isDisplayed());
    }
}
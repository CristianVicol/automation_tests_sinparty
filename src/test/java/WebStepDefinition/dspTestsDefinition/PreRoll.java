package WebStepDefinition.dspTestsDefinition;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pageObjects.webPages.AffiliateVideo;
import pageObjects.webPages.CookiesPopUp;
import pageObjects.webPages.dsp.DspElements;
import utilities.TestContext;
import utilities.Utilities;
import utilities.Wait;

import java.util.List;

import static enums.Context.WINDOW;

public class PreRoll {
    TestContext testContext;
    AffiliateVideo affiliateVideo;
    DspElements dspElements;
    CookiesPopUp cookiesPopUp;

    public PreRoll(TestContext testContext) {
        this.testContext = testContext;
        affiliateVideo = testContext.getPageObjectManager().getAffiliateVideo();
        dspElements = testContext.getPageObjectManager().getDspElements();
        cookiesPopUp = testContext.getPageObjectManager().getCookiesPopUp();
    }

    @When("user click on play video button")
    public void user_click_on_play_video_button() {
        Utilities.preparePageForDspTests(testContext, dspElements, cookiesPopUp);
        affiliateVideo.clickVideo();
    }

    @Then("pre-roll starts")
    public void pre_roll_starts() {
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                dspElements.getPreRoll(), 20L);
        Assert.assertTrue(dspElements.getPreRoll().isDisplayed());
    }
    @Then("skip ad button is displayed")
    public void skipAdButtonIsDisplayed() {
        List<WebElement> listOfElements = List.of(dspElements.getClosePreRollAdBtn(), dspElements.getSkipAdControl());
        Wait.untilListElementIsVisible(testContext.getDriverManager().getWebDriver(),
                listOfElements, 10L);
        Assert.assertTrue(dspElements.getClosePreRollAdBtn().isDisplayed());
        Assert.assertTrue(dspElements.getSkipAdControl().isDisplayed());
    }
}
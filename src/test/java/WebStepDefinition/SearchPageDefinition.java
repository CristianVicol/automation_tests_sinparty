package WebStepDefinition;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.testng.Assert;
import pageObjects.webPages.ChannelPage;
import pageObjects.webPages.CookiesPopUp;
import pageObjects.webPages.MainWeb;
import pageObjects.webPages.SearchPage;
import utilities.TestContext;
import utilities.Wait;

import java.util.concurrent.TimeUnit;

import static enums.Context.*;

public class SearchPageDefinition {
    TestContext testContext;
    MainWeb mainWeb;
    SearchPage searchPage;
    CookiesPopUp cookiesPopUp;
    ChannelPage channelPage;

    public SearchPageDefinition(TestContext testContext) {
        this.testContext = testContext;
        mainWeb = testContext.getPageObjectManager().getMainWeb();
        searchPage = testContext.getPageObjectManager().getSearchPage();
        cookiesPopUp = testContext.getPageObjectManager().getCookiesPopUp();
        channelPage = testContext.getPageObjectManager().getChannelPage();
    }

    @Given("user is on the search page")
    public void user_is_on_the_search_page() {
        cookiesPopUp.clickAcceptCookiesBtn();
        mainWeb.clickSearchBtn();
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                searchPage.getSearchInput(), 25L);
        Assert.assertTrue(searchPage.getSearchInput().isDisplayed());
        Assert.assertTrue(searchPage.getSearchBtn().isDisplayed());
    }

    @When("user tap name {string} of the creator into search field")
    public void user_tap_name_of_the_creator_into_search_field(String nameOfCreator) {
        testContext.getScenarioContext().setContext(CREATOR_NAME, nameOfCreator);
        searchPage.getSearchInput().sendKeys(nameOfCreator);
    }

    @When("click on search icon")
    public void click_on_search_icon() {
        searchPage.getSearchBtn().click();
    }

    @Then("creator is displayed on the page")
    public void creator_is_displayed_on_the_page() {
        try{
            testContext.getDriverManager().getWebDriver().manage().timeouts().implicitlyWait(2L, TimeUnit.SECONDS);
            Wait.untilTextIsPresentInTheElement(testContext.getDriverManager().getWebDriver(),
                    searchPage.getCreatorName(), 5L,
                    testContext.getScenarioContext().getContext(CREATOR_NAME).toString());
            Assert.assertEquals(searchPage.getCreatorName().getText(),
                    testContext.getScenarioContext().getContext(CREATOR_NAME).toString());
        }catch (TimeoutException te){
            Wait.untilTextIsPresentInTheElement(testContext.getDriverManager().getWebDriver(),
                    searchPage.getLiveCamCreatorName(), 5L,
                    testContext.getScenarioContext().getContext(CREATOR_NAME).toString());
            Assert.assertEquals(searchPage.getLiveCamCreatorName().getText(),
                    testContext.getScenarioContext().getContext(CREATOR_NAME).toString());
        }
    }

    @When("user click on All button, after selecting any other button: creatorsBtn, liveCamsBtn, pornVideosBtn")
    public void userClickOnAllButton() {
        searchPage.getCreatorsBtn().click();
        searchPage.getAllBtn().click();
    }

    @And("click on any random value")
    public void clickOnAnyRandomValue() {
        testContext.getScenarioContext().setContext(CREATOR_NAME,
                searchPage.getCreatorName().getText());
        searchPage.getCreatorName().click();
    }

    @Then("a new window is opened with the creators page")
    public void aNewWindowIsOpenedWithTheCreatorsPage() {
        Wait.untilPageReadyState(testContext.getDriverManager().getWebDriver(), 20L);
        String url = testContext.getDriverManager().getWebDriver().getCurrentUrl();
        String[] urlParts = url.split("/");
        String[] creatorNameParts = urlParts[urlParts.length - 1].split("-");
        String creatorName = testContext.getScenarioContext().getContext(CREATOR_NAME).toString();
        boolean checkUrl = creatorName.toLowerCase().contains(creatorNameParts[0]);
        System.out.println("THIS IS creatorName ===>" +creatorName.toLowerCase() + "STOP");
        System.out.println("THIS IS creatorNameParts[0] ===>" + creatorNameParts[0] + "STOP");
        Assert.assertTrue(checkUrl);
    }

    @When("user click on Creators button")
    public void userClickOnCreatorsButton() {
        searchPage.getCreatorsBtn().click();
    }

    @When("user click on Live cams button")
    public void userClickOnLiveCamsButton() {
        searchPage.getLiveCamsBtn().click();
    }

    @When("user click on Porn Videos button")
    public void userClickOnPornVideosButton() {
        searchPage.getPornVideosBtn().click();
    }

    @When("user click on Channels button")
    public void userClickOnChannelsButton() {
        searchPage.getChannelsBtn().click();
    }

    @And("click on any random value from live cams")
    public void clickOnAnyRandomValueFromLiveCams() {
        testContext.getScenarioContext().setContext(CREATOR_NAME,
                searchPage.getLiveCamValue().getText());
        searchPage.getLiveCamValue().click();
    }

    @And("click on any random value from porn videos")
    public void clickOnAnyRandomValueFromPornVideos() {
        String videoNameText = searchPage.getVideoNameText();
        if(videoNameText.equals("")) {
            videoNameText = searchPage.getVideoNameText();
        }
        testContext.getScenarioContext().setContext(VIDEO_NAME_TEXT, videoNameText);
        searchPage.clickPornVideoValue();
    }

    @Then("a new window is opened with the video page")
    public void aNewWindowIsOpenedWithTheVideoPage() {
        Wait.untilPageReadyState(testContext.getDriverManager().getWebDriver(), 20L);
        String url = testContext.getDriverManager().getWebDriver().getCurrentUrl();
        String[] urlParts = url.split("/");
        String[] videoNameParts = urlParts[urlParts.length - 1].split("-");
        String videoName = testContext.getScenarioContext().getContext(VIDEO_NAME_TEXT).toString();
        boolean checkUrl = videoName.toLowerCase().contains(videoNameParts[0]);
        Assert.assertTrue(checkUrl);
    }
    @And("click on any random value from channels results")
    public void clickOnAnyRandomValueFromChannelsResults() {
        searchPage.clickChannelVideoValue();
    }
    @Then("a new window is opened with channels video")
    public void aNewWindowIsOpenedWithChannelsVideo() {
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                channelPage.getChannelPageLogo(), 25L);
        Assert.assertTrue(channelPage.getChannelPageLogo().isDisplayed());
    }

    @Given("user is on the search page - trans")
    public void userIsOnTheSearchPageTrans() {
        cookiesPopUp.clickAcceptCookiesBtn();
        mainWeb.moveToChooseGender();
        mainWeb.clickOnTransOrientationBtn();
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                mainWeb.getSearchBtn(), 40L);
        mainWeb.clickSearchBtn();
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                searchPage.getSearchInput(), 25L);
        Assert.assertTrue(searchPage.getSearchInput().isDisplayed());
        Assert.assertTrue(searchPage.getSearchBtn().isDisplayed());
    }

    @Given("user is on the search page - gay")
    public void userIsOnTheSearchPageGay() {
        cookiesPopUp.clickAcceptCookiesBtn();
        mainWeb.moveToChooseGender();
        mainWeb.clickGayOrientationBtn();
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                mainWeb.getSearchBtn(), 40L);
        mainWeb.clickSearchBtn();
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                searchPage.getSearchInput(), 25L);
        Assert.assertTrue(searchPage.getSearchInput().isDisplayed());
        Assert.assertTrue(searchPage.getSearchBtn().isDisplayed());
    }
}
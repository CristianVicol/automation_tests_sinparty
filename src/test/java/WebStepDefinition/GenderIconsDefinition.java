package WebStepDefinition;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;
import pageObjects.webPages.CookiesPopUp;
import pageObjects.webPages.MainWeb;
import utilities.TestContext;
import utilities.Wait;

public class GenderIconsDefinition {
    TestContext testContext;
    MainWeb webMain;
    CookiesPopUp cookiesPopUp;

    public GenderIconsDefinition(TestContext testContext) {
        this.testContext = testContext;
        webMain = testContext.getPageObjectManager().getMainWeb();
        cookiesPopUp = testContext.getPageObjectManager().getCookiesPopUp();
    }

    @Then("a notification appears at the gender with information")
    public void aNotificationAppearsAtTheGenderWithInformation() {
        cookiesPopUp.clickAcceptCookiesBtn();
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                webMain.getGenderNotificationCrossBtn(), 5L);
        Assert.assertTrue(webMain.getGenderNotificationCrossBtn().isDisplayed());
    }

    @When("user click on gay orientation button")
    public void userClickOnGayButton() {
        cookiesPopUp.clickAcceptCookiesBtn();
        webMain.moveToChooseGender();
        webMain.clickGayOrientationBtn();
    }

    @Then("user is redirected to the page with gay content {string}")
    public void userIsRedirectedToThePageWithGayContent(String link) {
        Assert.assertEquals(testContext.getDriverManager().getWebDriver().getCurrentUrl(), link);
    }

    @When("user click on trans orientation button")
    public void userClickOnTransButton() {
        if (cookiesPopUp.isDisplayedAcceptCookiesBtn()) cookiesPopUp.clickAcceptCookiesBtn();
        webMain.moveToChooseGender();
        webMain.clickOnTransOrientationBtn();
    }

    @Then("user is redirected to the page with {string} content")
    public void userIsRedirectedToThePageWithTransContent(String orientation) {
        String url = testContext.getDriverManager().getWebDriver().getCurrentUrl();
        Assert.assertTrue(url.contains(orientation));
    }

    @When("user click on straight orientation button")
    public void userClickOnStraightOrientationButton() {
        if (cookiesPopUp.isDisplayedAcceptCookiesBtn()) cookiesPopUp.clickAcceptCookiesBtn();
        webMain.moveToChooseGender();
        webMain.clickStraightOrientationBtn();
    }

    @Then("user is redirected to straight content button")
    public void userIsRedirectedToStraightContentButton(DataTable dataTable) {
        String transOrientation = dataTable.cell(0, 0);
        String gayOrientation = dataTable.cell(0, 1);
        String url = testContext.getDriverManager().getWebDriver().getCurrentUrl();
        Assert.assertFalse(url.contains(transOrientation));
        Assert.assertFalse(url.contains(gayOrientation));
    }
}
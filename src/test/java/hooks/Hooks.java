package hooks;

import io.cucumber.java.After;
import io.cucumber.java.AfterStep;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import managers.FileReaderManager;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pageObjects.dashboard.ContentPage;
import pageObjects.dashboard.LogInPage;
import pageObjects.dashboard.MainPage;
import utilities.TestContext;
import utilities.Utilities;
import utilities.Wait;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static enums.Context.PASSWORD;
import static enums.Context.USERNAME;

public class Hooks {
    private final TestContext testContext;
    private WebDriver webDriver;
    private final MainPage mainPage;
    private final ContentPage contentPage;
    LogInPage logInPage;

    public Hooks(TestContext context) {
        testContext = context;
        mainPage = testContext.getPageObjectManager().getDashboardMainPage();
        contentPage = testContext.getPageObjectManager().getContentPage();
        logInPage = testContext.getPageObjectManager().getDashboardLogInPage();
    }

    private static boolean skipBefore = false;

    @Before(order = 0)
    public void setUp(Scenario scenario) {
        if (scenario.getSourceTagNames().contains("@DSP") || scenario.getSourceTagNames().contains("@skipBefore")) {
            skipBefore = true;
        } else {
            skipBefore = false;
            webDriver = testContext.getDriverManager().getWebDriver();
            webDriver.get(FileReaderManager.getInstance().getConfigFileReader().getUrl());
        }
    }

    @After("@UploadVideo")
    public void cleanContent() {
        String username = testContext.getScenarioContext().getContext(USERNAME).toString();
        String password = testContext.getScenarioContext().getContext(PASSWORD).toString();
        webDriver = testContext.getDriverManager().getWebDriver();
        webDriver.get(FileReaderManager.getInstance().getConfigFileReader().getUrl());
        Utilities.acceptAlertIfPresent(webDriver);
        logIn(username, password);
        Wait.untilElementIsVisible(webDriver, mainPage.getContentBtn(), 30L);
        mainPage.clickContentBtn();
        contentPage.clickDeleteVideoBtn();
        contentPage.clickConfirmDeletionBtn();
    }

    @After("@DeleteVideo")
    public void cleanContent(Scenario scenario) {
        String username = testContext.getScenarioContext().getContext(USERNAME).toString();
        String password = testContext.getScenarioContext().getContext(PASSWORD).toString();
        if (scenario.isFailed()) {
            webDriver = testContext.getDriverManager().getWebDriver();
            webDriver.get(FileReaderManager.getInstance().getConfigFileReader().getUrl());
            Utilities.acceptAlertIfPresent(webDriver);
            logIn(username, password);
            Wait.untilElementIsVisible(webDriver, mainPage.getContentBtn(), 30L);
            mainPage.clickContentBtn();
            contentPage.clickDeleteVideoBtn();
            contentPage.clickConfirmDeletionBtn();
        }
    }

    @After
    public void tearDown() {
        testContext.getDriverManager().getWebDriver().manage().deleteAllCookies();
        //testContext.getDriverManager().closeDriver();
    }

    @AfterStep
    public void takeScreenShot(Scenario scenario) throws IOException {
        WebDriver driver = testContext.getDriverManager().getWebDriver();
        if (scenario.isFailed()) {
            File sourcePath = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            byte[] fileContent = FileUtils.readFileToByteArray(sourcePath);
            scenario.attach(fileContent, "image/png", "image");
        }
    }

    public static boolean shouldSkipBefore() {
        return skipBefore;
    }

    private void logIn(String username, String password) {
        try {
            webDriver.manage().timeouts().implicitlyWait(1L, TimeUnit.SECONDS);
            Wait.untilElementIsVisible(webDriver, logInPage.getTfLoginUsername(), 2L);
            logInPage.getTfLoginUsername().sendKeys(username);
            logInPage.getTfLoginPassword().sendKeys(password);
            logInPage.getLogInButton().click();
        } catch (TimeoutException e) {

        }
    }
}
package runners;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;

@CucumberOptions(
        features = "@target/failed_scenarios.txt",
        glue = {"WebStepDefinition","DashboardStepsDefinition", "hooks"},
        plugin = {
                "pretty",
                "html:target/cucumber-reports/advanced-reports/cucumber-pretty.html",
                "json:target/cucumber-reports/advanced-reports/CucumberFailedTestsReport.json",
                "testng:target/web-cukes.xml",
                "com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:",
                "timeline:target/test-output-thread/",
                "rerun:target/failed_scenarios.txt"
        }
)
public class TestRunnerFailedTests extends AbstractTestNGCucumberTests {
    @Override
    @DataProvider(parallel = false)
    public Object[][] scenarios() {
        return super.scenarios();
    }
    @BeforeSuite
    public void beforeSuite() {
        System.out.println("================ BEFORE SUITE ================");
    }

    @AfterSuite
    public void afterSuite() {
        System.out.println("================ AFTER SUITE ================");
    }
}
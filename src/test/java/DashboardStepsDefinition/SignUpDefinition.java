package DashboardStepsDefinition;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import managers.FileReaderManager;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import pageObjects.authenticationForms.GoogleAuthentication;
import pageObjects.dashboard.*;
import pageObjects.dashboard.LogInPage;
import pageObjects.dashboard.MainPage;
import utilities.GmailReader;
import utilities.TestContext;
import utilities.Wait;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static enums.Context.EMAIL;
import static enums.Context.TEXT;

public class SignUpDefinition {
    TestContext testContext;
    SignUpPage dashBoardSignUpPage;
    OnboardingPage onboardingPage;
    GoogleAuthentication googleAuthenticationPage;
    MainPage mainPage;
    LogInPage logInPage;

    public SignUpDefinition(TestContext testContext){
        this.testContext = testContext;
        dashBoardSignUpPage = testContext.getPageObjectManager().getDashBoardSignUpPage();
        onboardingPage = testContext.getPageObjectManager().getOnboardingPage();
        googleAuthenticationPage = testContext.getPageObjectManager().getGoogleAuthentificationPage();
        mainPage = testContext.getPageObjectManager().getDashboardMainPage();
        logInPage = testContext.getPageObjectManager().getDashboardLogInPage();
    }
    @Given("Dashboard sign up page is displayed")
    public void dashboard_page_is_displayed() {
        logInPage.clickSignUpLink();
        String url = FileReaderManager.getInstance().getConfigFileReader().getUrl() + "signup";
        new WebDriverWait(testContext.getDriverManager().getWebDriver(), 10L)
                .until(ExpectedConditions.urlToBe(url));
    }
    @When("input valid email into email field and password in the password field")
    public void input_valid_email_into_email_field_and_password_in_the_password_field(DataTable dataTable) {
        String email = dataTable.cell(1,0);
        testContext.getScenarioContext().setContext(EMAIL, email);
        dashBoardSignUpPage.completeSignUpForm(email);
    }
    @When("click on sign up button")
    public void click_on_sign_up_button() {
        dashBoardSignUpPage.clickSignUpBtn();
    }
    @Then("creator is redirected to the onboarding page")
    public void creator_is_redirected_to_the_onboarding_page() {
        Assert.assertTrue(onboardingPage.getTfFirstName().isDisplayed());
        Assert.assertTrue(onboardingPage.getTfLastName().isDisplayed());
        Assert.assertTrue(onboardingPage.getContinueToProfileBtn().isDisplayed());
    }
    @When("creator complete the onboarding page and click continue button")
    public void creatorCompleteTheOnboardingPageAndClickContinueButton(DataTable dataTable) {
        List<String> dataRow = dataTable.row(1);
        String firstName = dataRow.get(0);
        String lastName = dataRow.get(1);
        String country = dataRow.get(2);
        String citizenship = dataRow.get(3);
        try {
            onboardingPage.completeOnboardingPage(firstName, lastName, country, citizenship);
        }catch (StaleElementReferenceException e){
            onboardingPage.getCompleteUploadProfilePictureBtn().click();
        }
        mainPage.clickContinueBtnFromPopUpAfterSignUp();
    }
    @Then("creator is redirected to the main page {string}")
    public void creatorIsRedirectedToTheMainPage(String textToAssert) {
        testContext.getScenarioContext().setContext(TEXT, textToAssert);
        Assert.assertEquals(mainPage.getVerifyEmailText().getText(), textToAssert);
    }

    @And("confirmation email is sent to the email")
    public void confirmationEmailIsSentToTheEmail() throws GeneralSecurityException, IOException, InterruptedException {
            GmailReader gmailReader = new GmailReader();
            String confirmationLink = gmailReader.getLinkFromInbox("partners@sinparty.com");
            System.out.println("CONFIRMATION LINK ===> " + confirmationLink);
            testContext.getDriverManager().getWebDriver().get(confirmationLink);
            gmailReader.deleteEmails();
    }

    @And("creator can confirm email address")
    public void creatorCanConfirmEmailAddress() {
        try{
            testContext.getDriverManager().getWebDriver().manage().timeouts().implicitlyWait(0L,
                    TimeUnit.SECONDS);
            Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                    mainPage.getVerifyEmailText(), 6L);
            Assert.fail();
        }catch (TimeoutException e){
            Assert.assertTrue(mainPage.getMyDashboardBtn().isDisplayed());
        }
    }

    @When("user insert {string} tag")
    public void userInsertTag(String tag) {
        onboardingPage.getInputTag().sendKeys(tag);
    }

    @Then("user is warned that tag limit is 25 characters")
    public void userIsWarnedThatTagLimitIsCharacters() {

    }

    @And("continue button is disabled")
    public void continueButtonIsDisabled() {

    }

    @When("creator complete the onboarding page until tag section")
    public void creatorCompleteTheOnboardingPageUntilTagSection(DataTable dataTable) {
        List<String> dataRow = dataTable.row(1);
        String firstName = dataRow.get(0);
        String lastName = dataRow.get(1);
        String country = dataRow.get(2);
        String citizenship = dataRow.get(3);
        onboardingPage.completeOnboardingPageUntilTags(firstName, lastName, country, citizenship);
    }
}
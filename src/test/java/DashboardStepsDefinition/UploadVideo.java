package DashboardStepsDefinition;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import managers.FileReaderManager;
import org.openqa.selenium.UnhandledAlertException;
import org.testng.Assert;
import pageObjects.dashboard.ContentPage;
import pageObjects.dashboard.MainPage;
import pageObjects.dashboard.VideoDetailsPopUp;
import utilities.TestContext;
import utilities.Utilities;
import utilities.Wait;

import static enums.Context.VIDEO_NAME;

public class UploadVideo {
    TestContext testContext;
    MainPage mainPage;
    VideoDetailsPopUp videoDetailsPopUp;
    ContentPage contentPage;

    public UploadVideo(TestContext testContext) {
        this.testContext = testContext;
        mainPage = testContext.getPageObjectManager().getDashboardMainPage();
        videoDetailsPopUp = testContext.getPageObjectManager().getUploadVideoDetailsPopUp();
        contentPage = testContext.getPageObjectManager().getContentPage();
    }
    @When("user click on upload video button")
    public void user_click_on_upload_video_button() {
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                mainPage.getUploadVideoBtn(), 30L);
        mainPage.getUploadVideoBtn().click();
    }
    @Then("upload video pop-up is displayed")
    public void upload_video_pop_up_is_displayed() {
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                mainPage.getUploadVideoPopUpTitle(), 30L);
        Assert.assertTrue(mainPage.getUploadVideoPopUpTitle().isDisplayed());
    }
    @When("user select a video to upload in {string} format")
    public void user_select_a_video_to_upload_in_format(String format) {
        switch (format) {
            case "MP4":
                String videoMP4RelativePath = "src/main/resources/testVideo/test.mp4";
                uploadVideo(videoMP4RelativePath);
                break;
            case "MOV":
                String videoMOVRelativePath = "src/main/resources/testVideo/test.mov";
                uploadVideo(videoMOVRelativePath);
                break;
            case "WMV":
                String videoWMVRelativePath = "src/main/resources/testVideo/test.wmv";
                uploadVideo(videoWMVRelativePath);
                break;
        }
    }
    @Then("pop-up is displayed with video details")
    public void pop_up_is_displayed_with_video_details() {
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                videoDetailsPopUp.getVideoPreviewText(), 30L);
        Assert.assertTrue(videoDetailsPopUp.getVideoPreviewText().isDisplayed());
    }
    @When("creator complete video details")
    public void creator_complete_video_details(DataTable dataTable) {
        String description = dataTable.cell(1, 0);
        String videoName = videoDetailsPopUp.getVideoName();
        testContext.getScenarioContext().setContext(VIDEO_NAME, videoName);
        videoDetailsPopUp.getFreeVideoBtn().click();
        videoDetailsPopUp.getVideoDescriptionTextArea().sendKeys(description);
        videoDetailsPopUp.clickOnStraightAudience();
        videoDetailsPopUp.setCategory();
    }
    @When("click on publish button")
    public void click_on_publish_button() {
        try {
            videoDetailsPopUp.clickPublishWithoutTranscoding();
            testContext.getDriverManager().getWebDriver().get(FileReaderManager.getInstance().getConfigFileReader().getUrl());
            mainPage.clickContentBtn();
        }catch (UnhandledAlertException e){
            Utilities.acceptAlertIfPresent(testContext.getDriverManager().getWebDriver());
        }
        mainPage.clickContentBtn();
    }
    @Then("video is posted")
    public void video_is_posted() {
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                contentPage.getVideoTitle(), 30L);
        Assert.assertEquals(contentPage.getVideoTitle().getText(), testContext.getScenarioContext()
                .getContext(VIDEO_NAME).toString());
    }
    @When("creator complete paid video details")
    public void creatorCompletePaidVideoDetails(DataTable dataTable) {
        String description = dataTable.cell(1, 0);
        String price = dataTable.cell(1,1);
        String videoName = videoDetailsPopUp.getVideoName();
        testContext.getScenarioContext().setContext(VIDEO_NAME, videoName);
        videoDetailsPopUp.clickPaidVideoButton();
        videoDetailsPopUp.getVideoDescriptionTextArea().sendKeys(description);
        videoDetailsPopUp.setPrice(price);
        videoDetailsPopUp.clickOnStraightAudience();
        videoDetailsPopUp.setCategory();
    }
    private void uploadVideo(String relativePath) {
        String currentWorkingDirectory = System.getProperty("user.dir");
        String absolutePath = currentWorkingDirectory + "/" + relativePath;
        mainPage.getUploadVideoFilesInput().sendKeys(absolutePath);
    }
}
package DashboardStepsDefinition;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import pageObjects.dashboard.ContentPage;
import pageObjects.dashboard.MainPage;
import pageObjects.dashboard.VideoDetailsPopUp;
import utilities.TestContext;
import utilities.Utilities;
import utilities.Wait;

import java.util.List;
import java.util.Random;

import static enums.Context.VIDEO_NAME;

public class EditVideo {
    TestContext testContext;
    MainPage mainPage;
    ContentPage contentPage;
    VideoDetailsPopUp videoDetailsPopUp;

    public EditVideo(TestContext testContext) {
        this.testContext = testContext;
        mainPage = testContext.getPageObjectManager().getDashboardMainPage();
        contentPage = testContext.getPageObjectManager().getContentPage();
        videoDetailsPopUp = testContext.getPageObjectManager().getUploadVideoDetailsPopUp();
    }

    @When("creator navigates to the content page")
    public void creator_navigates_to_the_content_page() {
        mainPage.clickContentBtn();
    }

    @When("click on the edit button")
    public void click_on_the_edit_button() {
        contentPage.clickEditVideoBtn();
    }

    @Then("edit video pop-up is displayed")
    public void edit_video_pop_up_is_displayed() {
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                videoDetailsPopUp.getPublishButton(), 30L);
        Assert.assertTrue(videoDetailsPopUp.getPublishButton().isDisplayed());
        Assert.assertTrue(videoDetailsPopUp.getFreeVideoBtn().isDisplayed());
    }

    @When("creator edit video name")
    public void creator_edit_video_name() {
        Utilities.deleteTextFromElement(testContext.getDriverManager().getWebDriver(),
                videoDetailsPopUp.getVideoTitleTextField());
        String generatedVideoName = "test" + generateRandomNumber();
        videoDetailsPopUp.getVideoTitleTextField().sendKeys(generatedVideoName);
        testContext.getScenarioContext().setContext(VIDEO_NAME, generatedVideoName);
    }

    @When("description")
    public void description() {
        Utilities.deleteTextFromElement(testContext.getDriverManager().getWebDriver(),
                videoDetailsPopUp.getVideoDescriptionTextArea());
        int randomNr = generateRandomNumber();
        videoDetailsPopUp.getVideoDescriptionTextArea()
                .sendKeys("description" + randomNr);
    }

    @When("category")
    public void category() {
        List<WebElement> listOfCategories = videoDetailsPopUp.getListOfCategories();
        videoDetailsPopUp.getSelectCategory().click();
        int indexForCategoriesList = random0to10();
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                listOfCategories.get(indexForCategoriesList), 30L);
        listOfCategories.get(indexForCategoriesList).click();
    }

    @When("tags")
    public void tags() {
        videoDetailsPopUp.clickCloseTagBtn();
    }
    @And("click on publish button after editing")
    public void clickOnPublishButtonAfterEditing() {
        videoDetailsPopUp.getPublishButton().click();
    }
    @Then("video is edited")
    public void video_is_edited() {
        String textToAssert = testContext.getScenarioContext().getContext(VIDEO_NAME).toString();
        Wait.untilTextIsPresentInTheElement(testContext.getDriverManager().getWebDriver(),
                contentPage.getVideoTitle(),30L, textToAssert);
        Assert.assertEquals(contentPage.getVideoTitle().getText(), textToAssert);
    }

    private int generateRandomNumber() {
        Random random = new Random();
        return random.nextInt(1000000000);
    }

    private int random0to10() {
        Random random = new Random();
        return random.nextInt(10);
    }
}
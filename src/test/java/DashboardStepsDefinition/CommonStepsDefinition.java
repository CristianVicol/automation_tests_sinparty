package DashboardStepsDefinition;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import org.testng.Assert;
import pageObjects.dashboard.LogInPage;
import pageObjects.dashboard.MainPage;
import utilities.TestContext;

import java.util.List;

import static enums.Context.PASSWORD;
import static enums.Context.USERNAME;

public class CommonStepsDefinition {
    TestContext testContext;
    LogInPage logInPage;
    MainPage mainPage;
    public CommonStepsDefinition(TestContext testContext){
        this.testContext = testContext;
        logInPage = testContext.getPageObjectManager().getDashboardLogInPage();
        mainPage = testContext.getPageObjectManager().getDashboardMainPage();
    }
    @Given("creator is on the main dashboard page")
    public void creatorIsOnTheMainDashboardPage(DataTable dataTable) {
        List<String> dataRow = dataTable.row(1);
        String username = dataRow.get(0);
        String password = dataRow.get(1);
        testContext.getScenarioContext().setContext(USERNAME, username);
        testContext.getScenarioContext().setContext(PASSWORD, password);
        logInPage.fillUsernameAndPassword(username, password);
        logInPage.getLogInButton().click();
        Assert.assertEquals(mainPage.getMyDashboardBtn().getText(), "My Dashboard");
    }
}
package DashboardStepsDefinition;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pageObjects.dashboard.MainPage;
import pageObjects.dashboard.Transactions;
import pageObjects.dashboard.WithdrawModal;
import utilities.TestContext;
import utilities.Utilities;
import utilities.Wait;

import java.util.List;

public class Withdrawal {
    TestContext testContext;
    MainPage mainPage;
    WithdrawModal withdrawModal;
    Transactions transactions;
    public Withdrawal(TestContext testContext){
        this.testContext = testContext;
        mainPage = testContext.getPageObjectManager().getDashboardMainPage();
        withdrawModal = testContext.getPageObjectManager().getWithdrawModal();
        transactions = testContext.getPageObjectManager().getTransactions();
    }
    @When("creator click on withdraw button")
    public void creator_click_on_withdraw_button() {
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                mainPage.getWithdrawButton(), 30L);
        mainPage.getWithdrawButton().click();
    }
    @Then("withdraw pop-up is displayed")
    public void withdraw_pop_up_is_displayed() {
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                withdrawModal.getWithdrawModalTitle(), 30L);
        Assert.assertTrue(withdrawModal.getWithdrawModalTitle().isDisplayed());
    }
    @When("creator insert withdraw amount {int}")
    public void creator_insert_withdraw_amount(Integer amount) {
        JavascriptExecutor jsExecutor = (JavascriptExecutor) testContext.getDriverManager().getWebDriver();
        jsExecutor.executeScript("arguments[0].click();", withdrawModal.getWithdrawInputField());
        jsExecutor.executeScript("arguments[0].value = '';", withdrawModal.getWithdrawInputField());
        jsExecutor.executeScript("arguments[0].value = '';", withdrawModal.getWithdrawInputField());
        withdrawModal.getWithdrawInputField().sendKeys(amount.toString());
    }
    @And("select payout method")
    public void selectPayoutMethod() {
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                withdrawModal.getPayoutMethodSelector(), 40L);
        withdrawModal.getPayoutMethodSelector().click();
    }
    @Then("request withdrawal button is disabled")
    public void request_withdrawal_button_is_disabled() {
        JavascriptExecutor jsExecutor = (JavascriptExecutor) testContext.getDriverManager().getWebDriver();
        jsExecutor.executeScript("arguments[0].scrollBy(0, 5000);", withdrawModal.getWithdrawModal());
        Assert.assertFalse(withdrawModal.getRequestWithdrawalButton().isEnabled());
    }

    @And("click on request withdraw button")
    public void clickOnRequestWithdrawButton() {
        withdrawModal.clickRequestWithdrawal();
    }

    @Then("Withdraw request pop-up is displayed")
    public void withdrawRequestPopUpIsDisplayed() {
        List<WebElement> listOfElements = List.of(withdrawModal.getWithdrawRequestPopUpText(), withdrawModal.getProceedButton());
        Wait.untilListElementIsVisible(testContext.getDriverManager().getWebDriver(),
                listOfElements, 30L);
        Assert.assertTrue(withdrawModal.getWithdrawRequestPopUpText().isDisplayed());
        Assert.assertTrue(withdrawModal.getProceedButton().isDisplayed());
    }

    @When("creator click on proceed button")
    public void creatorClickOnProceedButton() {
        withdrawModal.getProceedButton().click();
    }

    @Then("request complete pop-up is displayed")
    public void requestCompletePopUpIsDisplayed() {
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                withdrawModal.getRequestCompletedPopUpText(), 30L);
        Assert.assertTrue(withdrawModal.getRequestCompletedPopUpText().isDisplayed());
        withdrawModal.getCloseRequestCompletedPopUp().click();
    }

    @And("transaction is displayed on the transaction page")
    public void transactionIsDisplayedOnTheTransactionPage() {
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                mainPage.getTransactionsButton(), 30L);
        mainPage.getTransactionsButton().click();
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                transactions.getTransactionDate(), 30L);
        Assert.assertEquals(transactions.getTransactionDate().getText(), Utilities.getCurrentDate());
    }
}
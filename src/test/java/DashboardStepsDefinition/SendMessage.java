package DashboardStepsDefinition;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.StaleElementReferenceException;
import org.testng.Assert;
import pageObjects.dashboard.Inbox;
import pageObjects.dashboard.MainPage;
import utilities.TestContext;
import utilities.Utilities;
import utilities.Wait;

import java.io.IOException;
import java.util.Random;

import static enums.Context.*;

public class SendMessage {
    TestContext testContext;
    MainPage mainPage;
    Inbox inbox;

    public SendMessage(TestContext testContext) {
        this.testContext = testContext;
        mainPage = testContext.getPageObjectManager().getDashboardMainPage();
        inbox = testContext.getPageObjectManager().getCreatorInbox();
    }

    @Given("creator is on the inbox page")
    public void creator_is_on_the_inbox_page() {
        mainPage.clickInboxBtn();
        Assert.assertTrue(inbox.getSendMassMessageBtn().isDisplayed());
    }

    @When("creator click on New Mass Message button")
    public void creator_click_on_new_mass_message_button() {
        inbox.clickSendMassMessageBtn();
    }

    @Then("pop-up with message details is displayed")
    public void pop_up_with_message_details_is_displayed() {
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                inbox.getMassMessageModalHeader(), 30L);
        Assert.assertTrue(inbox.getMassMessageModalHeader().isDisplayed());
    }

    @When("creator click on {string} button")
    public void creator_click_on_followers_button(String receiver) {
        switch (receiver) {
            case "All":
                inbox.getAllFromMassMessageModalBtn().click();
                testContext.getScenarioContext().setContext(RECEIVER, receiver);
                break;
            case "Followers":
                inbox.getFollowersFromMassMessageModalBtn().click();
                testContext.getScenarioContext().setContext(RECEIVER, receiver);
                break;
            case "Subscribers":
                inbox.getSubscribersFromMassMessageModalBtn().click();
                testContext.getScenarioContext().setContext(RECEIVER, receiver);
                break;
        }
    }

    @When("type message into the message text area")
    public void type_message_into_the_message_text_area(DataTable dataTable) {
        String testData = dataTable.cell(1, 0);
        String message = testData + generateRandomNr() + " ";
        inbox.getMessageInput().sendKeys(message);
        testContext.getScenarioContext().setContext(MESSAGE, message);
    }

    @When("select an emoji")
    public void select_an_emoji() {
        inbox.getEmojiBtn().click();
        inbox.clickEmojiValue();
    }

    @When("click on Send Message button")
    public void click_on_send_message_button() {
        inbox.clickSendMessageBtn();
        Utilities.sleep(10000);
    }

    @Then("message is displayed in the chat")
    public void message_is_displayed_in_the_chat() {
        String receiver = testContext.getScenarioContext().getContext(RECEIVER).toString().trim();
        String sentMessage = testContext.getScenarioContext().getContext(MESSAGE).toString();
        switch (receiver){
            case "Subscribers":
                inbox.clickSubscribersBtn();
                inbox.clickSubscriberChatItem();
                String lastMessage = inbox.getLastTextInMessageWithAttachment().getText();
                System.out.println("sentMessage ===> " + sentMessage);
                System.out.println("lastMessage ===> " + lastMessage);
                Assert.assertTrue(lastMessage.contains(sentMessage));
                break;
            case "Followers":
                inbox.clickFollowersBtn();
                String lastMessage2 = inbox.getLastTextInMessageWithAttachment().getText();
                System.out.println("sentMessage ===> " + sentMessage);
                System.out.println("lastMessage2 ===> " + lastMessage2);
                Assert.assertTrue(lastMessage2.contains(sentMessage));
                break;
            case "All":
                String lastMessage3 = inbox.getLastTextInMessageWithAttachment().getText();
                System.out.println("sentMessage ===> " + sentMessage);
                System.out.println("lastMessage2 ===> " + lastMessage3);
                Assert.assertTrue(lastMessage3.contains(sentMessage));
                inbox.clickRecentChatItem();
                Utilities.sleep(2000);
                Assert.assertTrue(lastMessage3.contains(sentMessage));
        }
    }
    @And("attach a file to the message")
    public void attachAFileToTheMessage(DataTable dataTable) throws IOException {
        inbox.clickAttachFilesBtn();
        inbox.clickUploadBtn();
        Utilities.sleep(2000);
        Runtime.getRuntime().exec("C:\\Automation_Tests_SinParty\\src\\main\\resources\\file_upload.exe");
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                inbox.getAttachmentPriceInput(), 30L);
        String priceOfAttachment = dataTable.cell(1, 0);
        testContext.getScenarioContext().setContext(PRICE, priceOfAttachment);
        inbox.getAttachmentPriceInput().sendKeys(priceOfAttachment);
    }

    @Then("message is displayed in the chat with attachment")
    public void messageIsDisplayedInTheChatWithAttachment() {
        String sentMessage = testContext.getScenarioContext().getContext(MESSAGE).toString().trim();
        Utilities.sleep(10000);
        String textInMessageWithAttachment = inbox.getLastTextInMessageWithAttachment().getText();
        Assert.assertEquals(textInMessageWithAttachment, sentMessage);
        Assert.assertTrue(inbox.getImageFromTheChat().isDisplayed());
        String priceOfAttachment = testContext.getScenarioContext().getContext(PRICE).toString();
        Assert.assertEquals(inbox.getPriceOfTheSentImage().getText(),
                "$" + priceOfAttachment);
    }
    private int generateRandomNr() {
        Random random = new Random();
        return random.nextInt(1000000000);
    }
}
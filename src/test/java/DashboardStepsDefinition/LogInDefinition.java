package DashboardStepsDefinition;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;
import pageObjects.dashboard.LogInPage;
import pageObjects.dashboard.MainPage;
import pageObjects.authenticationForms.GoogleAuthentication;
import utilities.TestContext;
import utilities.Wait;

import java.util.List;

public class LogInDefinition {
    TestContext testContext;
    LogInPage dashboardLogInPage;
    MainPage dashboardMainPage;
    GoogleAuthentication googleAuthentication;
    public LogInDefinition(TestContext context) {
        testContext = context;
        dashboardLogInPage = testContext.getPageObjectManager().getDashboardLogInPage();
        dashboardMainPage = testContext.getPageObjectManager().getDashboardMainPage();
        googleAuthentication = testContext.getPageObjectManager().getGoogleAuthentificationPage();
    }
    @Given("Dashboard log in page is displayed")
    public void dashboardLogInPageIsDisplayed() {
       // Assert.assertTrue(dashboardLogInPage.getSignUpLink().isDisplayed());  TEMPORARY COMMENTED
        Assert.assertTrue(dashboardLogInPage.getTfLoginPassword().isDisplayed());
        Assert.assertTrue(dashboardLogInPage.getTfLoginUsername().isDisplayed());
    }

    @When("input valid credentials into username or email field and password field")
    public void inputValidCredentials(DataTable dataTable) {
        List<String> dataRow = dataTable.row(1);
        String username = dataRow.get(0);
        String password = dataRow.get(1);
        dashboardLogInPage.fillUsernameAndPassword(username, password);
    }
    @When("click on login button")
    public void clickOnLoginButton() {
        dashboardLogInPage.getLogInButton().click();
    }
    @When("creator click on Log in with google button")
    public void creatorClickOnGoogleButton() {
        dashboardLogInPage.getGoogleLogInBtn().click();
    }
    @Then("dashboard google authentication page is displayed")
    public void dashboardGoogleAuthenticationPageIsDisplayed() {
        Assert.assertTrue(googleAuthentication.getEmailField().isDisplayed());
    }
    @When("user complete dashboard authentication google page")
    public void userCompleteDashboardAuthenticationGooglePage(DataTable dataTable) {
        List<String> dataRow = dataTable.row(1);
        String email = dataRow.get(0);
        String password = dataRow.get(1);
        Wait.untilElementIsClickable(testContext.getDriverManager().getWebDriver(),
                googleAuthentication.getNextBtn(), 5L);
        googleAuthentication.getEmailField().sendKeys(email);
        googleAuthentication.clickNextBtn();
        Wait.untilElementIsClickable(testContext.getDriverManager().getWebDriver(),
                googleAuthentication.getNextBtn(), 5L);
        googleAuthentication.getPasswordField().sendKeys(password);
        googleAuthentication.clickNextBtn();
    }
    @Then("creator is logged in")
    public void creatorIsLoggedIn() {
        Assert.assertEquals(dashboardMainPage.getMyDashboardBtn().getText(), "My Dashboard");
    }
}
package DashboardStepsDefinition;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import managers.FileReaderManager;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageObjects.dashboard.ContentPage;
import pageObjects.dashboard.MainPage;
import pageObjects.dashboard.VideoDetailsPopUp;
import utilities.TestContext;
import utilities.Utilities;
import utilities.Wait;

import java.util.concurrent.TimeUnit;

public class DeleteVideo {
    TestContext testContext;
    MainPage mainPage;
    VideoDetailsPopUp videoDetailsPopUp;
    ContentPage contentPage;

    public DeleteVideo(TestContext testContext) {
        this.testContext = testContext;
        mainPage = testContext.getPageObjectManager().getDashboardMainPage();
        videoDetailsPopUp = testContext.getPageObjectManager().getUploadVideoDetailsPopUp();
        contentPage = testContext.getPageObjectManager().getContentPage();
    }

    @Given("creator has a video on the content page")
    public void creator_has_a_video_on_the_content_page() {
        try {
            uploadVideo();
            testContext.getDriverManager().getWebDriver().get(FileReaderManager.getInstance().getConfigFileReader().getUrl());
            mainPage.clickContentBtn();
        }catch (UnhandledAlertException e){
            Utilities.acceptAlertIfPresent(testContext.getDriverManager().getWebDriver());
        }
        mainPage.clickContentBtn();
    }

    @When("creator click on delete button")
    public void creator_click_on_delete_button() {
        contentPage.clickDeleteVideoBtn();
    }

    @Then("confirmation pop-up is displayed")
    public void confirmation_pop_up_is_displayed() {
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                contentPage.getConfirmDeletionBtn(), 30L);
        Assert.assertTrue(contentPage.getConfirmDeletionBtn().isDisplayed());
    }

    @When("creator confirm deletion")
    public void creator_confirm_deletion() {
        contentPage.clickConfirmDeletionBtn();
    }

    @Then("video is not present on the content page")
    public void video_is_not_present_on_the_content_page() {
        WebDriver webDriver = testContext.getDriverManager().getWebDriver();
        webDriver.get(FileReaderManager.getInstance().getConfigFileReader().getUrl());
        webDriver.switchTo().alert().accept();
        mainPage.clickContentBtn();
        try {
            testContext.getDriverManager().getWebDriver().manage().timeouts().implicitlyWait(1L, TimeUnit.SECONDS);
            Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                    contentPage.getVideoTitle(), 3L);
            Assert.fail();
        } catch (TimeoutException e) {
            Assert.assertTrue(true);
        }
    }

    private void uploadVideo() {
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                mainPage.getUploadVideoBtn(), 30L);
        mainPage.getUploadVideoBtn().click();
        String relativePath = "src/main/resources/testVideo/test.mp4";
        String currentWorkingDirectory = System.getProperty("user.dir");
        String absolutePath = currentWorkingDirectory + "/" + relativePath;
        mainPage.getUploadVideoFilesInput().sendKeys(absolutePath);
        String description = "Video Description for test";
        videoDetailsPopUp.getFreeVideoBtn().click();
        videoDetailsPopUp.getVideoDescriptionTextArea().sendKeys(description);
        videoDetailsPopUp.clickOnStraightAudience();
        videoDetailsPopUp.setCategory();
        videoDetailsPopUp.clickPublishWithoutTranscoding();
    }
}
package DashboardStepsDefinition;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;
import pageObjects.dashboard.ForgotPassword;
import pageObjects.dashboard.LogInPage;
import pageObjects.dashboard.MainPage;
import utilities.GmailReader;
import utilities.TestContext;

import java.io.IOException;
import java.security.GeneralSecurityException;

import static enums.Context.*;

public class ForgotPasswordDashboard {
    TestContext testContext;
    LogInPage logInPage;
    ForgotPassword forgotPassword;
    MainPage mainPage;

    public ForgotPasswordDashboard(TestContext testContext) {
        this.testContext = testContext;
        logInPage = testContext.getPageObjectManager().getDashboardLogInPage();
        forgotPassword = testContext.getPageObjectManager().getForgotPassword();
        mainPage = testContext.getPageObjectManager().getDashboardMainPage();
    }

    @Given("creator is on log in page")
    public void creator_is_on_log_in_page() {
        //Assert.assertTrue(logInPage.getSignUpLink().isDisplayed());    TEMPORARY COMMENTED
        Assert.assertTrue(logInPage.getTfLoginPassword().isDisplayed());
        Assert.assertTrue(logInPage.getTfLoginUsername().isDisplayed());
    }
    @When("creator click on forgot password link")
    public void creator_click_on_forgot_password_link() {
        logInPage.getForgotPasswordLink().click();
    }
    @Then("forgot password page is displayed for providing email")
    public void forgot_password_page_is_displayed_for_providing_email() {
        Assert.assertTrue(forgotPassword.getEmailField().isDisplayed());
        Assert.assertTrue(forgotPassword.getResetPasswordButton().isDisplayed());
    }
    @When("creator tap the email {string} into the field")
    public void creator_tap_the_email_into_the_field(String email) {
        forgotPassword.getEmailField().sendKeys(email);
        testContext.getScenarioContext().setContext(EMAIL, email);
    }
    @When("click on Reset Password button")
    public void click_on_reset_password_button() {
        forgotPassword.getResetPasswordButton().click();
    }
    @Then("mail with reset password link is sent to the email address provided from {string}")
    public void mail_with_reset_password_link_is_sent_to_the_email_address_provided(String emailSender) {
        try{
            GmailReader gmailReader = new GmailReader();
            String resetPasswordLink = gmailReader.getLinkFromInbox(emailSender);
            System.out.println("RESET LINK ===>" + resetPasswordLink);
            testContext.getScenarioContext().setContext(LINK, resetPasswordLink);
        }catch ( IOException | GeneralSecurityException e ) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
    @When("creator access the link sent to email address")
    public void creator_access_the_link_sent_to_email_address() {
        String resetPasswordLink = testContext.getScenarioContext().getContext(LINK).toString();
        testContext.getDriverManager().getWebDriver().get(resetPasswordLink);
    }
    @Then("creator is redirected to the reset password section")
    public void creator_is_redirected_to_the_reset_password_section() {
        Assert.assertTrue(forgotPassword.getNewPasswordField().isDisplayed());
        Assert.assertTrue(forgotPassword.getConfirmPasswordField().isDisplayed());
        Assert.assertTrue(forgotPassword.getResetPasswordButton().isDisplayed());
    }
    @When("creator insert new password into the fields")
    public void creator_insert_new_password_into_the_fields() {
        String newPassword = forgotPassword.generateNewPassword();
        System.out.println("NEW PASSWORD ===> " + newPassword);
        testContext.getScenarioContext().setContext(NEW_PASSWORD, newPassword);
        forgotPassword.getNewPasswordField().sendKeys(newPassword);
        forgotPassword.getConfirmPasswordField().sendKeys(newPassword);
        forgotPassword.getResetPasswordButton().click();
    }
    @Then("creator is redirected to log in page")
    public void creator_is_redirected_to_log_in_page() {
        Assert.assertTrue(logInPage.getTfLoginUsername().isDisplayed());
        Assert.assertTrue(logInPage.getTfLoginPassword().isDisplayed());
    }
    @Then("the creator can log in with new password")
    public void the_creator_can_log_in_with_new_password() {
        String newPassword = testContext.getScenarioContext().getContext(NEW_PASSWORD).toString();
        logInPage.getTfLoginUsername().sendKeys(testContext.getScenarioContext().getContext(EMAIL).toString());
        logInPage.getTfLoginPassword().sendKeys(newPassword);
        logInPage.getLogInButton().click();
        try{
            GmailReader gmailReader = new GmailReader();
            gmailReader.deleteEmails();
        } catch (GeneralSecurityException | IOException e) {
            e.printStackTrace();
        }
        Assert.assertTrue(mainPage.getMyDashboardBtn().isDisplayed());
        Assert.assertTrue(mainPage.getUploadVideoBtn().isDisplayed());
    }
}
@Web @Run
Feature: Pop-up cookies are displayed when accessing the website and are working properly

  Background:
    Given user access the website
    Then pop-up cookies is displayed with all elements on it

  @Web
  Scenario: Upon clicking on the Learn more link a new window opens with privacy policy page
    When user click on learn more link
    Then a new window open with privacy policy page at cookies section
      | Name of cookies section from privacy policy        | Link                                                 |
      | COOKIES AND AUTOMATIC DATA COLLECTION TECHNOLOGIES | sinparty.com/privacy-policy#element8 |

  @Web
  Scenario: Upon clicking cookie settings button a new pop-up opens with cookies settings
    When user click on cookie settings button
    Then a new pop-up opens with cookie settings
    When user click on cancel button
    Then pop-up cookies is displayed with all elements on it
    When user click on cookie settings button
    And click on save button
    Then main website is displayed

  @Web
  Scenario: Upon clicking reject all button main page is displayed
    When user click on reject all button
    Then main website is displayed
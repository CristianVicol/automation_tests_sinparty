@Web @Run
Feature: Forgot Password test

  @Web @Forgot
  Scenario: User can reset password by providing valid email
    Given user is on log form
    When user click on forgot password link
    Then forgot password pop-up is displayed for providing email
    When user tap the email "testsinparty2023@gmail.com" into the field
    When clicks on Reset Password button
    Then reset password link is sent to the email address provided from "partners@sinparty.com"
    When user access the link sent to email address
    Then user is redirected to the reset password section
    When user insert new password into the fields
    Then password was successfully reset
    And user is redirected to log in form
    And the user can log in with new password
      | username      |
      | testsinpartyy |
    And main page is displayed
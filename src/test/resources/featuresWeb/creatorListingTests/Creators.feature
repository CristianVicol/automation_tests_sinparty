@Web @Run
Feature: Creators page(in the header of the page) tests from regression checklist

  Background:
    Given user is on the creators listing page

  @Web
  Scenario: User click on the button in creators inside the open page - (update logo above the icons)
    When user click on the update button
    Then page is refreshed
      | Url                                |
      #| https://testing.sinparty.com/porn-creators |
      | https://sinparty.com/porn-creators |

  @Web
  Scenario: Check pagination
    When user goes to next page by clicking on next button
    Then pages are changed
      | page-2 |
    When user opens randomly a model page
    Then model page is displayed
    When user goes back to creators page
    And user goes to next page by clicking on next button
    Then pages are changed
      | page-3 |
    And user opens randomly a model page
    Then model page is displayed

  @Web
  Scenario: Check Live cams button
    When user click on Live cams button from creator listing page
    Then is redirected to live cams page
      | Url                            |
      | https://sinparty.com/live-porn |
      #| https://testing.sinparty.com/live-porn |

  @Web
  Scenario: Top Rankings section check
    When user click on top rankings button
    Then top rankings section is displayed
      | Header Text |
      | Top Ranking |
    When user click on the sinparty logo
    Then user is redirected to the main page
    When user navigates back to the Top Rankings page
    Then top rankings section is displayed
      | Header Text |
      | Top Ranking |

  @Web
  Scenario Outline: List of creators page checks
    When user click on '<Type List>' button
    Then page is displayed
    When user opens randomly a model page
    Then model page is displayed
    When user goes back to creators page
    When user goes to next page by clicking on next button
    Then pages are changed
      | page-2 |
    When user opens randomly a model page
    Then model page is displayed
    When user goes back to creators page
    When user goes to next page by clicking on next button
    Then pages are changed
      | page-3 |
    When user opens randomly a model page
    Then model page is displayed
    Examples:
      | Type List   |
      | Trending    |
      | Most Viewed |
      | Newest      |
      | Alphabetic  |

  @Web
  Scenario: Check Random button
    When user click on random button
    Then user is redirected to a random model page

  @Web
  Scenario Outline: Filter section checks
    When user click on featured '<Button>'
    Then user is redirected to featured section '<Test Data>'
    When user opens randomly a model page
    Then model page is displayed
    Examples:
      | Button   | Test Data     |
      | Featured | featured=true |
      | Premium  | myparty=true  |

  @Web
  Scenario Outline: Gender section checks
    When user click on gender '<Button>'
    Then user is redirected to selected gender section '<Test Data>'
    When user opens randomly a model page
    Then model page is displayed
    Examples:
      | Button | Test Data     |
      | Male   | gender=male   |
      | Female | gender=female |

  @Web
  Scenario: Creator live now checks(bottom of the page)
    When creators hover on creator live now container
    Then scroll button is displayed
    When user click on see all creator live now
    Then user is redirected to live creators page
      | https://sinparty.com/live-porn |
      #| https://testing.sinparty.com/live-porn |

@Web @Run
Feature: Live Creators tests

  Background:
    Given user is on live creators page

  @Web
  Scenario: Check that user can change gender Girls/Guys
    When user click on Guys button
    Then gay live porn page is displayed
      | https://sinparty.com/gay-porn/live |
    When user click on girls button
    Then straight live porn page is displayed
      | https://sinparty.com/live-porn |

  @Web
  Scenario: Check new models section
    When user click on new models button
    Then new models section is displayed
      | new_models=1 |
    When user click on a live model
    Then live model page is displayed
    When user goes back to live models page
    And click on next button
    Then next page with live models is displayed
      | page-2 |
    When user click on a live model
    Then live model page is displayed

  @Web
  Scenario: Check gold show section
    When user click on gold show button
    Then new models section is displayed
      | goldshow=1 |
    When user click on a live model
    Then live model page is displayed

  @Web
  Scenario: Check tags section
    When user click on a random tag
    Then user is redirected to the relevant page
    When user goes back to live models page
    When user click on show less button
    Then less tags are displayed
      | Show More |
    And click on show more tags button
    Then more tags are displayed
      | Show Less |

  @Web
  Scenario: Check category filters section
    When user click on Category filter button
    And click on a random category
    Then page is loaded with selected category
    When click on next button
    Then next page with live models is displayed
      | page-2 |
    When user click on show more categories button
    Then more categories are displayed
      | Show Less |
    When user click on show less categories button
    Then less categories are displayed
      | Show More |

  @Web
  Scenario: Check region filter section
    When user click on region filter button
    And select a region
    Then live creator of selected region are displayed
    When click on next button
    Then next page with live models is displayed
      | page-2 |

  @Web
  Scenario: Check age filter section
    When user click on age filter button
    Then live creatos of select age range are displayed
      | D=18-19 | D=20-29 | D=30-39 | D=40-49 | D=50-99 |
    When click on next button
    Then next page with live models is displayed
      | page-2 |
    When user click on a live model
    Then live model page is displayed

  @Web
  Scenario: Check language filter section
    When user click on language
    Then live creator of selected language is displayed
      | D=en | D=de | D=es | D=fr | D=it | D=sv | D=pt | D=nl |

  @Web
  Scenario: Check new models filter section
    When user click on show only new
    Then new models are displayed
      | new_models=1 |
    When user click on show all
    Then all models are displayed
      | new_models=0 |

  @Web
  Scenario: Check gender filter section
    When user choose a gender
    Then creators of selected gender are displayed
      | D=ff | D=mf | D=m | D=mm | D=tf2m | D=tm2f | D=g |
    When click on next button
    Then next page with live models is displayed
      | page-2 |
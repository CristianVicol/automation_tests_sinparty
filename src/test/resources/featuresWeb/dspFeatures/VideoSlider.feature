@Web @DSP
Feature: Video slider tests

  Background:
    Given user access the site from a paid channel "https://www.fuq.com/"

  @Web @DSP
  Scenario: The user must be able to close the ad video slider immediately
    Then user close the ad video slider immediately by clicking on cross button
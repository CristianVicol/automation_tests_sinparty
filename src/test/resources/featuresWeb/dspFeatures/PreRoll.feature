@Web @DSP
Feature: Pre-roll tests

  Background:
    Given user access the site from a paid channel "https://www.fuq.com/"

  @Web @DSP
  Scenario: The pre-roll starts playing after user click on play video button
    When user click on play video button
    Then pre-roll starts
    And skip ad button is displayed





@Web @DSP
Feature: DSP Banners tests

  Background:
    Given user access the site from a paid channel "https://www.fuq.com/"

  @Web @DSP
  Scenario: Ad Banners are displayed on the affiliate video page
    Then advertising banner is displayed on the right side of the video
    Then first advertising banner is displayed
    Then second advertising banner is displayed
    Then third advertising banner is displayed
    Then forth advertising banner is displayed
    Then fifth advertising banner is displayed
    Then sixth advertising banner is displayed

  @Web @DSP1
  Scenario: Ad Banners are displayed on the creator video page
    When user navigates on the creator video page
    Then advertising banner is displayed on the right side of the creators video
    Then first advertising banner is displayed
    Then second advertising banner is displayed
    Then third advertising banner is displayed
    Then advertising banner is displayed on the right side of premium content section
    Then forth advertising banner is displayed
    Then fifth advertising banner is displayed
    Then sixth advertising banner is displayed

  @Web @DSP
  Scenario: Ad Banners are displayed on the Homepage
    When user navigates to the homepage
    Then banner under creators spotlight is displayed
    Then banner under popular on sinparty is displayed
    Then video adv is displayed under trending now section
    Then adv banners are displayed under trending now section
    Then banner under paid videos section is displayed
    Then banner under welcome to the party section is displayed
    Then banner under most recent section is displayed
    Then banner under creator categories is displayed
    Then banner under the hottest section is displayed

  @Web @DSP
  Scenario: Ad Banners are displayed on the Creator Listing Page
    When user navigates to the creators listing page
    Then first ad banner is displayed
    Then second ad banner is displayed
    Then third ad banner is displayed
    Then forth ad banner is displayed
    Then banner on the left side of the page is displayed
    Then video ad is displayed
    Then ad banner on the end of the page is displayed

  #@Web @DSP   #PROBLEM
  #Scenario: Ad Banners are displayed on the Video Listing Page
    #When user navigates to the videos listing page
    #Then first ad banner is displayed
    #Then second ad banner is displayed
    #Then third ad banner is displayed
    #Then forth ad banner is displayed
    #Then banner on the left side of the page is displayed
    #Then video ad is displayed
    #Then ad banner on the end of the page is displayed

  #@Web @DSP PROBLEM
  #Scenario: Ad Banners are displayed on the Categories Listing Page
    #When user navigates to the categories listing page
    #Then first ad banner is displayed
    #Then second ad banner is displayed
    #Then banner on the left side of the page is displayed
    #Then video ad is displayed
    #Then ad banner on the end of the page is displayed

  @Web @DSP
  Scenario: Ad Banners are displayed on the Video Listing Page
    When user navigates to the live creators listing page
    Then first ad banner is displayed
    Then second ad banner is displayed
    Then third ad banner is displayed
    Then forth ad banner is displayed
    Then banner on the left side of the page is displayed
    Then video ad is displayed
    Then fifth banner on the end of the page is displayed

  @Web @DSP
  Scenario: Ad Banners are displayed on the Live Creators Page
    When user navigates to the live creator page
    Then ad banner under live creator video is displayed
    Then ad banner is displayed in the center of the page
    Then ad banner is displayed on the left side of the page
    Then ad banner is displayed on the right side of the page
    Then video ad is displayed on the bottom of the page
    Then ad banner is displayed on the bottom if the page
@Web @DSP
Feature: DSP Banners tests - Trans

  Background:
    Given user access the site from a paid channel "https://www.fuq.com/"

  @Web @DSP
  Scenario: Ad Banners are displayed on the Homepage - Trans
    When user navigates on the trans home page
    Then banner under creators spotlight is displayed
    Then banner under popular on sinparty is displayed
    Then video adv is displayed under trending now section
    Then adv banners are displayed under trending now section
    Then banner under paid videos section is displayed
    Then banner under welcome to the party section is displayed
    Then banner under most recent section is displayed
    Then banner under creator categories is displayed
    Then banner under the hottest section is displayed
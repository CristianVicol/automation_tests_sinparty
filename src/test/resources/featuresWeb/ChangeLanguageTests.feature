@Web @Run
Feature: User change language tests

  Scenario Outline: When user change language of the website it remains unchanged when navigating to other pages
    Given user is on the website main page - english language
      | username    | password |
      | testaccount | 02040204 |
    When user click on one of the  '<Languages>' icon from the bottom of the page
    Then language of the website is changed to the selected one '<Main Page Url>'
    When user navigates to my account page
    Then the language remains unchanged '<My Account Url>'
    Examples:
      | Languages | Main Page Url           | My Account Url                             |
      | Deutsch   | https://sinparty.com/de | https://sinparty.com/de/account/my-account |
      | Francais  | https://sinparty.com/fr | https://sinparty.com/fr/account/my-account |
      | Spanish   | https://sinparty.com/es | https://sinparty.com/es/account/my-account |
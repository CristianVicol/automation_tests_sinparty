@Web
Feature: A non existing user must be able to register

  Background:
    Given User is on sign up form

  @Web
  Scenario: User can sign up with a valid email and password
    When complete the form with valid email, user and password
      | username | email              | password  |
      | autotest | autotest@gmail.com | testpass1 |
    And click on Sign Up button
    Then Thank you pop up is displayed
    When user click on proceed button
    Then user is redirected to main page
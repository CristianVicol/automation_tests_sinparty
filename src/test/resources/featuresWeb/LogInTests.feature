@Web
Feature: As a registered user I can log in on the website

  Background:
    Given Website is displayed

  @Web @Run
  Scenario: Login to sinparty.com using valid user account
    When user click on sign in button
    And input credentials into username and password field
      | Username    | Password |
      | testaccount | 02040204 |
    And click on Login button
    Then user is Logged in

  @Web
  Scenario: Login to sinparty.com by clicking on Login with Google button
    When user click on sign in button
    And click on on Login with Google button
    Then google authentication page is displayed
    When user complete authentication google page
      | email                      | password  | username      |
      | testsinparty2023@gmail.com | Tester@38 | testsinpartyy |
    Then user is Logged in

  @Web @Run
  Scenario: Login to sinparty.com using invalid user account
    When user click on sign in button
    And input credentials into username and password field
      | Username | Password  |
      | kkkttt   | 123456789 |
    And click on Login button
    Then user not found pop-up is displayed
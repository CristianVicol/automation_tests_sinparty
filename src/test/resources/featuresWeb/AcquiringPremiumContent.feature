@Web
Feature: Acquiring premium content tests

  @Web
  Scenario: User can subscribe to the premium content of the creator
    Given user is on the creator page to which  is not subscribed
      | username    | password | creator name |
      | testaccount | 02040204 | spbigbear69  |
    When user click on premium posts section
    And click on subscribe now button
    Then pop-up is displayed with subscribe options
    When user click on subscribe button from a chosen option
    Then purchase successful pop-up is displayed
    When user click on continue button from the pop-up
    Then congratulations pop-up is displayed with send message to creator option on it
      | message |
      | Hello!  |
    And user is subscribed to the creator

  @Web
  Scenario: User can unsubscribe from creator's premium content
    Given user is logged in
      | username    | password |
      | testaccount | 02040204 |
    When user navigate to Active subscriptions section
    And click on cancel subscription option
    Then proceed to cancel pop-up is displayed
    When user click on proceed to cancel button
    Then provide reason pop-up is displayed
    When user provide reason
    And select a rate for the creator
    And click on cancel subscription button from the provide reason pop-up
    Then subscription canceled pop-up is displayed
    And subscription is not present in the active section
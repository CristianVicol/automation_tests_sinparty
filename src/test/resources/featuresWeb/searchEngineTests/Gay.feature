@Web @Run
Feature: Search Engine tests (In the header of the page, search logo)

  Background:
    Given user is on the search page - gay

  @Web
  Scenario Outline: User can search a model by his/her name
    When user tap name '<Names>' of the creator into search field
    And click on search icon
    Then creator is displayed on the page
    Examples:
      | Names        |
      | Lateron      |
      | JasonLegend  |
  @Web
  Scenario: User can open a category by clicking on any random value when All button is selected
    When user click on All button, after selecting any other button: creatorsBtn, liveCamsBtn, pornVideosBtn
    And click on any random value
    Then a new window is opened with the creators page

  @Web
  Scenario: User can open a category by clicking on any random value when Creators button is selected
    When user click on Creators button
    And click on any random value
    Then a new window is opened with the creators page

  @Web
  Scenario: User can open a category by clicking on any random value when Live cams button is selected
    When user click on Live cams button
    And click on any random value from live cams
    Then a new window is opened with the creators page

  @Web
  Scenario: User can open a category by clicking on any random value when Porn Videos button is selected
    When user click on Porn Videos button
    And click on any random value from porn videos
    Then a new window is opened with the video page

  @Web
  Scenario: User can open a category by clicking on any random value when Channels button is selected
    When user click on Channels button
    And click on any random value from channels results
    Then a new window is opened with channels video
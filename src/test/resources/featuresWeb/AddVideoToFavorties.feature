@Web
Feature: Add video to favorite tests

  @Web
  Scenario: Logged in user can add video to favorites
    Given user is logged in on the main page
      | username    | password |
      | testaccount | 02040204 |
    When user click on add to favorites icon from a video
    Then add to pop-up is displayed
    When user click on the playlist
    And click on add button
    Then video added to playlist notification is displayed
    And the video is present in my favorites videos section

  @Web
  Scenario: Not authorized user try to add video to favorites
    When user in on the home page
    And not authorized user click on add to favorites icon from a video
    Then pop-up is displayed where log in is advised
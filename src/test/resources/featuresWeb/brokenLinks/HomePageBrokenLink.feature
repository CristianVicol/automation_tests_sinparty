@Web @Run
Feature: Check for broken links on home page

  @Web
  Scenario: Ensure that on home page are not broken link when user is not logged in
    Given not logged in user is on homepage
    Then links displayed on the page are not broken

  @Web
  Scenario: Ensure that on home page are not broken link when user is logged in
    Given user is on homepage
      | Username    | Password |
      | testaccount | 02040204 |
    Then links displayed on the page are not broken
@Web @Run
Feature: Check functionality of gender icons on the web main page

  Background:
    Given user access the website

  @Web
  Scenario: Upon accessing the website a notification appears at the gender with information
    Then a notification appears at the gender with information

  @Web
  Scenario: User can change orientation content by clicking on the gender icons
    When user click on gay orientation button
    Then user is redirected to the page with "gay" content
    When user click on trans orientation button
    Then user is redirected to the page with "transgender" content
    When user click on straight orientation button
    Then user is redirected to straight content button
    |gay|transgender|
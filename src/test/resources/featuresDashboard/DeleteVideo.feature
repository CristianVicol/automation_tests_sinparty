@Dashboard @Run
Feature: Creator delete video tests

  Background:
    Given creator is on the main dashboard page
      | username                | password |
      #| yatetin673@vaband.com | yatetin673@vaband.com |
      | diyoka4406@onlcool.com | 02040204 |

  @DeleteVideo
  Scenario: Creator can delete a video from the content page
    Given creator has a video on the content page
    When creator click on delete button
    Then confirmation pop-up is displayed
    When creator confirm deletion
    Then video is not present on the content page
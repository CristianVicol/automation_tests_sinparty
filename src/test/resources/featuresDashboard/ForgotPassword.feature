@Dashboard @Run
Feature: Forgot password test

  @Dashboard
  Scenario: Creator can change password by clicking on forgot password link
    Given creator is on log in page
    When creator click on forgot password link
    Then forgot password page is displayed for providing email
    When creator tap the email "testsinparty2023@gmail.com" into the field
    And click on Reset Password button
    Then mail with reset password link is sent to the email address provided from "content@sinparty.com"
    When creator access the link sent to email address
    Then creator is redirected to the reset password section
    When creator insert new password into the fields
    And click on Reset Password button
    Then creator is redirected to log in page
    And the creator can log in with new password
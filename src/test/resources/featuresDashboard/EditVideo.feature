@Dashboard @Run
Feature: Edit video tests

  Background:
    Given creator is on the main dashboard page
      | username                 | password  |
      | testsinparty2023USERNAME | Tester@38 |

  Scenario: Creator can edit video details with valid data
    When creator navigates to the content page
    And click on the edit button
    Then edit video pop-up is displayed
    When creator edit video name
    And description
    And category
    And tags
    And click on publish button after editing
    Then video is edited
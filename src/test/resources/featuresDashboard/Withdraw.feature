@Dashboard
Feature: Creator withdraw tests

  @Dashboard
  Scenario: Creator can not withdraw less than 20$
    Given creator is on the main dashboard page
      | username                | password |
      | creatorsqaevg@gmail.com | Tester39 |
    When creator click on withdraw button
    Then withdraw pop-up is displayed
    When creator insert withdraw amount 19
    And select payout method
    Then request withdrawal button is disabled

  @Dashboard
  Scenario: Creator can withdraw 20$ from his account
    Given creator is on the main dashboard page
      | username                | password |
      | creatorsqaevg@gmail.com | Tester39 |
    When creator click on withdraw button
    Then withdraw pop-up is displayed
    When creator insert withdraw amount 20
    And select payout method
    And click on request withdraw button
    Then Withdraw request pop-up is displayed
    When creator click on proceed button
    Then request complete pop-up is displayed
    And transaction is displayed on the transaction page
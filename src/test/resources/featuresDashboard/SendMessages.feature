@Dashboard @Run
Feature: Creator send messages tests

  Background:
    Given creator is on the main dashboard page
      | username               | password |
      | diyoka4406@onlcool.com | 02040204 |

  @Dashboard
  Scenario Outline: Creator can send mass message - without attachments
    Given creator is on the inbox page
    When creator click on New Mass Message button
    Then pop-up with message details is displayed
    When creator click on '<Receivers>' button
    And type message into the message text area
      | Message     |
      | TestMessage |
    And select an emoji
    And click on Send Message button
    Then message is displayed in the chat
    Examples:
      | Receivers   |
      | All         |
      | Followers   |
      | Subscribers |

  @Dashboard
  Scenario Outline: Creator can send mass message with attachments
    Given creator is on the inbox page
    When creator click on New Mass Message button
    Then pop-up with message details is displayed
    When creator click on '<Receivers>' button
    And attach a file to the message
      | Price of attachment |
      | 5                   |
    And type message into the message text area
      | Message     |
      | TestMessage |
    And click on Send Message button
    Then message is displayed in the chat with attachment
    Examples:
      | Receivers |
      | All       |
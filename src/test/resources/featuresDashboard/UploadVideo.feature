@Dashboard @Run
Feature: Creator upload video tests

  Background:
    Given creator is on the main dashboard page
      | username                | password |
      | creatorsqaevg@gmail.com | Tester39 |

  @Dashboard @UploadVideo
  Scenario Outline: Creator can upload video of different formats
    When user click on upload video button
    Then upload video pop-up is displayed
    When user select a video to upload in '<Video Format>' format
    Then pop-up is displayed with video details
    When creator complete video details
      | Description       |
      | Video Description |
    And click on publish button
    Then video is posted
    Examples:
      | Video Format |
      | MP4          |
      | MOV          |
      | WMV          |

  @Dashboard @UploadVideo
  Scenario: Creator can upload a paid video
    When user click on upload video button
    Then upload video pop-up is displayed
    When user select a video to upload in 'MOV' format
    Then pop-up is displayed with video details
    When creator complete paid video details
      | Description       | Price |
      | Video Description | 5     |
    And click on publish button
    Then video is posted
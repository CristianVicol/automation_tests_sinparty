@Dashboard
Feature: As a registered creator I can log in dashboard

  Background:
    Given Dashboard log in page is displayed

  @Dashboard @Run
  Scenario: Login to dashboard.sinparty.com using valid creator account
    When input valid credentials into username or email field and password field
      | Username     | Password |
      | sp_bigbear69 | Tester39 |
    And click on login button
    Then creator is logged in
  @Dashboard
  Scenario: Login to dashboard.sinparty.com using google account
    When creator click on Log in with google button
    Then dashboard google authentication page is displayed
    When user complete dashboard authentication google page
      | email                      | password  |
      | testsinparty2023@gmail.com | Tester@38 |
    Then creator is logged in
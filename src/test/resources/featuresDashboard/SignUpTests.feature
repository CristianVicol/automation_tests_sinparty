@Dashboard
Feature: A non existing creator must be able to register

  #Background:
    #Given Dashboard sign up page is displayed

  @Dashboard
  Scenario: SignUp on the dashboard with a valid email and password
    When input valid email into email field and password in the password field
      | Email            |
      | testsinparty2023 |
    And click on sign up button
    Then creator is redirected to the onboarding page
    When creator complete the onboarding page and click continue button
      | first name | last name | Country | Citizenship |
      | Jessica    | Jessica   | Andorra | Andorra     |
    Then creator is redirected to the main page 'Email not verified. Please make sure to verify your email address to complete your profile registration. Resend Email'
    And confirmation email is sent to the email
    And creator can confirm email address

  @Dashboard @Signup
  Scenario: User can not sign up with tag longer than 25 characters
    When input valid email into email field and password in the password field
      | Email            |
      | testsinparty2023 |
    And click on sign up button
    Then creator is redirected to the onboarding page
    When creator complete the onboarding page until tag section
      | first name | last name | Country | Citizenship |
      | Jessica    | Jessica   | Andorra | Andorra     |
    And user insert "tagsstagsstagsstagsstagsss" tag
    #Then user is warned that tag limit is 25 characters
    #And continue button is disabled
package managers;

import org.openqa.selenium.WebDriver;
import pageObjects.authenticationForms.WebGoogleAuthentication;
import pageObjects.dashboard.*;
import pageObjects.authenticationForms.GoogleAuthentication;
import pageObjects.dashboard.LogInPage;
import pageObjects.dashboard.MainPage;
import pageObjects.paidChannels.Fuq;
import pageObjects.webPages.*;
import pageObjects.webPages.dsp.DspElements;
import pageObjects.webPages.dsp.DspElementsLiveCreators;
import pageObjects.webPages.dsp.DspElementsOnListingPages;
import pageObjects.webPages.dsp.HomePageDspElements;

public class PageObjectManager {
    private final WebDriver webDriver;
    private LogInPage dashboardLogInPage;
    private MainPage dashboardMainPage;
    private GoogleAuthentication googleAuthenticationPage;
    private SignUpPage dashBoardSignUpPage;
    private OnboardingPage onboardingPage;
    private MainWeb webMain;
    private CookiesPopUp cookiesPopUp;
    private SignUpForm signUpForm;
    private PrivacyPolicy privacyPolicy;
    private LogInForm logInForm;
    private SearchPage searchPage;
    private WebGoogleAuthentication webGoogleAuthentication;
    private ForgotPassword forgotPassword;
    private Fuq fuq;
    private CreatorsListing creatorsListing;
    private LiveCreatorsListing liveCreatorsListing;
    private DspElements dspElements;
    private HomePageDspElements homePageDspElements;
    private DspElementsOnListingPages dspElementsOnListingPages;
    private DspElementsLiveCreators dspElementsLiveCreators;
    private AffiliateVideo affiliateVideo;
    private ChannelPage channelPage;
    private CreatorPage creatorPage;
    private UserAccount userAccount;
    private WithdrawModal withdrawModal;
    private Transactions transactions;
    private MyVideos myVideos;
    private VideoDetailsPopUp videoDetailsPopUp;
    private ContentPage contentPage;
    private Inbox creatorInbox;
    public PageObjectManager(WebDriver webDriver){
        this.webDriver = webDriver;
    }
    public ContentPage getContentPage(){
        if(contentPage == null){
            contentPage = new ContentPage(webDriver);
        }
        return contentPage;
    }
    public Inbox getCreatorInbox(){
        if(creatorInbox == null){
            creatorInbox = new Inbox(webDriver);
        }
        return creatorInbox;
    }
    public VideoDetailsPopUp getUploadVideoDetailsPopUp(){
        if(videoDetailsPopUp == null){
            videoDetailsPopUp = new VideoDetailsPopUp(webDriver);
        }
        return videoDetailsPopUp;
    }
    public MyVideos getMyVideos(){
        if(myVideos == null){
            myVideos = new MyVideos(webDriver);
        }
        return myVideos;
    }
    public Transactions getTransactions(){
        if(transactions == null){
            transactions = new Transactions(webDriver);
        }
        return transactions;
    }
    public WithdrawModal getWithdrawModal(){
        if(withdrawModal == null){
            withdrawModal = new WithdrawModal(webDriver);
        }
        return withdrawModal;
    }
    public CreatorPage getCreatorPage(){
        if(creatorPage == null){
            creatorPage = new CreatorPage(webDriver);
        }
        return creatorPage;
    }
    public UserAccount getUserAccount(){
        if(userAccount == null){
            userAccount = new UserAccount(webDriver);
        }
        return userAccount;
    }
    public ChannelPage getChannelPage(){
        if(channelPage == null){
            channelPage = new ChannelPage(webDriver);
        }
        return channelPage;
    }
    public AffiliateVideo getAffiliateVideo(){
        if(affiliateVideo == null){
            affiliateVideo = new AffiliateVideo(webDriver);
        }
        return affiliateVideo;
    }
    public LiveCreatorsListing getLiveCreatorsListing(){
        if(liveCreatorsListing == null){
            liveCreatorsListing = new LiveCreatorsListing(webDriver);
        }
        return liveCreatorsListing;
    }
    public DspElementsLiveCreators getDspElementsLiveCreators(){
        if(dspElementsLiveCreators == null){
            dspElementsLiveCreators = new DspElementsLiveCreators(webDriver);
        }
        return dspElementsLiveCreators;
    }
    public DspElementsOnListingPages getDspElementsOnListingPages(){
        if(dspElementsOnListingPages == null){
            dspElementsOnListingPages = new DspElementsOnListingPages(webDriver);
        }
        return dspElementsOnListingPages;
    }
    public HomePageDspElements getHomePageDspElements(){
        if(homePageDspElements == null){
            homePageDspElements = new HomePageDspElements(webDriver);
        }
        return homePageDspElements;
    }
    public CreatorsListing getCreatorsListing(){
        if(creatorsListing == null){
            creatorsListing = new CreatorsListing(webDriver);
        }
        return creatorsListing;
    }
    public DspElements getDspElements(){
        if(dspElements == null){
            dspElements = new DspElements(webDriver);
        }
        return dspElements;
    }
    public Fuq getPaidChannel(){
        if(fuq == null){
            fuq = new Fuq(webDriver);
        }
        return fuq;
    }
    public ForgotPassword getForgotPassword(){
        if(forgotPassword == null){
            forgotPassword = new ForgotPassword(webDriver);
        }
        return forgotPassword;
    }

    public WebGoogleAuthentication getGoogleAuthentication() {
        if(webGoogleAuthentication == null){
            webGoogleAuthentication = new WebGoogleAuthentication(webDriver);
        }
        return webGoogleAuthentication;
    }

    public MainWeb getMainWeb(){
        if(webMain == null){
            webMain = new MainWeb(webDriver);
        }
        return webMain;
    }
    public SearchPage getSearchPage(){
        if(searchPage == null){
            searchPage = new SearchPage(webDriver);
        }
        return searchPage;
    }
    public LogInForm getLogInForm(){
        if(logInForm == null){
            logInForm = new LogInForm(webDriver);
        }
        return logInForm;
    }
    public PrivacyPolicy getPrivacyPolicy(){
        if(privacyPolicy == null){
            privacyPolicy = new PrivacyPolicy(webDriver);
        }
        return privacyPolicy;
    }
    public SignUpForm getSignUpForm(){
        if(signUpForm == null){
            signUpForm = new SignUpForm(webDriver);
        }
        return signUpForm;
    }
    public CookiesPopUp getCookiesPopUp(){
        if(cookiesPopUp == null){
            cookiesPopUp = new CookiesPopUp(webDriver);
        }
        return cookiesPopUp;
    }
    public OnboardingPage getOnboardingPage(){
        if(onboardingPage == null){
            onboardingPage = new OnboardingPage(webDriver);
        }
        return onboardingPage;
    }
    public SignUpPage getDashBoardSignUpPage(){
        if(dashBoardSignUpPage == null){
            dashBoardSignUpPage = new SignUpPage(webDriver);
        }
        return dashBoardSignUpPage;
    }
    public GoogleAuthentication getGoogleAuthentificationPage(){
        if(googleAuthenticationPage == null){
            googleAuthenticationPage = new GoogleAuthentication(webDriver);
        }
        return googleAuthenticationPage;
    }
    public MainPage getDashboardMainPage(){
        if(dashboardMainPage == null){
            dashboardMainPage = new MainPage(webDriver);
        }
        return dashboardMainPage;
    }
    public LogInPage getDashboardLogInPage(){
        if(dashboardLogInPage == null){
            dashboardLogInPage = new LogInPage(webDriver);
        }
        return dashboardLogInPage;
    }
}
package utilities;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.googleapis.json.GoogleJsonError;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.GmailScopes;
import com.google.api.services.gmail.model.ListMessagesResponse;
import com.google.api.services.gmail.model.Message;
import com.google.api.services.gmail.model.MessagePart;
import org.apache.commons.codec.binary.Base64;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.security.GeneralSecurityException;
import java.util.*;

import static javax.mail.Message.RecipientType.TO;

public class GmailReader {
    private static final String TEST_EMAIL = "testsinparty2023@gmail.com";
    private final Gmail service;

    public GmailReader() throws GeneralSecurityException, IOException {
        NetHttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();
        GsonFactory jsonFactory = GsonFactory.getDefaultInstance();
        service = new Gmail.Builder(httpTransport, jsonFactory, getCredentials(httpTransport, jsonFactory))
                .setApplicationName("SinPartyTestAutomation")
                .build();
    }

    public static Credential getCredentials(final NetHttpTransport httpTransport, GsonFactory jsonFactory)
            throws IOException {
        //Load client secrets
        GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(jsonFactory, new InputStreamReader(GmailReader.class.getResourceAsStream("/client_secret_518446568119-405j5vfv0l1gcc5ahraghdsr732evoht.apps.googleusercontent.com.json")));

        //Build flow and trigger user authorization request
        Set<String> scopes = new HashSet<>(Arrays.asList(GmailScopes.GMAIL_SEND, GmailScopes.GMAIL_READONLY,
                GmailScopes.GMAIL_MODIFY, GmailScopes.MAIL_GOOGLE_COM));
        GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
                httpTransport, jsonFactory, clientSecrets, scopes)
                .setDataStoreFactory(new FileDataStoreFactory(Paths.get("tokens").toFile()))
                .setAccessType("offline")
                .build();
        LocalServerReceiver receiver = new LocalServerReceiver.Builder().setPort(8888).build();
        return new AuthorizationCodeInstalledApp(flow, receiver).authorize("user");
    }

    public void sendMail(String subject, String message) throws GeneralSecurityException, IOException, MessagingException {

        Properties prop = new Properties();
        Session session = Session.getDefaultInstance(prop, null);
        MimeMessage email = new MimeMessage(session);
        email.setFrom(new InternetAddress(TEST_EMAIL));
        email.addRecipient(TO, new InternetAddress(TEST_EMAIL));
        email.setSubject(subject);
        email.setText(message);

        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        email.writeTo(buffer);
        byte[] rawMessageBytes = buffer.toByteArray();
        String encodedEmail = Base64.encodeBase64URLSafeString(rawMessageBytes);
        Message msg = new Message();
        msg.setRaw(encodedEmail);

        try {
            msg = service.users().messages().send("me", msg).execute();
            System.out.println("Message id: " + msg.getId());
            System.out.println(msg.toPrettyString());
        } catch (GoogleJsonResponseException e) {
            GoogleJsonError error = e.getDetails();
            if (error.getCode() == 403) {
                System.err.println("Unable to send message " + e.getDetails());
            } else {
                throw e;
            }
        }
    }

    public String getLinkFromInbox(String email) throws IOException, InterruptedException {
        long initialDelay = 1000; // in milliseconds
        long maxDelay = 32000; // in milliseconds
        long delay = initialDelay;
        int retryCount = 0;
        int maxRetryCount = 5;
        Message message = null;
        while(retryCount < maxRetryCount){
            try{
                ListMessagesResponse response = service.users().messages().list("me").setQ("from:" + email)
                        .setMaxResults(1L).set("orderBy", "date desc").execute();
                List<Message> messages = response.getMessages();
                if(messages != null && !messages.isEmpty()){
                    String messageId  = messages.get(0).getId();
                    message = service.users().messages().get("me", messageId).execute();
                    break;
                }
            }catch (IOException ioe){
                ioe.printStackTrace();
            }
            retryCount++;
            try {
                Thread.sleep(delay);
                delay = Math.min(maxDelay, delay * 2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        List<MessagePart> parts = message.getPayload().getParts();
        for (MessagePart part : parts) {
            String mimeType = part.getMimeType();
            if (mimeType.equals("text/html")) {
                // Decode the message body from base64
                byte[] bodyBytes = Base64.decodeBase64(part.getBody().getData());
                String htmlBody = new String(bodyBytes, StandardCharsets.UTF_8);
                // JSoup to parse the HTML and extract the link
                Document doc = Jsoup.parse(htmlBody);
                Elements links = doc.select("a[href]");
                return links.get(1).attr("href");
            }
        }
        return "No link found!!!";
    }
    public void deleteEmails() {
        try {
            ListMessagesResponse response = service.users().messages().list("me").execute();
            List<Message> messages = response.getMessages();
            for (Message message : messages) {
                service.users().messages().delete("me", message.getId()).execute();
            }
        }catch (IOException e){
            e.printStackTrace();
        }
    }
}
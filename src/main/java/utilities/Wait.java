package utilities;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;

public class Wait {
    private static void until(WebDriver webDriver, Long timeOutInSeconds, Function<WebDriver, Boolean> waitCondition) {
        WebDriverWait webDriverWait = new WebDriverWait(webDriver, timeOutInSeconds);
        try {
            webDriverWait.until(waitCondition);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static void untilPageReadyState(WebDriver webDriver, Long timeOutInSeconds) {
        until(webDriver, timeOutInSeconds, (function) -> {
            String isPageLoaded = String.valueOf(((JavascriptExecutor) webDriver).executeScript("return document.readyState"));
            if (isPageLoaded.equals("complete")) {
                return true;
            } else {
                System.out.println("Page is loading");
                return false;
            }
        });
    }

    public static void untilTextIsPresentInTheElement(WebDriver webDriver, WebElement webElement, Long timeOutInSeconds,
                                                      String text) {
        new WebDriverWait(webDriver, timeOutInSeconds)
                .until(ExpectedConditions.textToBePresentInElement(webElement, text));
    }

    public static void untilElementIsVisible(WebDriver webDriver, WebElement webElement, Long timeOutInSeconds) {
        new WebDriverWait(webDriver, timeOutInSeconds).until(ExpectedConditions.visibilityOf(webElement));
    }

    public static void untilElementIsClickable(WebDriver webDriver, WebElement webElement, Long timeOutInSeconds) {
        new WebDriverWait(webDriver, timeOutInSeconds)
                .until(ExpectedConditions.elementToBeClickable(webElement));
    }

    public static void untilElementToBeSelected(WebDriver driver, WebElement webElement, Long timeOutInSeconds) {
        new WebDriverWait(driver, timeOutInSeconds)
                .until(ExpectedConditions.elementToBeSelected(webElement));
    }

    public static void untilStalenessOfElement(WebDriver webDriver, WebElement webElement, Long timeOutInSeconds) {
        new WebDriverWait(webDriver, timeOutInSeconds)
                .until(ExpectedConditions.stalenessOf(webElement));
    }

    public static void untilUrlIs(WebDriver webDriver, String url, Long timeOutInSeconds) {
        new WebDriverWait(webDriver, timeOutInSeconds)
                .until(ExpectedConditions.urlToBe(url));
    }

    public static void untilTextIs(WebDriver webDriver, By by, String text, Long timeout) {
        new WebDriverWait(webDriver, timeout)
                .until(ExpectedConditions.textToBe(by, text));
    }

    public static void untilListElementIsVisible(WebDriver webDriver, List<WebElement> webElements, Long timeOutInSeconds) {
        new WebDriverWait(webDriver, timeOutInSeconds).until(ExpectedConditions.visibilityOfAllElements(webElements));
    }

    public static void waitUntilPageFullyLoaded(WebDriver webDriver, Long timeout) {
        new WebDriverWait(webDriver, timeout).until((ExpectedCondition<Boolean>) driver -> ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete"));
    }

    public static void textToBePresent(WebDriver driver,WebElement webElement, Long timeOut, String subString) {
        new WebDriverWait(driver, timeOut)
                .until((ExpectedCondition<Boolean>) var -> {
                    String webElementText = webElement.getText();
                    return webElementText.contains(subString);
                });
    }

}

//WebElement webElement = d.findElement(By.xpath(xpath));
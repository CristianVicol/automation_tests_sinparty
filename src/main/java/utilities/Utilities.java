package utilities;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import pageObjects.webPages.CookiesPopUp;
import pageObjects.webPages.dsp.DspElements;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class Utilities {

    public static void switchWindow(WebDriver driver, String originalWindow) {
        Set<String> windowHandles = driver.getWindowHandles();
        for (String windowHandle : windowHandles) {
            if (!windowHandle.equals(originalWindow)) {
                driver.close();
                driver.switchTo().window(windowHandle);
                break;
            }
        }
    }

    public static void scrollWindow(WebDriver driver, int start, int end) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(" + start + "," + end + ")");
    }

    public static void moveToElement(WebDriver webDriver, WebElement webElement) {
        Actions actions = new Actions(webDriver);
        actions.moveToElement(webElement).perform();
    }

    public static void preparePageForDspTests(TestContext testContext, DspElements dspElements,
                                              CookiesPopUp cookiesPopUp) {
        Wait.untilElementIsVisible(testContext.getDriverManager().getWebDriver(),
                dspElements.getCloseAdvPopUpBtn(), 30L);
        dspElements.clickCloseAdvPopUpBtn();
        try {
            testContext.getDriverManager().getWebDriver().manage().timeouts().implicitlyWait(2L, TimeUnit.SECONDS);
            dspElements.clickCloseVideoSliderBtn();
        }catch (NoSuchElementException e){
        }
        clickRejectCookiesIfDisplayed(testContext, cookiesPopUp);
    }

    public static void clickRejectCookiesIfDisplayed(TestContext testContext, CookiesPopUp cookiesPopUp) {
        try {
            testContext.getDriverManager().getWebDriver().manage().timeouts().implicitlyWait(2L, TimeUnit.SECONDS);
            cookiesPopUp.clickRejectAllCookiesBtn();
        } catch (NoSuchElementException e) {
        }
    }
    public static String getCurrentDate() {
        LocalDate currentDate = LocalDate.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd MMM yyyy");
        return currentDate.format(formatter);
    }
    public static void moveToWebElement(WebDriver webDriver, WebElement webElement){
        Actions actions = new Actions(webDriver);
        actions.moveToElement(webElement).perform();
    }
    public static void acceptAlertIfPresent(WebDriver webDriver){
        try {
            Alert alert = webDriver.switchTo().alert();
            alert.accept();
        } catch (NoAlertPresentException npe) {
        }
    }
    public static void deleteTextFromElement(WebDriver webDriver, WebElement webElement){
        Actions actions = new Actions(webDriver);
        actions.moveToElement(webElement)
                .click(webElement)
                .keyDown(Keys.CONTROL)
                .sendKeys("a")
                .keyUp(Keys.CONTROL)
                .sendKeys(Keys.BACK_SPACE)
                .build()
                .perform();
    }
    public static void closePopUpIfDisplayed(TestContext testContext, WebElement closePopUp){
        try{
            testContext.getDriverManager().getWebDriver().manage().timeouts().implicitlyWait(2L, TimeUnit.SECONDS);
            closePopUp.click();
        }catch (NoSuchElementException e){

        }
    }
    public static void sleep(int milliseconds){
        try{
            Thread.sleep(milliseconds);
        }catch (InterruptedException e){

        }
    }
}
package enums;

public enum Context {
    USERNAME,
    LINK,
    NEW_PASSWORD,
    EMAIL,
    TEXT,
    CREATOR_NAME,
    VIDEO_NAME_TEXT,
    WINDOW,
    VIDEO_NAME,
    PASSWORD,
    MESSAGE,
    RECEIVER,
    PRICE,
    TYPE,
    INDEX_OF_CREATOR,
    INDEX,
    HREF,
    CATEGORY
}
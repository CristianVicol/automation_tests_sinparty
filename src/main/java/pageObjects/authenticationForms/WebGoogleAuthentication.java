package pageObjects.authenticationForms;

import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@Getter
public class WebGoogleAuthentication {
    WebDriver webDriver;
    public WebGoogleAuthentication(WebDriver webDriver){
        this.webDriver = webDriver;
        PageFactory.initElements(webDriver, this);
    }
    @FindBy(xpath = "//input[@type='email']")
    private WebElement emailField;
    @FindBy(css = ".YKooDc .Xb9hP input")
    private WebElement passwordField;
    @FindBy(css = ".VfPpkd-dgl2Hf-ppHlrf-sM5MNb .VfPpkd-LgbsSe-OWXEXe-k8QpJ")
    private WebElement nextBtn;

    public boolean googleAuthenticationPageIsDisplayed(){
        return emailField.isDisplayed();
    }

}
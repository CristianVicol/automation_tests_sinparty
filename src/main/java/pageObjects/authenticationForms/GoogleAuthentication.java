package pageObjects.authenticationForms;

import lombok.Getter;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utilities.Wait;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
@Getter
public class GoogleAuthentication {
    private final WebDriver webDriver;
    public GoogleAuthentication(WebDriver webDriver){
        this.webDriver = webDriver;
        PageFactory.initElements(webDriver, this);
    }
    @FindBy(xpath = "//input[contains(@autocomplete, 'username')]")
    private WebElement emailField;
    @FindBy(css = ".VfPpkd-dgl2Hf-ppHlrf-sM5MNb .VfPpkd-LgbsSe-OWXEXe-k8QpJ")
    private WebElement googleContinueBtn;
    @FindBy(css = ".YKooDc .Xb9hP input")
    private WebElement passwordField;
    @FindBy(css = ".hDp5Db .whsOnd")
    private WebElement tfPasswordToConfirmEmail;
    @FindBy(xpath = "(//div[@class='VfPpkd-dgl2Hf-ppHlrf-sM5MNb']/button)[2]")
    private WebElement nextBtn;
    public void clickNextBtn(){
        Wait.untilElementIsClickable(webDriver, nextBtn, 5L);
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        js.executeScript("arguments[0].click();", nextBtn);
    }
}
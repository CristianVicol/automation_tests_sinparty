package pageObjects.dashboard;

import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
@Getter
public class Transactions {
    private WebDriver webDriver;

    public Transactions(WebDriver webDriver){
        this.webDriver = webDriver;
        PageFactory.initElements(webDriver, this);
    }
    @FindBy(xpath = "(//div[contains(@class, 'transactions_date')]//span[contains(@class, 'mr-0')])[1]")
    private WebElement transactionDate;
}
package pageObjects.dashboard;

import lombok.Getter;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.Wait;
import java.util.Random;

@Getter
public class SignUpPage {
    private final WebDriver webDriver;

    public SignUpPage(WebDriver webDriver) {
        this.webDriver = webDriver;
        PageFactory.initElements(webDriver, this);
    }
    @FindBy(xpath = "//input[@placeholder='example@email.com']")
    private WebElement emailField;
    @FindBy(xpath = "//input[@placeholder='Password']")
    private WebElement passwordField;
    @FindBy(xpath = "//button[contains(@class, 'rounded')]")
    private WebElement signUpBtn;

    public void completeSignUpForm(String email) {
        String newEmail = generateEmail(email);
        Actions actions = new Actions(webDriver);
        try {
            Wait.untilElementIsVisible(webDriver, emailField, 25L);
            actions.moveToElement(emailField).click().sendKeys(newEmail).perform();
            passwordField.sendKeys("pass1234");
        } catch (StaleElementReferenceException | ElementNotInteractableException e) {
            WebElement emailField = webDriver.findElement(By.xpath("//input[@placeholder='example@email.com']"));
            WebElement passwordField = webDriver.findElement(By.xpath("//input[@placeholder='Password']"));
            Wait.untilElementIsVisible(webDriver, emailField, 25L);
            actions.moveToElement(emailField).click().sendKeys(newEmail).perform();
            passwordField.sendKeys("pass1234");
        }
    }

    public void clickSignUpBtn() {
        Wait.untilElementIsClickable(webDriver, signUpBtn, 25L);
        signUpBtn.click();
    }

    private String generateEmail(String email) {
        Random rand = new Random();
        int n = rand.nextInt(10000000);
        return email + "+" + n + "@gmail.com";
    }
}
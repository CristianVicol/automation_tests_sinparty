package pageObjects.dashboard;

import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.Wait;

@Getter
public class ContentPage {
    private WebDriver webDriver;
    public ContentPage(WebDriver webDriver){
        this.webDriver = webDriver;
        PageFactory.initElements(webDriver, this);
    }
    @FindBy(xpath = "(//div[contains(@class, 'content_title')])[1]")
    private WebElement videoTitle;
    @FindBy(xpath = "//div[@class='content_status']//div[contains(@class, 'content_state__live')]")
    private WebElement videoLiveState;
    @FindBy(xpath = "//div[@class='content_item']")
    private WebElement videoContentItem;
    @FindBy(xpath = "//button[contains(@class, 'content_actions--delete')]")
    private WebElement deleteVideoBtn;
    @FindBy(xpath = "(//button[contains(@class, 'confirm-button')])[2]")
    private WebElement confirmDeletionBtn;
    @FindBy(css = ".content_actions--wrapper .content_actions--edit")
    private WebElement editVideoBtn;
    ////button[contains(@class, 'content_actions--edit')]

    public void clickDeleteVideoBtn(){
        Wait.untilElementIsVisible(webDriver, videoContentItem, 30L);
        Actions actions = new Actions(webDriver);
        actions.moveToElement(videoContentItem).perform();
        Wait.untilElementIsVisible(webDriver, deleteVideoBtn, 30L);
        deleteVideoBtn.click();
    }
    public void clickConfirmDeletionBtn(){
        Wait.untilElementIsVisible(webDriver, confirmDeletionBtn, 30L);
        confirmDeletionBtn.click();
    }
    public void clickEditVideoBtn(){
        Wait.untilElementIsVisible(webDriver, videoContentItem, 30L);
        Actions actions = new Actions(webDriver);
        actions.moveToElement(videoContentItem).perform();
        Wait.untilElementIsClickable(webDriver, editVideoBtn, 30L);
        editVideoBtn.click();
    }
}

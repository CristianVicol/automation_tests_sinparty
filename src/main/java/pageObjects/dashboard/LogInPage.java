package pageObjects.dashboard;

import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.Wait;

import java.util.ArrayList;
import java.util.List;

@Getter
public class LogInPage {
    private final WebDriver webDriver;

    public LogInPage(WebDriver webDriver) {
        this.webDriver = webDriver;
        PageFactory.initElements(webDriver, this);
    }

    @FindBy(xpath = "//span[contains(@style, 'color:')]")
    private WebElement logInText;
    @FindBy(xpath = "//img[@src='/images/logo.png']")
    private WebElement sinpartyImage;
    @FindBy(xpath = "(//div[@class='auth_social_buttons-button__control'])[1]")
    private WebElement googleLogInBtn;
    @FindBy(xpath = "(//div[@class='auth_social_buttons-button__control'])[2]")
    private WebElement twitterLogInBtn;
    @FindBy(id = "input-login-username-email")
    private WebElement tfLoginUsername;
    @FindBy(id = "input-login-password")
    private WebElement tfLoginPassword;
    @FindBy(xpath = "//div[contains(@class, 'auth_action')]")
    private WebElement logInButton;
    @FindBy(css = ".auth_register .auth_link")
    private WebElement signUpLink;
    @FindBy(xpath = "//div[contains(@class, 'justify-end')]/a")
    private WebElement forgotPasswordLink;

    public void clickSignUpLink() {
        Wait.untilElementIsVisible(webDriver, signUpLink, 5L);
        Actions actions = new Actions(webDriver);
        actions.moveToElement(signUpLink).click(signUpLink).perform();
    }
    public void fillUsernameAndPassword(String username, String password) {
        tfLoginUsername.sendKeys(username);
        tfLoginPassword.sendKeys(password);
    }
}

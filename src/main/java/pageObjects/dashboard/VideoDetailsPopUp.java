package pageObjects.dashboard;

import lombok.Getter;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.Wait;

import java.io.File;
import java.util.List;

@Getter
public class VideoDetailsPopUp {
    private final Actions actions;
    private final WebDriver webDriver;
    public VideoDetailsPopUp(WebDriver webDriver){
        this.webDriver = webDriver;
        actions = new Actions(webDriver);
        PageFactory.initElements(webDriver, this);
    }
    @FindBy(xpath = "//input[contains(@placeholder, 'title')]")
    private WebElement videoTitleTextField;
    @FindBy(css = ".free-video-anchor button")
    private WebElement freeVideoBtn;
    @FindBy(css = ".paid-anchor button")
    private WebElement paidVideoBtn;
    @FindBy(css = ".video-configuration__video-preview .section__title")
    private WebElement videoPreviewText;
    @FindBy(css = ".section-tags .v-select__slot input:nth-child(1)")
    private List<WebElement> addTagForVideoInput;
    @FindBy(css = ".toasted-container .toast_alert")
    private WebElement toastAlert;
    @FindBy(css = ".section-tags .section__title")
    private WebElement selectTagText;
    @FindBy(xpath = "//div[@class='v-messages__message']")
    private WebElement warningMessageForVideoTitle;
    @FindBy(xpath = "//textarea[contains(@placeholder, 'description')]")
    private WebElement videoDescriptionTextArea;
    @FindBy(css = ".section-audience button:nth-child(1)")
    private WebElement straightAudience;
    @FindBy(css = ".section-audience button:nth-child(2)")
    private WebElement gayAudience;
    @FindBy(css = ".section-audience button:nth-child(3)")
    private WebElement transAudience;
    @FindBy(css = ".section-categories .v-input__slot")
    private WebElement selectCategory;
    @FindBy(css = ".section-tags .v-input__slot")
    private WebElement addTags;
    @FindBy(xpath = "//footer[@class='video-configuration__footer']//button[contains(@class, 'v-btn--rounded')]")
    private WebElement publishButton;
    @FindBy(xpath = "(//div[@class='v-list-item__content'])[1]")
    private WebElement categoryFromList;
    @FindAll(@FindBy(xpath = "//div[@class='v-list-item__content']"))
    private List<WebElement> listOfCategories;
    @FindBy(xpath = "//footer[@class='video-configuration__footer']/span")
    private WebElement footer;
    @FindBy(css = ".video-configuration__header button")
    private WebElement closeBtn;
    @FindBy(css = ".app-number-field .v-text-field__slot input")
    private WebElement setPriceField;
    @FindBy(xpath = "(//span[contains(@class, 'cardCustom-chip')]//button)[1]")
    private WebElement closeTagBtn;

    public void clickOnStraightAudience(){
        Actions actions = new Actions(webDriver);
        actions.moveToElement(straightAudience).perform();
        Wait.untilElementIsClickable(webDriver, straightAudience, 30L);
        straightAudience.click();
    }
    public void setCategory(){
        Actions actions = new Actions(webDriver);
        actions.moveToElement(selectCategory).perform();
        Wait.untilElementIsClickable(webDriver, selectCategory, 30L);
        selectCategory.click();
        Wait.untilElementIsVisible(webDriver, categoryFromList, 30L);
        categoryFromList.click();
    }
    public void clickPublishButton() {
        Wait.untilTextIs(webDriver, By.xpath("//footer[@class='video-configuration__footer']/span"),
                "", 120L);
        Wait.untilElementIsClickable(webDriver, publishButton, 120L);
        publishButton.click();
    }
    public void clickCloseTagBtn(){
        Wait.untilElementIsVisible(webDriver, closeTagBtn, 30L);
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        js.executeScript("arguments[0].click();", closeTagBtn);
    }
    public void clickPublishWithoutTranscoding() {
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        js.executeScript("arguments[0].click();", publishButton);
    }
    public String getVideoName(){
        String currentWorkingDirectory = System.getProperty("user.dir");
        String relativePath = "src/main/resources/testVideo/test.mp4";
        String filePath = currentWorkingDirectory + "/" +relativePath;
        File videoFile = new File(filePath);
        String[] videoNameParts = videoFile.getName().split("\\.");
        return videoNameParts[0];
    }
    public void setPrice(String price){
        Actions actions = new Actions(webDriver);
        actions.moveToElement(setPriceField).perform();
        setPriceField.sendKeys(price);
    }
    public void clickPaidVideoButton(){
        Wait.untilElementIsClickable(webDriver, paidVideoBtn, 60L);
        paidVideoBtn.click();
    }
}
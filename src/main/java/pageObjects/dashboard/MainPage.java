package pageObjects.dashboard;

import lombok.Getter;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.Wait;

import java.util.List;
@Getter
public class MainPage {
    private final WebDriver webDriver;
    public MainPage(WebDriver webDriver) {
        this.webDriver = webDriver;
        PageFactory.initElements(webDriver, this);
    }
    @FindBy(xpath = "//div[@class='tiles_title']")
    private WebElement overviewTitle;
    @FindBy(xpath = "(//a[@href='/dashboard'])[1]")
    private WebElement myDashboardBtn;
    @FindBy(xpath = "(//a[@href='/dashboard/inbox'])[1]")
    private WebElement inboxBtn;
    @FindBy(css = ".v-toolbar__content .v-btn--rounded")
    private WebElement uploadVideoBtn;
    @FindBy(css = ".video-select .video-select__file-input")
    private WebElement uploadVideoFilesInput;
    @FindBy(css = ".video-select__title:nth-child(2)")
    private WebElement uploadVideoPopUpTitle;
    @FindBy(xpath = "(//div[@class='v-input__slot']/div[@class='v-text-field__slot'])[15]")
    private WebElement videoNameInput;
    @FindBy(css = ".video-configuration__button-type .v-btn__content")
    private List<WebElement> typeOfVideoBtns;
    @FindBy(xpath = "//div[contains(@class, 'email__container-wrapper')]")
    private WebElement verifyEmailText;
    @FindBy(css = ".pa-10 .v-btn--rounded")
    private WebElement continueBtnFromPopUpAfterSignUp;
    @FindBy(css = ".user-balance:nth-child(2) .withdraw-btn")
    private WebElement withdrawButton;
    @FindBy(xpath = "(//div[@class='v-navigation-drawer__content']//li)[6]")
    private WebElement transactionsButton;
    @FindBy(xpath = "(//a[@href='/dashboard/content'])[1]")
    private WebElement contentBtn;

    public void clickContinueBtnFromPopUpAfterSignUp() {
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        js.executeScript("arguments[0].scrollIntoView(true);", continueBtnFromPopUpAfterSignUp);
        js.executeScript("arguments[0].click();", continueBtnFromPopUpAfterSignUp);
    }

    public void clickContentBtn() {
        Wait.untilElementIsVisible(webDriver, contentBtn, 30L);
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        js.executeScript("arguments[0].click();", contentBtn);
    }
    public void clickMyDashboardBtn(){
        Wait.untilElementIsVisible(webDriver, myDashboardBtn, 30L);
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        js.executeScript("arguments[0].click();", myDashboardBtn);
    }
    public void clickInboxBtn(){
        Wait.untilElementIsVisible(webDriver, myDashboardBtn, 30L);
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        js.executeScript("arguments[0].click();", inboxBtn);
    }
}
package pageObjects.dashboard;

import lombok.Getter;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.Utilities;
import utilities.Wait;

import java.util.List;
@Getter
public class Inbox {
    private WebDriver webDriver;
    public Inbox(WebDriver webDriver){
        this.webDriver = webDriver;
        PageFactory.initElements(webDriver, this);
    }
    @FindAll(@FindBy(xpath = "//div[contains(@class, 'chat-message--personal')]"))
    private List<WebElement> listOfMessages;
    @FindAll(@FindBy(xpath = "//div[contains(@class, 'chat-message__text')]"))
    private List<WebElement> listOfTextsInMessageWithAttachment;
    @FindBy(xpath = "(//div[contains(@class, 'v-tabs-bar__content')]/div)[3]")
    private WebElement subscribersBtn;
    @FindBy(xpath = "(//div[contains(@class, 'v-tabs-bar__content')]/div)[4]")
    private WebElement followersBtn;
    @FindBy(css = ".inbox_sidebar .inbox_group-message button")
    private WebElement sendMassMessageBtn;
    @FindBy(xpath = "//div[contains(@class, 'massmessage__header')]")
    private WebElement massMessageModalHeader;
    @FindBy(xpath = "(//div[contains(@class, 'massmessage__main')]//button)[3]")
    private WebElement followersFromMassMessageModalBtn;
    @FindBy(xpath = "(//div[contains(@class, 'massmessage__main')]//button)[2]")
    private WebElement subscribersFromMassMessageModalBtn;
    @FindBy(xpath = "(//div[contains(@class, 'massmessage__main')]//button)[1]")
    private WebElement allFromMassMessageModalBtn;
    @FindBy(css = ".massmessage__container .ql-editor")
    private WebElement messageInput;
    @FindBy(css = ".massmessage__container .justify-center button")
    private WebElement sendMessageBtn;
    @FindBy(css = ".massmessage__main .ql-emoji")
    private WebElement emojiBtn;
    @FindBy(css = ".massmessage__main .bem-sweat_smile")
    private WebElement emojiValue;
    @FindAll(@FindBy(xpath = "(//div[contains(@class, 'inbox_chats__chats-tab')])[3]//div[@class='inbox_chat__item']"))
    private List<WebElement> followersTabItems;
    @FindBy(xpath = "(//div[contains(@class, 'inbox_chats__chats-tab')])[3]//div[contains(@class, 'inbox_chat__item')]")
    private WebElement subscribersTabItems;
    @FindAll(@FindBy(xpath = "(//div[contains(@class, 'inbox_chats__chats-tab')])[2]//div[@class='inbox_chat__item']"))
    private List<WebElement> recentTabItems;
    @FindBy(css = ".ql-toolbar .v-form .v-btn")
    private WebElement attachFiles;
    @FindBy(css = ".attachment-price input")
    private WebElement attachmentPriceInput;
    @FindBy(css = ".chat-message-file img")
    private WebElement imageFromTheChat;
    @FindBy(css = ".chat-message-file-container .chat-message__price")
    private WebElement priceOfTheSentImage;
    @FindBy(css = ".video-select__main .upload-button")
    private WebElement uploadButton;
    public WebElement getLastMessageFromInbox(){
        int lastIndex = listOfMessages.size() - 1;
        return listOfMessages.get(lastIndex);
    }
    public void clickUploadBtn(){
        Wait.untilElementIsVisible(webDriver, uploadButton, 20L);
        uploadButton.click();
    }
    public WebElement getLastTextInMessageWithAttachment(){
        int lastIndex = listOfTextsInMessageWithAttachment.size() -1;
        return listOfTextsInMessageWithAttachment.get(lastIndex);
    }
    public void clickSubscriberChatItem(){
        Wait.untilElementIsVisible(webDriver, subscribersTabItems, 30L);
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        js.executeScript("arguments[0].click();", subscribersTabItems);
    }
    public void clickSendMassMessageBtn(){
        Wait.untilElementIsVisible(webDriver, sendMassMessageBtn, 30L);
        sendMassMessageBtn.click();
    }
    public void clickEmojiValue(){
        Wait.untilElementIsVisible(webDriver, emojiValue, 30L);
        emojiValue.click();
    }
    public void clickSendMessageBtn(){
        Wait.untilElementIsClickable(webDriver, sendMessageBtn, 20L);
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        js.executeScript("arguments[0].click();", sendMessageBtn);
    }
    public void clickFollowersChatItem(){
        Wait.untilElementIsVisible(webDriver, followersTabItems.get(1), 30L);
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        js.executeScript("arguments[0].click();", followersTabItems.get(1));
    }
    public void clickRecentChatItem(){
        Wait.untilElementIsVisible(webDriver, recentTabItems.get(1), 30L);
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        js.executeScript("arguments[0].click();", recentTabItems.get(1));
    }
    public void clickSubscribersBtn(){
        Wait.untilElementIsVisible(webDriver, subscribersBtn, 30L);
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        js.executeScript("arguments[0].click();", subscribersBtn);
    }
    public void clickFollowersBtn(){
        Wait.untilElementIsVisible(webDriver, followersBtn, 30L);
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        js.executeScript("arguments[0].click();", followersBtn);
    }
    public void clickFirstChatItem(){
        Wait.untilElementIsVisible(webDriver, followersTabItems.get(0), 30L);
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        js.executeScript("arguments[0].click();", followersTabItems.get(0));
    }

    public void clickAttachFilesBtn() {
        Wait.untilElementIsVisible(webDriver, attachFiles, 20L);
        attachFiles.click();
    }
}
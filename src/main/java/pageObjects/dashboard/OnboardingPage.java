package pageObjects.dashboard;

import lombok.Getter;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utilities.Utilities;
import utilities.Wait;

import java.util.List;
@Getter
public class OnboardingPage {
    private final WebDriver webDriver;

    public OnboardingPage(WebDriver webDriver) {
        this.webDriver = webDriver;
        PageFactory.initElements(webDriver, this);
    }
    @FindBy(xpath = "(//div[@class='v-text-field__slot']/input)[1]")
    private WebElement tfFirstName;
    @FindBy(xpath = "(//div[@class='v-text-field__slot']/input)[2]")
    private WebElement tfLastName;
    @FindBy(xpath = "//input[@id='dateInput']")
    private WebElement inputDateOfBirth;
    @FindBy(xpath = "(//div[@class='v-select__selections'])[1]")
    private WebElement genderPicker;
    @FindBy(className = "v-list-item__title")
    private WebElement gender;
    @FindBy(css = ".mt-6 .v-select__selections input")
    private List<WebElement> citizenshipAndCountryInput;
    @FindBy(xpath = "//button[@type='submit']")
    private WebElement continueToProfileBtn;
    @FindBy(css = ".mt-12 .text-white")
    private WebElement saveNameBtn;
    @FindBy(css = ".onboarding-steps_content .v-text-field__slot textarea")
    private WebElement bioTextArea;
    @FindBy(css = ".categories-autocomplete .v-select__slot input")
    private WebElement addTagInput;
    @FindBy(css = ".bg-pink")
    private WebElement continueBtnAfterAddBioAndTags;
    @FindBy(css = ".overview-profile__banner-block__upload_items input")
    private WebElement uploadProfilePicture;
    @FindBy(css = ".mt-12 .rounded-xl:nth-child(2)")
    private WebElement completeUploadProfilePictureBtn;
    @FindBy(className = "vue-preview__wrapper")
    private WebElement configureSizeOfPicture;
    @FindBy(css = ".gap-4 .v-btn--contained")
    private WebElement doneCoverBtn;
    @FindBy(css = ".mt-12 .text-white")
    private WebElement continueBtnFromAddPicturePage;
    @FindBy(xpath = "//div[contains(@class, 'toasted-primary')]")
    private WebElement warningBlacklistMessage;
    @FindBy(css = ".gap-4 .v-btn--contained")
    private WebElement doneUploadPictureBtn;
    @FindBy(xpath = "//input[contains(@placeholder, 'Tag')]")
    private WebElement inputTag;
    public void completeOnboardingPage(String firstName, String lastName, String country, String citizenship) {
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        tfFirstName.sendKeys(firstName);
        tfLastName.sendKeys(lastName);
        completeDateInfo();
        completeGenderInfo();
        completeCountryInfo(country);
        completeCitizenInfo(citizenship);
        js.executeScript("arguments[0].click();", continueToProfileBtn);
        js.executeScript("arguments[0].click();", saveNameBtn);
        completeBioInfo();
        clickContinueBtnAddBioPage();
        uploadProfilePicture();
    }

    public void completeOnboardingPageUntilTags(String firstName, String lastName, String country, String citizenship) {
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        tfFirstName.sendKeys(firstName);
        tfLastName.sendKeys(lastName);
        completeDateInfo();
        completeGenderInfo();
        completeCountryInfo(country);
        completeCitizenInfo(citizenship);
        js.executeScript("arguments[0].click();", continueToProfileBtn);
        js.executeScript("arguments[0].click();", saveNameBtn);
        completeBioInfo();
        //clickContinueBtnAddBioPage();
        //uploadProfilePicture();
    }

    private void completeOnboardingPageTillBioInfo(String firstName, String lastName,String country, String citizenship) {
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        tfFirstName.sendKeys(firstName);
        tfLastName.sendKeys(lastName);
        completeDateInfo();
        completeGenderInfo();
        completeCountryInfo(country);
        completeCitizenInfo(citizenship);
        js.executeScript("arguments[0].click();", continueToProfileBtn);
        js.executeScript("arguments[0].click();", saveNameBtn);
    }
    private void completeDateInfo() {
        inputDateOfBirth.sendKeys("10/10/1999");
    }
    private void completeGenderInfo() {
        genderPicker.click();
        new WebDriverWait(webDriver, 10L)
                .until(ExpectedConditions.elementToBeClickable(gender));
        gender.click();
    }
    private void completeCountryInfo(String country) {
        WebElement countryInput = citizenshipAndCountryInput.get(1);
        countryInput.sendKeys(country);
    }
    private void completeCitizenInfo(String citizenship) {
        WebElement citizenshipInput = citizenshipAndCountryInput.get(0);
        citizenshipInput.sendKeys(citizenship);
    }
    private void completeBioInfo() {
        String sampleText = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. " +
                "Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, " +
                "when an unknown printer took a galley";
        bioTextArea.sendKeys(sampleText);
        addTagInput.sendKeys("amateur" + Keys.RETURN);
    }
    public void clickContinueBtnAddBioPage() {
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        js.executeScript("arguments[0].click();", continueBtnAfterAddBioAndTags);
    }
    public void completeBioInfo(List<String> blacklistedWords, String firstName, String lastName,
                                String country, String citizenship) {
        completeOnboardingPageTillBioInfo(firstName, lastName, country, citizenship);
        String sampleText = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. " +
                "Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, " +
                "when an unknown printer took a galley";
        bioTextArea.sendKeys(sampleText);
        for (String blacklistedWord : blacklistedWords) {
            addTagInput.sendKeys(blacklistedWord);
            new WebDriverWait(webDriver, 10L)
                    .until(ExpectedConditions.elementToBeClickable(addTagInput));
            addTagInput.sendKeys(Keys.RETURN);
        }
    }
    public void uploadProfilePicture() {
        String picturePath = System.getProperty("user.dir") + "\\src\\main\\resources\\pictureForProfileAndCover\\baner-ads-examples-tf.jpg";
        uploadProfilePicture.sendKeys(picturePath);
        Wait.untilElementIsVisible(webDriver, configureSizeOfPicture, 5L);
        doneUploadPictureBtn.click();
        Wait.untilElementIsClickable(webDriver, completeUploadProfilePictureBtn, 10L);
        Utilities.sleep(3000);
        clickCompleteUploadProfilePictureBtn();
    }

    public void clickCompleteUploadProfilePictureBtn(){
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        js.executeScript("arguments[0].click();", completeUploadProfilePictureBtn);
    }
    private void uploadCoverPicture() {
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        String picturePath = System.getProperty("user.dir") + "\\src\\main\\resources\\pictureForProfileAndCover\\baner-ads-examples-tf.jpg";
        uploadProfilePicture.sendKeys(picturePath);
        new WebDriverWait(webDriver, 10L)
                .until(ExpectedConditions.elementToBeClickable(doneCoverBtn));
        Wait.untilElementIsVisible(webDriver, configureSizeOfPicture, 5L);
        js.executeScript("arguments[0].scrollIntoView(true);", doneCoverBtn);
        doneCoverBtn.click();
        js.executeScript("arguments[0].scrollIntoView(true);", continueBtnFromAddPicturePage);
        js.executeScript("arguments[0].click();", continueBtnFromAddPicturePage);
    }
}
package pageObjects.dashboard;

import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.Random;

@Getter
public class ForgotPassword {
    private WebDriver webDriver;

    public ForgotPassword(WebDriver webDriver){
        this.webDriver = webDriver;
        PageFactory.initElements(webDriver, this);
    }
    @FindBy(css = ".v-text-field__slot input")
    private WebElement emailField;
    @FindBy(css = ".auth_action .app_button")
    private WebElement resetPasswordButton;
    @FindBy(id = "input-15")
    private WebElement newPasswordField;
    @FindBy(id = "input-19")
    private WebElement confirmPasswordField;

    public String generateNewPassword(){
        Random random = new Random();
        return "pass" + random.nextInt(1000000000);
    }
}
package pageObjects.dashboard;

import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.Wait;

@Getter
public class WithdrawModal {
    private final WebDriver webDriver;
    public WithdrawModal(WebDriver webDriver){
        this.webDriver = webDriver;
        PageFactory.initElements(webDriver, this);
    }
    @FindBy(css = ".v-dialog--active")
    private WebElement withdrawModal;
    @FindBy(css = ".withdraw_price--input .v-text-field__slot input")
    private WebElement withdrawInputField;
    @FindBy(css = ".withdraw_wrapper .withdraw_title")
    private WebElement withdrawModalTitle;
    @FindBy(css = ".withdraw_info .v-input--selection-controls__ripple")
    private WebElement payoutMethodSelector;
    @FindBy(xpath = "(//div[contains(@class, 'withdraw_wrapper')]//button)[2]")
    private WebElement requestWithdrawalButton;
    @FindBy(xpath = "//div[contains(@class, 'withdraw_text')]")
    private WebElement withdrawRequestPopUpText;
    @FindBy(xpath = "//div[@class='mt-8']//button[contains(@class, 'size16-weight300')]")
    private WebElement proceedButton;
    @FindBy(css = ".mt-8 .withdraw_text")
    private WebElement requestCompletedPopUpText;
    @FindBy(xpath = "(//div[@class='withdraw_close']/button)[2]")
    private WebElement closeRequestCompletedPopUp;
    public void clickRequestWithdrawal(){
        Wait.untilElementIsClickable(webDriver, requestWithdrawalButton, 30L);
        requestWithdrawalButton.click();
    }
}
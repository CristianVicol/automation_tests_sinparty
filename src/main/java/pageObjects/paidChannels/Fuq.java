package pageObjects.paidChannels;

import lombok.Getter;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.Wait;

@Getter
public class Fuq {
    WebDriver webDriver;
    public Fuq(WebDriver webDriver){
        this.webDriver = webDriver;
        PageFactory.initElements(webDriver, this);
    }
    @FindBy(id = "search_query_query")
    private WebElement searchBar;
    @FindBy(css = ".modal-footer button")
    private WebElement confirmAgeBtn;
    @FindBy(xpath = "(//div[@class='cards-container']//div[contains(@class, 'card')])[3]")
    private WebElement video;

    public void searchForSinParty(){
        searchBar.sendKeys("sinparty");
        searchBar.sendKeys(Keys.ENTER);
    }
    public void clickVideo(){
        Wait.untilElementIsVisible(webDriver, video, 10L);
        video.click();
    }
}
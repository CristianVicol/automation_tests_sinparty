package pageObjects.webPages;

import lombok.Getter;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.Wait;

@Getter
public class AffiliateVideo {
    private WebDriver webDriver;
    public AffiliateVideo(WebDriver webDriver){
        this.webDriver = webDriver;
        PageFactory.initElements(webDriver, this);
    }
    @FindBy(css = ".asg-vjs-overlay")
    private WebElement video;
    @FindBy(css = ".vjs-big-play-button")
    private WebElement playVideoButton;
    @FindBy(css = ".vjs-icon-placeholder")
    private WebElement playIconPlaceHolder;

    public void clickVideo(){
        Wait.untilElementIsVisible(webDriver, video, 20L);
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        js.executeScript("arguments[0].click();", video);
    }
}
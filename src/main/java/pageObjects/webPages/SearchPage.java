package pageObjects.webPages;

import lombok.Getter;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.Wait;

import java.util.List;
import java.util.concurrent.TimeUnit;

@Getter
public class SearchPage {
    WebDriver webDriver;

    public SearchPage(WebDriver webDriver){
        this.webDriver = webDriver;
        PageFactory.initElements(webDriver, this);
    }
    @FindBy(css = ".searchbox__input")
    private WebElement searchInput;
    @FindBy(css = ".searchbox__form--modal svg")
    private WebElement searchBtn;
    @FindBy(xpath = "(//button[contains(@class, 'searchbox__filter')])[1]")
    private WebElement allBtn;
    @FindBy(xpath = "(//button[contains(@class, 'searchbox__filter')])[2]")
    private WebElement creatorsBtn;
    @FindBy(xpath = "(//button[contains(@class, 'searchbox__filter')])[3]")
    private WebElement liveCamsBtn;
    @FindBy(xpath = "(//button[contains(@class, 'searchbox__filter')])[4]")
    private WebElement pornVideosBtn;
    @FindBy(xpath = "(//button[contains(@class, 'searchbox__filter')])[5]")
    private WebElement channelsBtn;
    @FindBy(css = ".searchbox__warning-title")
    private WebElement warningText;
    @FindBy(css = ".searchbox .popup__close")
    private WebElement closeBtn;
    @FindBy(xpath = "/html/body/div[1]/header/div/div[2]/div/div/div/div[3]/div")
    private WebElement noVideoFoundText;
    @FindBy(css = ".searchbox__section .scroller__item .creator-card__title")
    private WebElement creatorName;
    @FindBy(css = ".searchbox__section .scroller__item .cam-tile__title")
    private WebElement liveCamCreatorName;
    @FindBy(xpath = "(//a[contains(@class, 'cam-tile')])[1]")
    private WebElement liveCamValue;
    @FindBy(xpath = "(//div[contains(@class, 'searchbox__result-item--video')]/div[@class='video-card__footer'])[2]")
    private WebElement pornVideosValue;
    @FindBy(xpath = "(//div[contains(@class, 'searchbox__results')]//a[contains(@class, 'creator-category')])[3]")
    private WebElement channelVideoValue;

    private boolean isUserWarnedAboutBlacklistedWord(String blackListedWord){
        try{
            searchInput.sendKeys(blackListedWord);
            Wait.untilElementIsVisible(webDriver, warningText,6L);
            return warningText.isDisplayed();
        }catch (TimeoutException e){
            return false;
        }
    }
    public boolean checkTheListOfBlackListedWords(List<String> listOfWords){
        MainWeb webMain = new MainWeb(webDriver);
        boolean checkWordsAreBlacklisted = true;
        for(String word : listOfWords){
            boolean val = isUserWarnedAboutBlacklistedWord(word);
            if(!val){
                checkWordsAreBlacklisted = false;
                System.out.println("Check this word!!!! =====>" + word);
            }
            closeBtn.click();
            Wait.untilElementIsVisible(webDriver, webMain.getSearchBtn(), 5L);
            webMain.getSearchBtn().click();
            Wait.untilElementIsVisible(webDriver, searchInput, 5L);
        }
        return checkWordsAreBlacklisted;
    }
    public boolean checkUserCanNotSearchVideoByShadowBanWord(List<String> videoNames){
        MainWeb webMain = new MainWeb(webDriver);
        boolean b = true;
        for (String videoName : videoNames) {
            searchInput.sendKeys(videoName);
            Wait.untilElementIsVisible(webDriver, noVideoFoundText, 5L);
            if(!noVideoFoundText.getText().equals("Nothing found")){
                b = false;
                System.out.println("CHECK THIS TAG =====> " + videoName);
            }
            closeBtn.click();
            Wait.untilElementIsVisible(webDriver, webMain.getSearchBtn(), 5L);
            webMain.getSearchBtn().click();
            Wait.untilElementIsVisible(webDriver, searchInput, 5L);
        }
        return b;
    }
    public void clickPornVideoValue(){
        try {
            Wait.untilElementIsClickable(webDriver,
                    pornVideosValue, 40L);
            pornVideosValue.click();
        }catch (StaleElementReferenceException e){
            WebElement pornVideo = webDriver
                    .findElement(By.xpath("(//div[contains(@class, 'searchbox__result-item--video')]/div[@class='video-card__footer'])[2]"));
            pornVideo.click();
        }
    }
    public String getVideoNameText(){
        try{
            Wait.untilElementIsVisible(webDriver, pornVideosValue, 20L);
            return pornVideosValue.getText();
        }catch (StaleElementReferenceException e){
            WebElement pornVideo = webDriver
                    .findElement(By.xpath("(//div[contains(@class, 'searchbox__result-item--video')]/div[@class='video-card__footer'])[2]"));
            Wait.untilElementIsVisible(webDriver, pornVideo, 20L);
            return pornVideo.getText();
        }
    }
    public void clickChannelVideoValue(){
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        try{
            Wait.untilElementIsClickable(webDriver, channelVideoValue, 20L);
            js.executeScript("arguments[0].click();", channelVideoValue);
        }catch (StaleElementReferenceException e){
            WebElement channelValue = webDriver
                    .findElement(By.xpath("(//div[contains(@class, 'searchbox__results')]//a[contains(@class, 'creator-category')])[3]"));
            js.executeScript("arguments[0].click();", channelValue);
        }
    }
}
package pageObjects.webPages;

import lombok.Getter;
import managers.FileReaderManager;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utilities.Wait;

import java.util.List;
import java.util.concurrent.TimeUnit;

@Getter
public class MainWeb {
    private WebDriver webDriver;

    public MainWeb(WebDriver webDriver) {
        this.webDriver = webDriver;
        PageFactory.initElements(webDriver, this);
    }

    @FindBy(id = "member_login_button")
    private WebElement signInBtn;
    @FindBy(css = ".confirm-orientation-modal__close")
    private WebElement genderNotificationCrossBtn;
    @FindBy(css = ".hideonmobile .orientation__selected")
    private WebElement chooseGender;
    @FindBy(css = ".hideonmobile .orientation__link")
    private List<WebElement> orientationOptions;
    @FindBy(css = ".header__main .login-user .user-name__title")
    private WebElement userNameTitle;
    @FindBy(css = ".header__search-toggle")
    private WebElement searchBtn;
    @FindBy(xpath = "(//h2[contains(@class, 'hero-slide__title')])[1]")
    private WebElement featuredCreatorLink;
    @FindBy(css = ".navigation__item:nth-child(1)")
    private WebElement creatorsButton;
    @FindBy(xpath = "(//header[@class='header']//a[contains(@class, 'navigation__link')])[2]")
    private WebElement sinPartyLiveButton;
    @FindBy(xpath = "(//header[@class='header']//a[contains(@class, 'navigation__link')])[3]")
    private WebElement videosButton;
    @FindBy(xpath = "(//header[@class='header']//a[contains(@class, 'navigation__link')])[4]")
    private WebElement categoriesButton;
    @FindBy(css = ".header__main .header__logo")
    private WebElement sinPartyLogo;
    @FindBy(xpath = "((//ul[@class='acc-nav'])[1]//li)[2]")
    private WebElement myAccountBtn;
    @FindBy(xpath = "((//ul[@class='acc-nav'])[1]//li)[3]")
    private WebElement myVideosBtn;
    @FindBy(css = ".user-menu--desktop")
    private WebElement userMenu;
    @FindAll(@FindBy(xpath = "//li/a"))
    private List<WebElement> listOfLinks;
    @FindAll(@FindBy(xpath = "//section[@class='main__section ']//span[@class='video-card__video']"))
    private List<WebElement> videoValues;
    @FindAll(@FindBy(xpath = "//section[@class='main__section ']//a[@class='video-card__title']"))
    private List<WebElement> videoValueTitle;
    @FindAll(@FindBy(xpath = "//section[@class='main__section ']//button[contains(@class, 'btn--favorite')]"))
    private List<WebElement> addVideoToFavoritesIcon;
    @FindBy(css = ".form__submit")
    private WebElement addToFavoritesPopUpBtn;
    @FindBy(css = ".btn--acid-bordered")
    private WebElement playlist;
    @FindBy(xpath = "(//div[@class='popup-notification__content'])[1]")
    private WebElement videoAddedToFavoritesNotification;
    @FindBy(css = ".app-modal__ModalAccountRequired:nth-child(1)")
    private WebElement accountRequiredModal;
    @FindBy(css = ".language-selector__item:nth-child(1)")
    private WebElement deutschLanguage;
    @FindBy(css = ".language-selector__item:nth-child(3)")
    private WebElement spanishLanguage;
    @FindBy(css = ".language-selector__item:nth-child(4)")
    private WebElement frenchLanguage;
    @FindBy(xpath = "(//button[@class='scroller__next'])[9]")
    private WebElement scrollNextBtn;

    public boolean isDisplayed() {
        Wait.untilElementIsVisible(webDriver, signInBtn, 30L);
        return signInBtn.isDisplayed() && searchBtn.isDisplayed();
    }

    public void clickAddToFavoritesIcon() {
        Actions actions = new Actions(webDriver);
        WebElement videoValue = videoValues.get(1);
        WebElement addToFavoritesIcon = addVideoToFavoritesIcon.get(1);
        Wait.untilElementIsVisible(webDriver, videoValue, 30L);
        actions.moveToElement(videoValue)
                .build()
                .perform();
         Wait.untilElementIsClickable(webDriver, addToFavoritesIcon, 30L);
         ((JavascriptExecutor) webDriver).executeScript("arguments[0].click();", addToFavoritesIcon);
    }
    public void clickSignInBtn() {
        ((JavascriptExecutor) webDriver).executeScript("arguments[0].click();", signInBtn);
    }

    public void moveToChooseGender() {
        Actions actions = new Actions(webDriver);
        actions.moveToElement(chooseGender).perform();
    }

    public void clickGayOrientationBtn() {
        WebElement gayOrientation = orientationOptions.get(1);
        Wait.untilElementIsVisible(webDriver, gayOrientation, 30L);
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        js.executeScript("arguments[0].click();", gayOrientation);
    }

    public void clickOnTransOrientationBtn() {
        WebElement transOrientation = orientationOptions.get(2);
        Wait.untilElementIsVisible(webDriver, transOrientation, 30L);
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        js.executeScript("arguments[0].click();", transOrientation);
    }

    public void clickStraightOrientationBtn() {
        WebElement straightOrientation = orientationOptions.get(0);
        Wait.untilElementIsVisible(webDriver, straightOrientation, 30L);
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        js.executeScript("arguments[0].click();", straightOrientation);
    }

    public void clickSearchBtn() {
        new WebDriverWait(webDriver, 30L)
                .until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".header__search-toggle")));
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        js.executeScript("arguments[0].click();", searchBtn);
    }

    public void clickCreatorsButton() {
        Wait.untilElementIsVisible(webDriver, creatorsButton, 30L);
        WebElement creatorsButtonLink = webDriver.findElement(By.cssSelector(".navigation__item:nth-child(1) .navigation__link:nth-child(1)"));
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        js.executeScript("arguments[0].click();", creatorsButtonLink);
    }

    public void clickSinPartyLiveButton() {
        Wait.untilElementIsClickable(webDriver, sinPartyLiveButton, 30L);
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        js.executeScript("arguments[0].click();", sinPartyLiveButton);
    }

    public void clickCategoriesButton() {
        Wait.untilElementIsClickable(webDriver, categoriesButton, 30L);
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        js.executeScript("arguments[0].click();", categoriesButton);
    }

    public void clickVideosButton() {
        Wait.untilElementIsClickable(webDriver, videosButton, 30L);
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        js.executeScript("arguments[0].click();", videosButton);
    }

    public void clickSinPartyLogo() {
        Wait.untilElementIsVisible(webDriver, sinPartyLogo, 65L);
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        js.executeScript("arguments[0].click();", sinPartyLogo);
    }

    public void clickMyVideoBtn() {
        try {
            webDriver.manage().timeouts().pageLoadTimeout(3L, TimeUnit.SECONDS);
            new Actions(webDriver).moveToElement(userMenu)
                    .click(myVideosBtn)
                    .build()
                    .perform();
        } catch (TimeoutException e) {

        } finally {
            webDriver.manage().timeouts().pageLoadTimeout(FileReaderManager.getInstance()
                    .getConfigFileReader().getTime(), TimeUnit.SECONDS);
        }
    }
}
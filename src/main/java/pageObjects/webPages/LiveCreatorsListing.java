package pageObjects.webPages;

import lombok.Getter;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.Wait;

import java.util.List;

@Getter
public class LiveCreatorsListing {
    private WebDriver webDriver;

    public LiveCreatorsListing(WebDriver webDriver){
        this.webDriver = webDriver;
        PageFactory.initElements(webDriver, this);
    }
    @FindBy(xpath = "(//span[@class='cam-tile__details'])[3]")
    private WebElement liveCreator;
    @FindBy(xpath = "//a[contains(@title, 'Next')]")
    private WebElement nextPageBtn;
    @FindBy(xpath = "//a[@title='Guys']")
    private WebElement guysBtn;
    @FindBy(xpath = "//a[@title='Girls']")
    private WebElement girlsBtn;
    @FindBy(xpath = "(//a[contains(@href, 'new_models')])[1]")
    private WebElement newModelsBtn;
    @FindAll(@FindBy(css = ".cam-tile__details"))
    private List<WebElement> listOfLiveModels;
    @FindBy(css = ".content-count")
    private WebElement contentCount;
    @FindBy(xpath = "//li//a[contains(@href, 'goldshow')]")
    private WebElement goldShowBtn;
    @FindAll(@FindBy(css = ".listing-filter .trending-links a"))
    private List<WebElement> listOfTags;
    @FindBy(xpath = "(//a[@class='show-more-button'])[1]")
    private WebElement showMoreOrLessTagsBtn;
    @FindBy(css = ".open .show-more-button")
    private WebElement showMoreOrShowLessCategoriesBtn;
    @FindBy(css = ".app-modal__ModalRegisterBonus .popup__close")
    private WebElement closePopUpBtn;
    @FindBy(css = ".listing-filter .live-cam-filter-categories")
    private WebElement categoryFilterBtn;
    @FindAll(@FindBy(css = ".live-cam-filter-categories .checkmark"))
    private List<WebElement> listOfCategoriesCheckBox;
    @FindAll(@FindBy(css = ".live-cam-filter-categories .col-12"))
    private List<WebElement> listOfCategories;
    @FindBy(xpath ="//a[@href='javascript:;' and contains(text(), 'Region')]")
    private WebElement regionFilterBtn;
    @FindAll(@FindBy(xpath = "//ul[preceding-sibling::a[@href='javascript:;' and contains(text(), 'Region')]]//li"))
    private List<WebElement> listOfRegions;
    @FindBy(xpath = "//a[@href='javascript:;' and contains(text(), 'Age')]")
    private WebElement ageFilterBtn;
    @FindAll(@FindBy(xpath = "//ul[preceding-sibling::a[@href='javascript:;' and contains(text(), 'Age')]]//li"))
    private List<WebElement> listOfAges;
    @FindBy(xpath = "//a[@href='javascript:;' and contains(text(), 'Language')]")
    private WebElement languageFilterBtn;
    @FindAll(@FindBy(xpath = "//ul[preceding-sibling::a[@href='javascript:;' and contains(text(), 'Language')]]//li"))
    private List<WebElement> listOfLanguageElements;
    @FindBy(xpath = "//a[@href='javascript:;' and contains(text(), 'Models')]")
    private WebElement newModelsFilterBtn;
    @FindAll(@FindBy(xpath = "//ul[preceding-sibling::a[@href='javascript:;' and contains(text(), 'Models')]]//li"))
    private List<WebElement> listOfNewModelsFilterOptions;
    @FindBy(xpath = "//a[@href='javascript:;' and contains(text(), 'Gender')]")
    private WebElement genderFilterBtn;
    @FindAll(@FindBy(xpath = "//ul[preceding-sibling::a[@href='javascript:;' and contains(text(), 'Gender')]]//li"))
    private List<WebElement> listOfGenderFilterOptions;

    public void clickGenderFilterBtn(){
        Wait.untilElementIsVisible(webDriver, genderFilterBtn, 20L);
        new Actions(webDriver).moveToElement(genderFilterBtn)
                .click(genderFilterBtn)
                .build()
                .perform();
    }
    public void clickListOfGenderFilterOptions(int index){
        WebElement element = listOfGenderFilterOptions.get(index);
        Wait.untilElementIsVisible(webDriver, element, 20L);
        new Actions(webDriver).moveToElement(element)
                .click(element)
                .build()
                .perform();
    }
    public void clickNewModelsFilterBtn(){
        new Actions(webDriver).moveToElement(newModelsFilterBtn)
                .click(newModelsFilterBtn)
                .build()
                .perform();
    }

    public void clickAgeFilterBtn(){
        new Actions(webDriver).moveToElement(ageFilterBtn)
                .click(ageFilterBtn)
                .build()
                .perform();
    }

    public void clickLanguageFilterBtn(){
        Wait.untilElementIsVisible(webDriver, languageFilterBtn, 20L);
        new Actions(webDriver).moveToElement(languageFilterBtn)
                .click(languageFilterBtn)
                .build()
                .perform();
    }

    public void clickListOfLanguageElement(int index){
        WebElement element = listOfLanguageElements.get(index);
        Wait.untilElementIsVisible(webDriver, element, 20L);
        new Actions(webDriver).moveToElement(element)
                .click(element)
                .build()
                .perform();
    }

    public void clickListOfAgesElement(int index){
        WebElement element = listOfAges.get(index);
        Wait.untilElementIsVisible(webDriver, element, 20L);
        new Actions(webDriver).moveToElement(element)
                .click(element)
                .build()
                .perform();
    }

    public void clickShowMoreOrLessTagsBtn(){
        new Actions(webDriver).moveToElement(showMoreOrLessTagsBtn)
                .click(showMoreOrLessTagsBtn)
                .build()
                .perform();
    }

    public void clickListOfRegionsElement(int index){
        WebElement element = listOfRegions.get(index);
        Wait.untilElementIsVisible(webDriver, element, 20L);
        new Actions(webDriver).moveToElement(element)
                .click(element)
                .build()
                .perform();
    }

    public void clickRegionFilterBtn(){
        new Actions(webDriver).moveToElement(regionFilterBtn)
                .click(regionFilterBtn)
                .build()
                .perform();
    }

    public void clickShowMoreOrLessCategoriesBtn(){
        new Actions(webDriver).moveToElement(showMoreOrShowLessCategoriesBtn)
                .click(showMoreOrShowLessCategoriesBtn)
                .build()
                .perform();
    }

    public void clickCategoryFilterBtn(){
        new Actions(webDriver).moveToElement(categoryFilterBtn)
                .click(categoryFilterBtn)
                .build()
                .perform();
    }

    public void clickCheckBoxListOfCategoriesElement(int index){
        WebElement webElement = listOfCategoriesCheckBox.get(index);
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        js.executeScript("arguments[0].click();", webElement);
    }

    public void clickListOfLiveModelsElement(int index){
        WebElement webElement = listOfLiveModels.get(index);
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        js.executeScript("arguments[0].click();", webElement);
    }
    public void clickListOfTagsElement(int index){
        WebElement webElement = listOfTags.get(index);
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        js.executeScript("arguments[0].click();", webElement);
    }
    public void clickGoldShowBtn(){
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        js.executeScript("arguments[0].click();", goldShowBtn);
    }

    public void clickNewModelsBtn(){
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        js.executeScript("arguments[0].click();", newModelsBtn);
    }

    public void clickLiveCreator(){
        Wait.untilElementIsClickable(webDriver, liveCreator, 20L);
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        js.executeScript("arguments[0].click();", liveCreator);
    }
    public void clickGirlsBtn(){
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        js.executeScript("arguments[0].click();", girlsBtn);
    }
    public void clickGuysBtn(){
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        js.executeScript("arguments[0].click();", guysBtn);
    }

    public void clickNextPageBtn() {
        Wait.untilElementIsVisible(webDriver, nextPageBtn, 20L);
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        js.executeScript("arguments[0].click();", nextPageBtn);
    }
}

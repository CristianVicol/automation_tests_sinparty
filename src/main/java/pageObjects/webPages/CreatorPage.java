package pageObjects.webPages;

import lombok.Getter;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.Utilities;
import utilities.Wait;

@Getter
public class CreatorPage {
    private WebDriver webDriver;
    public CreatorPage(WebDriver webDriver){
        this.webDriver = webDriver;
        PageFactory.initElements(webDriver, this);
    }
    @FindBy(xpath = "(//button[contains(@class, 'model-page-panel__cta')])[1]")
    private WebElement followBtn;
    @FindBy(css = ".model-page-cta--subscribe")
    private WebElement subscribeBtn;
    @FindBy(xpath = "((//div[@class='video-gallery__item']//div[@class='video-card video-card--no-owner'])[1]//a)[1]")
    private WebElement creatorClipValue;
    @FindBy(xpath = "(//a[@class='model-page-menu__link']//span[@class='hideonmobile'])[1]")
    private WebElement premiumPostsButton;
    @FindBy(xpath = "(//button[contains(@class, 'my-cover__btn')])[1]")
    private WebElement subscribeNowBtn;
    @FindBy(xpath = "(//button[@class='modal-subscription__btn'])[4]")
    private WebElement subscribeToCreatorButtonFromPopUp;
    @FindBy(css = ".app-modal__ModalPayWithBalance")
    private WebElement purchaseSuccessfulModal;
    @FindBy(xpath = "//button[contains(@class, 'balance-pay__btn')]")
    private WebElement continueButtonFromModal;
    @FindBy(css = ".subscription-modal--successfull")
    private WebElement congratulationsPopUp;
    @FindBy(css = ".message-modal__messenger .ql-container .ql-editor")
    private WebElement congratulationsPopUpTextArea;
    @FindBy(css = ".messenger__actions .ql-emoji")
    private WebElement addEmojiBtn;
    @FindBy(css = ".creator-hero__poster")
    private WebElement modelPoster;
    @FindBy(css = ".message-modal__btn")
    private WebElement sendMessageButton;
    @FindBy(css = ".app-modal__ModalSendModelMessage .popup__close")
    private WebElement congratulationsPopUpCloseBtn;
    @FindBy(css = ".subscription-modal--message-successfull")
    private WebElement messageSentSuccessfulModal;
    @FindBy(css = ".app-modal__ModalInfo .popup__close")
    private WebElement closeMessageSentSuccessfulModal;
    @FindBy(xpath = "//ul[@class='creator-hero__menu']//span[contains(text(), 'Free Videos')]")
    private WebElement freeVideoBtn;

    public void clickCreatorClipValue(){
        //Wait.untilElementIsVisible(webDriver, creatorClipValue, 20L);
        Utilities.scrollWindow(webDriver, 0, 700);
        Utilities.sleep(4000);
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        js.executeScript("arguments[0].click();", creatorClipValue);
    }

    public void clickFreeVideoBtn() {
        Wait.untilElementIsVisible(webDriver, freeVideoBtn, 20L);
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        js.executeScript("arguments[0].click();", freeVideoBtn);
    }
}
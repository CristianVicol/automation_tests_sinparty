package pageObjects.webPages;

import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
@Getter
public class MyVideos {
    private WebDriver webDriver;
    public MyVideos(WebDriver webDriver){
        this.webDriver = webDriver;
        PageFactory.initElements(webDriver, this);
    }
    @FindBy(xpath = "(//span[@class='video-card__video'])[1]")
    private WebElement favoriteVideoValue;
    @FindBy(xpath = "(//a[@class='video-card__title'])[1]")
    private WebElement videoTitle;
}
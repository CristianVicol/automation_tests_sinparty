package pageObjects.webPages;

import lombok.Getter;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
@Getter
public class ChannelPage {
    private WebDriver webDriver;

    public ChannelPage(WebDriver webDriver){
        this.webDriver = webDriver;
        PageFactory.initElements(webDriver, this);
    }
    @FindBy(css = ".channel-page__logo")
    private WebElement channelPageLogo;
    @FindBy(xpath = "(//span[@class='video-card__video'])[2]")
    private WebElement videoValue;

    public void clickVideoValue(){
        new Actions(webDriver).moveToElement(videoValue);
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        js.executeScript("arguments[0].click();", videoValue);
    }
}
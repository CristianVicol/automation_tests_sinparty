package pageObjects.webPages.dsp;

import lombok.Getter;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@Getter
public class DspElements {
    private WebDriver webDriver;
    public DspElements(WebDriver webDriver){
        this.webDriver = webDriver;
        PageFactory.initElements(webDriver, this);
    }
    @FindBy(xpath = "//div[contains(@class, 'mol-vast-skip-control')]")
    private WebElement closeVideoSliderBtn;
    @FindBy(css = ".video-page__adv-inside-player")
    private WebElement advBannerUnderVideo;
    @FindBy(id = "video_page_side_of_player-1")
    private WebElement adBannerOnTheRightSideOfTheVideo1;
    @FindBy(id = "video_page_side_of_player-2")
    private WebElement adBannerOnTheRightSideOfTheVideo2;
    @FindBy(css = ".video-page__adv-below-player")
    private WebElement advBannerUnderVideo2;
    @FindBy(css = ".video-page__adv-above-related")
    private WebElement advBannerUnderAboutSection;
    @FindBy(css = ".video-page__adv-below-actions")
    private WebElement advBannerBottomOfPage;
    @FindBy(css = ".video-page__adv-below-feed")
    private WebElement advBannerUnderPopularCreatorsSection;
    @FindBy(css = ".video-page__adv-below-related")
    private WebElement advBannerUnderPopularVideosSection;
    @FindBy(xpath = "//aside//button[@class='popup__close']") //.promo-poster-lines .popup__close
    private WebElement closeAdvPopUpBtn;
    @FindBy(css = ".video-page__adv-side-of-feed")
    private WebElement advBannerOnSideOfFeed;
    @FindBy(xpath = "//a[@class='mol-vast-clickthrough']")
    private WebElement preRoll;
    @FindBy(xpath = "(//div[contains(@class, 'enabled')])[2]")
    private WebElement closePreRollAdBtn;
    @FindBy(xpath = "//div[@class='mol-vast-skip-control']")
    private WebElement skipAdControl;
    public void clickCloseAdvPopUpBtn(){
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        js.executeScript("arguments[0].click();" , closeAdvPopUpBtn);
    }
    public void clickCloseVideoSliderBtn(){
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        js.executeScript("arguments[0].click();", closeVideoSliderBtn);
    }
    public void clickClosePreRollAdBtn(){
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        js.executeScript("arguments[0].click();", closePreRollAdBtn);
    }
    public void clickSkipAdControl(){
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        js.executeScript("arguments[0].click();", skipAdControl);
    }
}
package pageObjects.webPages.dsp;

import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@Getter
public class HomePageDspElements {
    private WebDriver webDriver;
    public HomePageDspElements(WebDriver webDriver){
        this.webDriver = webDriver;
        PageFactory.initElements(webDriver, this);
    }
    @FindBy(xpath = "(//aside[contains(@class, 'adv-section--1-2-3-4')])[1]")
    private WebElement bannerUnderCreatorsSpotlight;
    @FindBy(xpath = "(//aside[contains(@class, 'adv-section--2nd-2nd-1-2')])[1]")
    private WebElement bannerUnderPopularOnSinPartySection;
    @FindBy(css = ".mol-video-ad-container .mol-vast-clickthrough")
    private WebElement videoAdvUnderTrendingNowSection;
    @FindBy(xpath = "(//aside[contains(@class, 'adv-section--1d')])[1]")
    private WebElement bannerUnderTrendingNowSection;
    @FindBy(xpath = "(//aside[contains(@class, 'adv-section--1-2-3-4')])[2]")
    private WebElement bannerUnderPaidVideosSection;
    @FindBy(css = ".adv-section--1-1-2")
    private WebElement bannerUnderWelcomeToThePartySection;
    @FindBy(xpath = "(//aside[contains(@class, 'adv-section--1-2-3-4')])[3]")
    private WebElement bannerUnderMostRecentSection;
    @FindBy(xpath = "(//aside[contains(@class, 'adv-section--2nd-2nd-1-2')])[2]")
    private WebElement bannerUnderCreatorsCategoriesSection;
    @FindBy(xpath = "(//aside[contains(@class, 'adv-section--grid')])[2]")
    private WebElement bannerUnderTheHottestCreatorsSection;
}
package pageObjects.webPages.dsp;

import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
@Getter
public class DspElementsOnListingPages {
    private WebDriver webDriver;
    public DspElementsOnListingPages(WebDriver webDriver){
        this.webDriver = webDriver;
        PageFactory.initElements(webDriver, this);
    }
    @FindBy(xpath = "(//aside[contains(@class, 'adv-section--1d')])[1]")
    private WebElement adBannerOne;
    @FindBy(xpath = "(//aside[contains(@class, 'adv-section--1d')])[2]")
    private WebElement adBannerTwo;
    @FindBy(xpath = "(//aside[contains(@class, 'adv-section--1d')])[3]")
    private WebElement adBannerThree;
    @FindBy(xpath = "(//aside[contains(@class, 'adv-section--1d')])[4]")
    private WebElement adBannerFour;
    @FindBy(xpath = "(//aside[contains(@class, 'adv-section--1d')])[5]")
    private WebElement adBannerFive;
    @FindBy(css = ".adv-section--side")
    private WebElement adBannerOnTheLeftSide;
    @FindBy(css = ".mol-video-ad-container .mol-vast-clickthrough")
    private WebElement videoAd;
    @FindBy(css = ".adv-section--2nd-2nd-1-2")
    private WebElement adBannerOnTheEndOfThePage;
}
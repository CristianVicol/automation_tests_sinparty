package pageObjects.webPages.dsp;

import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
@Getter
public class DspElementsLiveCreators {
    private WebDriver webDriver;
    public DspElementsLiveCreators(WebDriver webDriver){
        this.webDriver = webDriver;
        PageFactory.initElements(webDriver, this);
    }
    @FindBy(xpath = "(//aside[contains(@class, 'adv-section--1-2-3-4')])[1]")
    private WebElement adBannerUnderLiveCreator;
    @FindBy(css = ".hideonmobile .adv-section--center")
    private WebElement adBannerInTheCenterOfThePage;
    @FindBy(xpath = "(//aside[contains(@class, 'adv-section--side')])[1]")
    private WebElement adBannerOnTheLeftSide;
    @FindBy(xpath = "(//aside[contains(@class, 'adv-section--side')])[2]")
    private WebElement adBannerOnTheRightSide;
    @FindBy(css = ".adv-section--desktop")
    private WebElement adBannerOnTheBottom;
    @FindBy(css = ".mol-video-ad-container .mol-vast-clickthrough")
    private WebElement videoOnTheBottom;
}

package pageObjects.webPages;

import lombok.Getter;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.Wait;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Getter
public class CookiesPopUp {
    private WebDriver webDriver;

    public CookiesPopUp(WebDriver webDriver) {
        this.webDriver = webDriver;
        PageFactory.initElements(webDriver, this);
    }

    @FindBy(xpath = "//button[contains(@class, 'btn--primary')]")
    private WebElement acceptAllCookiesBtn;
    @FindBy(xpath = "(//button[contains(@class, 'btn--cancel')])[2]")
    private WebElement rejectAllBtn;
    @FindBy(xpath = "(//button[contains(@class, 'btn--cancel')])[1]")
    private WebElement cookieSettingsBtn;
    @FindBy(linkText = "Learn more")
    private WebElement learnMoreLinkBtn;
    @FindBy(css = ".btn--cancel")
    private WebElement cancelFromCookiesSettingsBtn;
    @FindBy(css = ".popup--common .btn--primary")
    private WebElement saveFromCookiesSettingsBtn;
    public boolean isDisplayedAcceptCookiesBtn(){
        boolean isDisplayed;
        try {
            webDriver.manage().timeouts().implicitlyWait(0L, TimeUnit.SECONDS);
            Wait.untilElementIsVisible(webDriver, acceptAllCookiesBtn, 1L);
            isDisplayed = acceptAllCookiesBtn.isDisplayed();
        } catch (TimeoutException e) {
            System.out.println(e.getMessage());
            isDisplayed = false;
        }
        return isDisplayed;
    }
    public boolean pupUpCookiesAreDisplayed() {
        boolean isDisplayed = true;
        List<WebElement> elementsToBeChecked = new ArrayList<>();
        elementsToBeChecked.add(cookieSettingsBtn);
        elementsToBeChecked.add(rejectAllBtn);
        elementsToBeChecked.add(learnMoreLinkBtn);
        elementsToBeChecked.add(acceptAllCookiesBtn);
        for (WebElement element : elementsToBeChecked) {
            if (!element.isDisplayed()) {
                isDisplayed = false;
            }
        }
        return isDisplayed;
    }

    public void clickLearnMoreLink() {
        learnMoreLinkBtn.click();
        String originalWindowHandle = webDriver.getWindowHandle();
        // Switch to the new window
        for (String windowHandle : webDriver.getWindowHandles()) {
            if (!windowHandle.equals(originalWindowHandle)) {
                webDriver.switchTo().window(windowHandle);
                break;
            }
        }
    }

    public void clickAcceptCookiesBtn() {
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        js.executeScript("arguments[0].click();", acceptAllCookiesBtn);
    }
    public void clickCookiesSettingsBtn(){
        cookieSettingsBtn.click();
    }
    public void clickCancelFromCookiesSettingsBtn(){
        cancelFromCookiesSettingsBtn.click();
    }
    public void clickSaveFromCookiesSettingsBtn(){
        Wait.untilElementIsVisible(webDriver, saveFromCookiesSettingsBtn, 5L);
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        js.executeScript("arguments[0].click();", saveFromCookiesSettingsBtn);
    }
    public void clickRejectAllCookiesBtn(){
       // Wait.untilElementIsClickable(webDriver, rejectAllBtn, 10L);
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        js.executeScript("arguments[0].click()", rejectAllBtn);
    }
}
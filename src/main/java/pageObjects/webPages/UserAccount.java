package pageObjects.webPages;

import lombok.Getter;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

@Getter
public class UserAccount {
    private WebDriver webDriver;

    public UserAccount(WebDriver webDriver){
        this.webDriver = webDriver;
        PageFactory.initElements(webDriver, this);
    }
    @FindBy(xpath = "(//div[@class='acc-section__filters']/button)[2]")
    private WebElement activeSubscriptionsBtn;
    @FindBy(css = ".card-following-list__name")
    private WebElement creatorActiveSubscriptionValue;
    @FindBy(xpath = "//td[@class='account-table__cell--menu']")
    private WebElement cellMenuFromCreatorSubscriptions;
    @FindBy(xpath = "(//li[@class='card-menu__option'])[2]")
    private WebElement cancelSubscriptionOption;
    @FindBy(css = ".subscription-modal__btn")
    private WebElement proceedToCancelSubscriptionBtn;
    @FindBy(css = ".subscription-modal__select")
    private WebElement selectReasonDropDown;
    @FindBy(css = ".rating__star:nth-child(3)")
    private WebElement ratingStar;
    @FindBy(css = ".withshadow")
    private WebElement cancelSubscriptionBtn;
    @FindBy(css = ".subscription-modal--cancel-successfull")
    private WebElement subscriptionCanceledSuccessfulModal;
    @FindBy(css = ".app-modal__ModalInfo .popup__close")
    private WebElement closeSubscriptionCanceledSuccessfulModalBtn;
    public void clickCancelSubscriptionOption(){
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        js.executeScript("arguments[0].click();", cancelSubscriptionOption);
    }

    public void selectReason(){
        Select select = new Select(selectReasonDropDown);
        select.selectByIndex(3);
    }
}
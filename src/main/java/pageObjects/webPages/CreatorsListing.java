package pageObjects.webPages;

import lombok.Getter;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.Utilities;
import utilities.Wait;

import java.util.List;

@Getter
public class CreatorsListing {
    private WebDriver webDriver;
    public CreatorsListing(WebDriver webDriver){
        this.webDriver = webDriver;
        PageFactory.initElements(webDriver, this);
    }
    @FindBy(xpath = "(//div//span[@class='video-details'])[1]")
    private WebElement creatorValue;
    @FindAll(@FindBy(xpath = "//span[@class='video-details']"))
    private List<WebElement> listOfCreators;
    @FindBy(css = ".content-count")
    private WebElement creatorCountOnThePage;
    @FindBy(xpath = "//a[contains(@title, 'Next')]")
    private WebElement nextPageBtn;
    @FindBy(xpath = "//div[@class='listing__header']//a")
    private WebElement updateButton;
    @FindBy(css = ".orientation-selector .gtm_click_blacklabel")
    private WebElement liveCamsBtn;
    @FindBy(xpath = "//ul[@class='main-filters']//a[contains(@href, 'rank')]")
    private WebElement topRankingsBtn;
    @FindBy(xpath = "//ul[@class='main-filters']//a[contains(@href, 'trending')]")
    private WebElement trendingBtn;
    @FindBy(xpath = "//ul[@class='main-filters']//a[contains(@href, 'most-viewed')]")
    private WebElement mostViewedBtn;
    @FindBy(xpath = "//ul[@class='main-filters']//a[contains(@href, 'newest')]")
    private WebElement newestBtn;
    @FindBy(xpath = "//ul[@class='main-filters']//a[contains(@href, 'alphabetical')]")
    private WebElement alphabeticalBtn;
    @FindBy(xpath = "//ul[@class='main-filters']//a[contains(@href, 'random')]")
    private WebElement randomBtn;
    @FindBy(css = ".listing__header span")
    private WebElement listingHeader;

    @FindBy(xpath = "//a[contains(@href, 'featured')]")
    private WebElement featuredCreatorsBtn;
    @FindBy(xpath = "//a[contains(@data-url, 'myparty')]")
    private WebElement premiumCreatorsBtn;
    @FindBy(xpath = "//a[@data-url='gender=female']")
    private WebElement femaleGenderBtn;
    @FindBy(xpath = "//a[@data-url='gender=male']")
    private WebElement maleGenderBtn;
    @FindBy(css = ".section-header .gtm_click_blacklabel")
    private WebElement seeAllCreatorsOnlineNow;
    @FindBy(css = ".scroller--live-cam")
    private WebElement scrollLiveCreatorsContainer;
    @FindBy(css = ".scroller--live-cam .scroller__arrow--next")
    private WebElement scrollNextLiveCreatorBtn;

    public void clickCreatorValue(){
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        js.executeScript("arguments[0].click();", creatorValue);
    }
    public void clickSeeAllCreatorsOnlineNow(){
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        js.executeScript("arguments[0].click();", seeAllCreatorsOnlineNow);
    }
    public void clickUpdateBtn(){
        Wait.untilElementIsVisible(webDriver, updateButton, 30L);
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        js.executeScript("arguments[0].click();", updateButton);
    }
    public void clickNextPageBtn(){
        Utilities.moveToElement(webDriver, nextPageBtn);
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        js.executeScript("arguments[0].click();", nextPageBtn);
    }
    public void clickOnCreatorElement(int index){
        WebElement videoElement = listOfCreators.get(index);
        Utilities.moveToElement(webDriver, videoElement);
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        js.executeScript("arguments[0].click();", videoElement);
    }
    public void clickLiveCamsBtn(){
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        js.executeScript("arguments[0].click();", liveCamsBtn);
    }
    public void clickTopRankingsBtn(){
        Wait.untilElementIsVisible(webDriver, topRankingsBtn, 30L);
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        js.executeScript("arguments[0].click();", topRankingsBtn);
    }
    public void clickTrendingBtn(){
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        js.executeScript("arguments[0].click();", trendingBtn);
    }
    public void clickMostViewedBtn(){
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        js.executeScript("arguments[0].click();", mostViewedBtn);
    }
    public void clickNewestBtn(){
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        js.executeScript("arguments[0].click();", newestBtn);
    }
    public void clickAlphabeticalBtn(){
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        js.executeScript("arguments[0].click();", alphabeticalBtn);
    }
    public void clickRandomBtn(){
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        js.executeScript("arguments[0].click();", randomBtn);
    }
    public void clickFeaturedCreatorsBtn(){
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        js.executeScript("arguments[0].click();", featuredCreatorsBtn);
    }
    public void clickPremiumCreatorsBtn(){
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        js.executeScript("arguments[0].click();", premiumCreatorsBtn);
    }
    public void clickFemaleGenderBtn(){
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        js.executeScript("arguments[0].click();", femaleGenderBtn);
    }
    public void clickMaleGenderBtn(){
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        js.executeScript("arguments[0].click();", maleGenderBtn);
    }
}
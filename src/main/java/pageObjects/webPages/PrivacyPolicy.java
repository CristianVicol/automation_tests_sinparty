package pageObjects.webPages;

import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
@Getter
public class PrivacyPolicy {
    WebDriver webDriver;
    public PrivacyPolicy(WebDriver webDriver){
        this.webDriver = webDriver;
        PageFactory.initElements(webDriver, this);
    }
    @FindBy(id = "element8")
    private WebElement titleCookiesSectionFromPrivacyPolicyPage;
}
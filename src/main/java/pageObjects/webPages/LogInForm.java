package pageObjects.webPages;

import lombok.Getter;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.Utilities;
import utilities.Wait;

import java.sql.Time;
import java.util.Random;
import java.util.concurrent.TimeUnit;

@Getter
public class LogInForm {
    private WebDriver webDriver;
    public LogInForm(WebDriver webDriver){
        this.webDriver = webDriver;
        PageFactory.initElements(webDriver, this);
    }
    @FindBy(xpath = "//div[contains(@class, 'form__input')]//input[@placeholder='Username/email']")
    private WebElement usernameField;
    @FindBy(xpath = "//div[contains(@class, 'form__input')]//input[@placeholder='Password']")
    private WebElement passwordField;
    @FindBy(xpath = "//button[contains(@class, 'modal-auth__submit')]")
    private WebElement logInBtn;
    @FindBy(css = ".modal-auth__footer .btn")
    private WebElement signUpLinkBtn;
    @FindBy(css = ".popup__body .text-warning")
    private WebElement usernameNotFoundPopUp;
    @FindBy(xpath = "//div[@class='modal-auth__logos']//a[contains(@href, '/auth/google/')]")
    private WebElement logInWithGoogleBtn;
    @FindBy(xpath = "//div[@class='modal-auth__logos']/a[@href='/auth/twitter/redirect']")
    private WebElement logInWithTwitterBtn;
    @FindBy(css = ".modal-auth__reset")
    private WebElement forgotPasswordLink;
    @FindBy(css = ".modal-auth__inner .form__group input")
    private WebElement forgotPasswordEmailField;
    @FindBy(css = ".modal-auth__inner .btn")
    private WebElement resetPasswordBtn;
    @FindBy(xpath = "//input[@name='password']")
    private WebElement newPasswordField;
    @FindBy(xpath = "//input[@name='confirm_password']")
    private WebElement confirmPasswordField;
    @FindBy(css = ".modal-auth__inner button")
    private WebElement loginButtonAfterResetPasswordIsDone;
    @FindBy(css = ".modal-auth__success--final")
    private WebElement passwordSuccessfullyResetModal;
    public void logIn(String username, String password) {
        usernameField.sendKeys(username);
        passwordField.sendKeys(password);
    }
    public void clickLogInBtn(){
        Wait.untilElementIsVisible(webDriver, logInBtn, 25L);
        ((JavascriptExecutor) webDriver).executeScript("arguments[0].click();", logInBtn);
    }
    public void clickLogInWithGoogleBtn(){
        ((JavascriptExecutor) webDriver).executeScript("arguments[0].click();", logInWithGoogleBtn);
    }
    public void clickLogInWithTwitterBtn(){
        ((JavascriptExecutor) webDriver).executeScript("arguments[0].click();", logInWithTwitterBtn);
    }
    public String generatePassword(){
        Random random = new Random();
        return "pass" + random.nextInt(10000000);
    }
}
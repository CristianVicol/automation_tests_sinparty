package pageObjects.webPages;

import lombok.Getter;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.Wait;

import java.util.Random;
@Getter
public class SignUpForm {
    private WebDriver webDriver;

    public SignUpForm(WebDriver webDriver){
        this.webDriver = webDriver;
        PageFactory.initElements(webDriver, this);
    }
    @FindBy(xpath = "//input[@name='username']")
    private WebElement usernameField;
    @FindBy(xpath = "//input[@name='email']")
    private WebElement emailField;
    @FindBy(xpath = "//input[@name='password']")
    private WebElement passwordField;
    @FindBy(css = ".app-modal__ModalRegister .modal-auth__submit")
    private WebElement signUpBtn;
    @FindBy(css = ".popup .btn--primary")
    private WebElement proceedBtn;
    public void navigateToSignUpForm(){
        MainWeb mainWeb = new MainWeb(webDriver);
        LogInForm logInForm = new LogInForm(webDriver);
        Wait.untilPageReadyState(webDriver, 5L);
        ((JavascriptExecutor) webDriver).executeScript("arguments[0].click();", mainWeb.getSignInBtn());
        ((JavascriptExecutor) webDriver).executeScript("arguments[0].click();", logInForm.getSignUpLinkBtn());
    }
    public void completeSigUpForm(String username, String email, String password){
        usernameField.sendKeys(username);
        emailField.sendKeys(email);
        passwordField.sendKeys(password);
    }
    public void clickSignUpBtn(){
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        js.executeScript("arguments[0].removeAttribute('disabled')", signUpBtn);
        js.executeScript("arguments[0].click();", signUpBtn);
    }
    public boolean thankYouPopUpIsDisplayed(){
        Wait.untilElementIsVisible(webDriver, proceedBtn, 5L);
        return proceedBtn.isDisplayed();
    }
    public void clickProceedBtn(){
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        js.executeScript("arguments[0].click();", proceedBtn);
    }
    public int generateRandomNumber(){
        Random rand = new Random();
        return rand.nextInt(10000000);
    }
}